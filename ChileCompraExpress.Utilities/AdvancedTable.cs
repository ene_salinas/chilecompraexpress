﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Utilities
{
    public class AdvancedTable:ITable
    {

        private IHtml[] htmlTable = null;
        private string concat;
        private string[] columnas;
        private string[] filas;
        private string[] attr;
        private IHtml tagTr = null;
        private IHtml tagTd = null;
        private IHtml tagTrClose = null;
        private IHtml tagTdClose = null;
        Table table = null;
        
        
        //atributos para la sub tabla
        private string[] subColumnas;
        private string[] subFilas;
        private string[] subIndice;
        private IHtml subTagTr = null;
        private IHtml subTagTd = null;
        private IHtml subTagTrClose = null;
        private IHtml subTagTdClose = null;
        private string concatSub = null;
        private List<Table> subTable = null;
        private string[] subPosicion = null;
        private string[] subTables = null;
        private string[] subAtrr = null;
        

        public override string ToString()
        {
            return this.concat;
        }

        /// <summary>
        /// Construye sub tabla dentro de un td en especifico
        /// </summary>
        /// <returns></returns>
        public string buildSubTable(int index)
        {
            //htmlTable = new TagHtml[subcolumnas.Length + subfilas.Length];
            //this.subColumnas = subcolumnas;
            //this.subFilas = subfilas;
            buildSubBody(index);
            return this.concatSub;
        }

        /// <summary>
        /// Construye la cabecera de la sub tabla
        /// </summary>
        /// <returns></returns>
        public string buildSubHeader(int index)
        {
            if (subTables != null && subTables.Length > 0)
            {                
                if (subColumnas != null && subColumnas.Length > 0)
                {
                    string[] largo = subColumnas[index].ToString().Split(';');

                    /*** <table> ***/
                    IHtml subtagTableOpen = new TagHtml();
                    concatSub += subtagTableOpen.tagTableOpen();

                    /////////////CONSTRUYE CABECERA///////////////////////

                    /**** <tr> ****/
                    subTagTr = new TagHtml();
                    concatSub += subTagTr.tagTrOpen();

                    for (int j = 0; j < largo.Length; j++)
                    {
                        subTagTd = new TagHtml();
                        concatSub += subTagTd.tagTdOpen();

                        concatSub += largo[j];

                        subTagTdClose = new TagHtml();
                        concatSub += subTagTdClose.tagTdClose();

                    }

                    /**** </tr> ****/
                    subTagTrClose = new TagHtml();
                    concatSub += subTagTrClose.tagTrClose();
                }
            }

            return concatSub;
        }

        
        /// <summary>
        /// Construye el cuerpo de la sub tabla
        /// </summary>
        /// <returns></returns>
        public string buildSubBody(int index)
        {
            if (subTable != null && subTable.Count > 0)
            {
                //if (subFilas != null && subFilas.Length > 0)
                //{
                    /*** <table> ***/
                    IHtml tagTableOpen = new TagHtml();
                    tagTableOpen.setAtt(subTable.ElementAt(index).Attribute);

                    concatSub += tagTableOpen.tagTableOpen();

                    for (int j = 0; j < subTable[index].getRows().Count; j++)
                    {
                        /**** <tr> ****/
                        subTagTr = new TagHtml();
                        Row row = subTable[index].getRows().ElementAt(j);

                        subTagTr.setAtt( row.Attribute);
                        concatSub += subTagTr.tagTrOpen();

                        for (int i = 0; i < subTable[index].getRows().ElementAt(j).getColumns().Count; i++)
                        {
                            /**** <td> ****/
                            subTagTd = new TagHtml();                            
                            
                            Column col = subTable[index].getRows().ElementAt(j).getColumns()[i];
                            
                            subTagTd.setAtt(col.Attribute);
                            concatSub += subTagTd.tagTdOpen();
                            
                            concatSub += col.Valor;

                            /**** </td> ****/
                            subTagTdClose = new TagHtml();
                            concatSub += subTagTdClose.tagTdClose();
                           
                        }

                        /**** </tr> ****/
                        tagTrClose = new TagHtml();
                        concatSub += tagTrClose.tagTrClose();

                    }

                    /*** </table> ***/
                    IHtml tagTableClose = new TagHtml();
                    concatSub += tagTableClose.tagTableClose();
                }
            //}

            return concatSub;
        }


        public string buildHeader()
        {
            if (columnas != null && columnas.Length > 0)
            {
                /*** <table> ***/
                IHtml tagTableOpen = new TagHtml();
                tagTableOpen.setAtt("class='table_scroll'");
                concat += tagTableOpen.tagTableOpen();


                /////////////CONSTRUYE CABECERA///////////////////////

                /**** <tr> ****/
                tagTr = new TagHtml();
                concat += tagTr.tagTrOpen();

                for (int j = 0; j < columnas.Length; j++)
                {
                    tagTd = new TagHtml();
                    concat += tagTd.tagTdOpen();

                    concat += columnas[j];

                    tagTdClose = new TagHtml();
                    concat += tagTd.tagTdClose();

                }

                /**** </tr> ****/
                tagTrClose = new TagHtml();
                concat += tagTrClose.tagTrClose();
            }

            return concat;
        }
        

      


        private int validarPosicion(int columna , int fila)
        {
            int resultado = -1;
            int col = -1;
            int row = -1;
            
            if (subTable != null && subTable.Count > 0)
            {
                for (int i = 0; i < subTable.Count; i++)
                {
                    col = subTable.ElementAt(i).Position[0];
                    row = subTable.ElementAt(i).Position[1];

                    if (columna == col && row == fila)
                    {
                        resultado = i;
                        concatSub = "";
                    }
                }

               
            }

            return resultado;
        }

        public string buildBody()
        {
            try
            {
                if (table.getRows() != null && table.getRows().Count > 0)
                {
                    ///////////////CONSTRUYE FILAS////////////////////////////
                    

                    /*** <table> ***/
                    IHtml tagTableOpen = new TagHtml();
                    tagTableOpen.setAtt(table.Attribute);
                    concat += tagTableOpen.tagTableOpen();

                    for (int i = 0; i < table.getRows().Count; i++)
                    {
                    
                        /**** <tr> ****/
                        tagTr = new TagHtml();

                        tagTr.setAtt(table.getRows().ElementAt(i).Attribute);
                        concat += tagTr.tagTrOpen();


                        for (int j = 0; j < table.getRows().ElementAt(i).getColumns().Count; j++)
                        {
                            /**** <td> ****/
                            tagTd = new TagHtml();

                            tagTd.setAtt(table.getRows().ElementAt(i).getColumns().ElementAt(j).Attribute);
                            concat += tagTd.tagTdOpen();

                            int resultado = validarPosicion(i, j);
                            concat += ((resultado == -1) ? table.getRows().ElementAt(i).getColumns().ElementAt(j).Valor : buildSubTable(resultado));

                            /**** </td> ****/
                            tagTdClose = new TagHtml();
                            concat += tagTd.tagTdClose();
                        }                       

                        /**** </tr> ****/
                        tagTrClose = new TagHtml();
                        concat += tagTrClose.tagTrClose();

                    }

                    /*** </table> ***/
                    IHtml tagTableClose = new TagHtml();
                    concat += tagTableClose.tagTableClose();
                }

                return concat;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

       
        public AdvancedTable(Table table, List<Table> subTable)
        {
            try
            {
                
                this.table = table;
                this.subTable = subTable;              
                
                buildBody();
            }
            catch (Exception ex)
            {
                throw(ex);
            }
        }
    }
}
