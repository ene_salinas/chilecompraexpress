using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;

namespace ChileCompraExpress.Utilities
{
    public class ProcedureMSQLServer : IProcedure
    {
        OleDbCommand cmd;
        private string nombreProcedimiento;
        private string[] nameParamIn;
        private string[] valueParamIn;
        private OleDbType[] tipos;
        private ParameterDirection[] direction;
        int[] size ;
        OleDbParameter oldbParam = null;
        OleDbParameter[] oldParamList ;

        public ProcedureMSQLServer
            (
                OleDbConnection conn,
                string nombreProcedimiento,
                string[] nameParamIn,
                string[] valueParamIn,
                ParameterDirection[] direction,
                OleDbType[] tipos ,
                int[] size
                  
            )
            : base()
        {
            cmd = new OleDbCommand();
            cmd.Connection = conn;
            this.nombreProcedimiento = nombreProcedimiento;
            this.nameParamIn = nameParamIn;
            this.valueParamIn = valueParamIn;
            this.tipos = tipos;
            this.direction = direction;
            this.size = size;
        }

        public object addObjectCommand()
        {
            
            //EXAMPLE:
            //cmd.Parameters.Add("@NOMBRE_PARAMETRO",OleDbType.Integer).Value=VALOR;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = this.nombreProcedimiento;
            

            if  (  ((this.nameParamIn != null) && (this.nameParamIn.Length > 0))
                && ((this.valueParamIn != null) && (this.valueParamIn.Length > 0))
                && ((this.tipos != null) && (this.tipos.Length > 0)))
            {

                oldParamList = new OleDbParameter[nameParamIn.Length];

                for (int i = 0; i < nameParamIn.Length; i++)
                {
                    oldbParam = new OleDbParameter("@" + this.nameParamIn[i].ToString(), this.tipos[i]);
                    oldbParam.Value = this.valueParamIn[i].ToString();
                    oldbParam.Direction = (this.direction == null) ? ParameterDirection.Input : direction[i];
                    oldbParam.Size = (this.size == null) ? 0 : size[i];

                    oldParamList[i] = oldbParam;

                    cmd.Parameters.Add(oldbParam);
                }
            }

            return (object)cmd;
        }



        public object[] getParameter()
        {
            return oldParamList;

        }
    }
}
