﻿using System.Text;
using System;
using System.Data;
using System.Configuration;
using System.Globalization;


namespace ChileCompraExpress.Utilities{

    /// <summary>
    /// Clase de recursos para formato de números y fechas.
    /// </summary>
    public class Formatos
    {
        //Constructor de la clase
        public Formatos()
        {
        }

       /// <summary>
        /// Formatea un numero a formato ###.###.###
        /// </summary>
        /// <param name="Objeto">Objeto.</param>
        /// <returns>string.</returns>
        public static string formatoEntero(object Objeto)
        {
            double tmpLng = formatearDecimal(Objeto);
            string tmpStr;
            if (tmpLng == 0)
            {
                tmpStr = "0";
            }
            else
            {
                //tmpStr = tmpLng.ToString("#,#.##");//con decimal
                tmpStr = tmpLng.ToString("#,#");//sin decimal
            }
            tmpStr = tmpStr.Replace(",", ".");
            return tmpStr;
        }

        /// <summary>
        /// Formatea un numero a formato ###.###.###
        /// </summary>
        /// <param name="Objeto">Objeto.</param>
        /// <returns>string.</returns>
        public static string formatoDecimal(object Objeto)
        {
            double tmpLng = formatearDecimal(Objeto);
            string tmpStr;
            if (tmpLng == 0)
            {
                tmpStr = "0";
            }
            else
            {
                if (System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator == ",")
                {
                    if (tmpLng.ToString().Contains("-0,") || tmpLng.ToString().Contains("0,"))
                    {
                        CultureInfo ci = new CultureInfo("en-us");
                        tmpStr = tmpLng.ToString("G", ci).Replace(".",",");
                    }
                    else
                    {
                        tmpStr = tmpLng.ToString("#,#.##");//con decimal
                    }
                }
                else
                {
                    tmpStr = tmpLng.ToString("#,#.##");
                    tmpStr = tmpStr.Replace(",", "#");
                    tmpStr = tmpStr.Replace(".", ",");
                    tmpStr = tmpStr.Replace("#", ".");
                }
            }

            return tmpStr;
        }

        /// <summary>
        /// Reemplaza acentos.
        /// </summary>
        /// <param name="texto">Texto entrada.</param>
        /// <returns>string.</returns>
        public static string formatoAcentos(string texto)
        {          
            texto = texto.Replace("&Aacute;","Á");
                texto = texto.Replace("&Agrave;","À");
            texto = texto.Replace("&Eacute;","É");
                texto = texto.Replace("&Egrave;","È");
            texto = texto.Replace("&Iacute;","Í");
                texto = texto.Replace("&Igrave;","Ì");
            texto = texto.Replace("&Oacute;","Ó");
                texto = texto.Replace("&Ograve;","Ò");
            texto = texto.Replace("&Uacute;","Ú");
                texto = texto.Replace("&Ugrave;","Ù");
            texto = texto.Replace("&aacute;","á");
                texto = texto.Replace("&agrave;","à");
            texto = texto.Replace("&eacute;","é");
                texto = texto.Replace("&egrave;","è");
            texto = texto.Replace("&iacute;","í");
                texto = texto.Replace("&igrave;","ì");
            texto = texto.Replace("&oacute;","ó");
                texto = texto.Replace("&ograve;","ò");
            texto = texto.Replace("&uacute;","ú");
                texto = texto.Replace("&ugrave;","ù");
            texto = texto.Replace("&Auml;","Ä");
                texto = texto.Replace("&Acirc;","Â");
            texto = texto.Replace("&Euml;","Ë");
                texto = texto.Replace("&Ecirc;","Ê");
            texto = texto.Replace("&Iuml;","Ï");
                texto = texto.Replace("&Icirc;","Î");
            texto = texto.Replace("&Ouml;","Ö");
            texto = texto.Replace("&Ocirc;","Ô");
            texto = texto.Replace("&Uuml;","Ü");
                texto = texto.Replace("&Ucirc;","Û");
            texto = texto.Replace("&auml;","ä");
                texto = texto.Replace("&acirc;","â");
            texto = texto.Replace("&euml;","ë");
                texto = texto.Replace("&ecirc;","ê");
            texto = texto.Replace("&iuml;","ï");
                texto = texto.Replace("&icirc;","î");
            texto = texto.Replace("&ouml;","ö");
                texto = texto.Replace("&ocirc;","ô");
            texto = texto.Replace("&uuml;","ü");
                texto = texto.Replace("&ucirc;","û");
            texto = texto.Replace("&Atilde;","Ã");
                texto = texto.Replace("&aring;","å");
            texto = texto.Replace("&Ntilde;","Ñ");
                texto = texto.Replace("&Aring;","Å");
            texto = texto.Replace("&Otilde;","Õ");
                texto = texto.Replace("&Ccedil;","Ç");
            texto = texto.Replace("&atilde;","ã");
                texto = texto.Replace("&ccedil;","ç");
            texto = texto.Replace("&ntilde;","ñ");
                texto = texto.Replace("&Yacute;","Ý");
            texto = texto.Replace("&otilde;","õ");
                texto = texto.Replace("&yacute;","ý");
            texto = texto.Replace("&Oslash;","Ø");
                texto = texto.Replace("&yuml;"," ÿ");
            texto = texto.Replace("&oslash;","ø");
                texto = texto.Replace("&THORN;","Þ");
            texto = texto.Replace("&ETH;","Ð");
                texto = texto.Replace("&thorn;","þ");
            texto = texto.Replace("&eth;","ð");
                texto = texto.Replace("&AElig;","Æ");
            texto = texto.Replace("&szlig;","ß");
            texto = texto.Replace("&aelig;", "æ");
            texto = texto.Replace("&ordm;", "º");
            
            return texto;
        }

        /// <summary>
        /// Formatea un numero a decimal.
        /// </summary>
        /// <param name="Objeto">Objeto.</param>
        /// <returns>double.</returns>
        static public double formatearDecimal(object Objeto)
        {
            double tmpRes = 0;
            string valor = Objeto.ToString();

            try
            {
                if ((Formatos.numeroRepeticionesCaracter(valor, ".") >= 2) && !valor.Contains(","))
                {
                    valor = valor.Replace(".", "");

                    return System.Convert.ToDouble(valor);
                }
                else if ((Formatos.numeroRepeticionesCaracter(valor, ",") >= 2) && !valor.Contains("."))
                {
                    valor = valor.Replace(",", "");

                    return System.Convert.ToDouble(valor);                    
                }
                else
                {

                    try
                    {
                        tmpRes = System.Convert.ToDouble(Objeto);
                    }
                    catch
                    {
                        try
                        {
                            tmpRes = System.Convert.ToDouble(Objeto.ToString().Substring(2));
                        }
                        catch
                        {
                            tmpRes = formatearValor(Objeto);
                        }
                    }

                    return tmpRes;
                }
            }
            catch 
            {
                return 0;
            }
        }

        /// <summary>
        /// Convierte a double un valor.
        /// </summary>
        /// <param name="valor">Valor entrada.</param>
        /// <returns>double.</returns>
        public static double convertirDouble(string valor) 
        {
            double respuesta = 0;
            string[] data;
            try
            {
                if ((valor.Contains(",")) && (valor.Contains(".")))
                {
                    if (Formatos.tieneCaracterAlFinal(valor, ",", 3))
                    {
                        data = valor.Split(',');
                        data[0] = data[0].Replace(".", "");

                        if (System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator == ",")
                        {
                            valor = data[0] + "," + data[1];
                        }
                        else 
                        {
                            valor = data[0] + "." + data[1];
                        }

                        respuesta = Convert.ToDouble(valor);
                    }
                    else if (Formatos.tieneCaracterAlFinal(valor, ".", 3))
                    {
                        data = valor.Split('.');
                        data[0] = data[0].Replace(",", "");

                        if (System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator == ",")
                        {
                            valor = data[0] + "," + data[1];
                        }
                        else
                        {
                            valor = data[0] + "." + data[1];
                        }

                        respuesta = Convert.ToDouble(valor);
                    }
                }
                else
                {                    
                    if (valor.Contains(","))
                    {                      
                        if (System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator == ",")
                        {
                            respuesta = Convert.ToDouble(valor);
                        }
                        else 
                        {
                            valor = valor.Replace(",", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

                            respuesta = Convert.ToDouble(valor);
                        }
                    }
                    else
                    {
                        if (System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator == ".")
                        {
                            respuesta = Convert.ToDouble(valor);
                        }
                        else
                        {
                            valor = valor.Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);

                            respuesta = Convert.ToDouble(valor);
                        }
                    }
                }

                return respuesta;
            }
            catch 
            {
                return respuesta;
            }
        }

        /// <summary>
        /// Formatea un objeto a numero.
        /// </summary>
        /// <param name="Objeto">Objeto.</param>
        /// <returns>double.</returns>
        public static double formatearValor(object Objeto)
        {
            int A = 0;
            double F = 0;
            string S = "";
            bool N = false;
            string T;
            if (Objeto != null)
            {
                T = Objeto.ToString();
                for (A = 0; A < T.Length; A++)
                {
                    switch (T.Substring(A, 1))
                    {
                        case "0": S = "0"; break;
                        case "1": S = "1"; break;
                        case "2": S = "2"; break;
                        case "3": S = "3"; break;
                        case "4": S = "4"; break;
                        case "5": S = "5"; break;
                        case "6": S = "6"; break;
                        case "7": S = "7"; break;
                        case "8": S = "8"; break;
                        case "9": S = "9"; break;
                        case ".": S = "."; break;
                        case "-": N = true; break;
                    }
                }
                try
                {
                    F = System.Convert.ToDouble(S);
                    if (N) F = 0 - F;
                }
                catch (System.FormatException)
                {
                    F = 0;
                }
            }
            else
            {
                F = 0;
            }
            return F;
        }

        /// <summary>
        /// Formatea fecha en Mes, Dia, Año.
        /// </summary>
        /// <param name="fecha">Fecha.</param>
        /// <returns>DateTime.</returns>
        public static DateTime formatearFechaMDA(string fecha)
        {
            DateTime date = Convert.ToDateTime(fecha);

            return Convert.ToDateTime(date.Month + "-" + date.Day + "-" + date.Year + "T00:00:00.0000000-05:00");
        }

        /// <summary>
        /// Formatea fecha en Año, Mes, Dia.
        /// </summary>
        /// <param name="fecha">Fecha.</param>
        /// <returns>DateTime.</returns>
        public static DateTime formatearFechaAMD(string fecha)
        {
            DateTime date = Convert.ToDateTime(fecha);

            return Convert.ToDateTime(date.Year + "-" + date.Month + "-" + date.Day + "T00:00:00.0000000-05:00");
        }

        /// <summary>
        /// Convierte una fecha en formato dd-mm-aaaa a formato Datatime.
        /// </summary>
        /// <param name="fecha">Fecha.</param>
        /// <returns>DateTime.</returns>
        public static DateTime convertirDateTime(string fecha) 
        {
            if((fecha != null) && (fecha != ""))
            {
                fecha = fecha.Replace('/', '-');

                string[] data = fecha.Split('-');

                try
                {
                    return Convert.ToDateTime(data[2] + "-" + data[1] + "-" + data[0]);
                }
                catch
                {
                    return Convert.ToDateTime(data[0] + "-" + data[1] + "-" + data[2]);
                }
            }
            else
            {
                return DateTime.Now;
            }
        }

        /// <summary>
        /// Modifica el formato de una Fecha
        /// </summary>
        /// <param name="fecha">Fecha.</param>
        /// <returns>string.</returns>
        public static string split_fecha(string fecha)
        {
            fecha = fecha.Replace("/", "-");

            string[] arr = fecha.Split('-');

            return arr[2] + "-" + arr[1] + "-" + arr[0];
        }

        /// <summary>
        /// Obtiene un parametro entre dos llaves.
        /// </summary>
        /// <param name="texto">Texto.</param>
        /// <param name="key">Parametro.</param>
        /// <returns></returns>
        public static string devolverValor(string texto, string key)
        {
            try
            {
                string valor = "";
                int inicio = texto.IndexOf(key);
                if (inicio >= 0)
                {
                    key = key.Replace("[", "<");
                    key = key.Replace("]", ">");
                    valor = texto.Replace("[", "<");
                    valor = valor.Replace("]", ">");

                    System.Text.RegularExpressions.Regex exp_regular = new System.Text.RegularExpressions.Regex("(?<=" + key + ").*(?=" + key + ")");
                    System.Text.RegularExpressions.MatchCollection mc = exp_regular.Matches(valor);
                    if ((mc != null) && (mc.Count > 0))
                    {
                        System.Text.RegularExpressions.GroupCollection col = mc[0].Groups;
                        valor = mc[0].ToString();
                        if (!valor.Contains(key))
                        {
                            valor = Formatos.limpiarXml(valor);
                            return valor;
                        }
                        else
                        {
                            texto = Formatos.limpiarXml(texto);

                            return texto;
                        }
                    }
                    else
                    {
                        texto = Formatos.limpiarXml(texto);

                        return texto;
                    }
                }
                else
                {
                    return texto;
                }
            }
            catch 
            {
                return texto;
            }
        }

        //Se obtiene un parametro entre dos llaves
        /// <summary>
        /// Obtiene un parametro entre dos llaves.
        /// </summary>
        /// <param name="texto">Texto.</param>
        /// <param name="key">Parametro.</param>
        /// <param name="vacio">Si es vacío.</param>
        /// <returns></returns>
        public static string devolverValor(string texto, string key, bool vacio)
        {
            try
            {
                string valor = "";
                int inicio = texto.IndexOf(key);
                if (inicio >= 0)
                {
                    key = key.Replace("[", "<");
                    key = key.Replace("]", ">");
                    valor = texto.Replace("[", "<");
                    valor = valor.Replace("]", ">");

                    System.Text.RegularExpressions.Regex exp_regular = new System.Text.RegularExpressions.Regex("(?<=" + key + ").*(?=" + key + ")");
                    System.Text.RegularExpressions.MatchCollection mc = exp_regular.Matches(valor);
                    if ((mc != null) && (mc.Count > 0))
                    {
                        System.Text.RegularExpressions.GroupCollection col = mc[0].Groups;
                        valor = mc[0].ToString();
                        if (!valor.Contains(key))
                        {
                            valor = Formatos.limpiarXml(valor);
                            return valor;
                        }
                        else
                        {
                            if (vacio == false)
                            {
                                texto = Formatos.limpiarXml(texto);

                                return texto;
                            }
                            else
                            {
                                return "";
                            }
                        }
                    }
                    else
                    {
                        if (vacio == false)
                        {
                            texto = Formatos.limpiarXml(texto);

                            return texto;
                        }
                        else
                        {
                            return "";
                        }
                    }
                }
                else
                {
                    if (vacio == false)
                    {
                        return texto;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch 
            {
                return texto;
            }
        }

        /// <summary>
        /// Limpia los datos para el XML
        /// </summary>
        /// <param name="cadena">Cadena xml.</param>
        /// <returns>string.</returns>
        public static string limpiarXml(string cadena)
        {
            cadena = cadena.Replace("&", "&amp;");
            cadena = cadena.Replace("<", "&lt;");
            cadena = cadena.Replace(">", "&gt;");
            cadena = cadena.Replace("'", "&apos;");
            cadena = cadena.Replace("\"", "&quot;");

            return cadena;
        }

        /// <summary>
        /// Obtiene el valor entero de un string.
        /// </summary>
        /// <param name="valor">Valor</param>
        /// <returns>int</returns>
        public static int convertirEntero(string valor)
        {
            try
            {
                double valor_nuevo = Convert.ToDouble(valor);

                return Convert.ToInt32(valor_nuevo);
            }
            catch 
            {
                if (valor.EndsWith(",00"))
                {
                    return Convert.ToInt32(valor.Replace(",00", ""));
                }
                else if (valor.EndsWith(".00"))
                {
                    return Convert.ToInt32(valor.Replace(".00", ""));
                }
                else 
                {
                    return Convert.ToInt32(valor);
                }
            }
        }

        /// <summary>
        /// Recibe un string y se convierte a booleano.
        /// </summary>
        /// <param name="valor">String</param>
        /// <returns>bool</returns>
        public static bool convertirBool(string valor)
        {
            if ((valor == "1") || (valor.ToLower() == "t") || (valor.ToLower() == "v")
                || (valor.ToLower() == "true") || (valor.ToLower() == "verdadero"))
            {
                return true;
            }
            else if ((valor == "0") || (valor.ToLower() == "f")
                || (valor.ToLower() == "false") || (valor.ToLower() == "falso"))
            {
                return false;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Devuelve el primer char de cada cadena en el string con mayúscula.
        /// </summary>
        /// <param name="cadena">String</param>
        /// <returns>string</returns>
        public static string first_upper_all_string(string cadena)
        {
            try
            {
                string respuesta = "";
                string[] palabras = cadena.Trim().Split(' ');
                for (int i = 0; i < palabras.Length; i++)
                {
                    respuesta += " " + Formatos.first_char_upper_string(palabras[i]);
                }

                return respuesta.Trim();
            }
            catch
            {
                return cadena.Trim();
            }
        }

        /// <summary>
        /// Devuelve el primer char del string con mayúscula.
        /// </summary>
        /// <param name="cadena">String</param>
        /// <returns>string</returns>
        public static string first_char_upper_string(string cadena)
        {
            string resp = "";
            try
            {
                resp = cadena.Trim().Substring(0, 1).ToUpper() + cadena.Substring(1, cadena.Length - 1).ToLower();
                return resp;
            }
            catch
            {
                return cadena.Trim();
            }
        }

        /// <summary>
        /// Reemplaza "'" con "''" para base de datos.
        /// </summary>
        /// <param name="texto">string</param>
        /// <returns>string</returns>
        public static string codificar_string(string texto)
        {

            texto = texto.Replace("'", "''");
            //   texto = texto.Replace("&", "&amp;");
            //  texto = texto.Replace("'", "&apos;");
            // texto = texto.Replace(Convert.ToString(Convert.ToChar(34)), "&amp;");
            //texto = texto.Replace(">", "&gt;");
            //texto = texto.Replace("<", "&lt;");
            //texto = texto.Replace(Convert.ToString(Convert.ToChar(92)),"&#92;");

            return texto;
        } 

       /// <summary>
        /// Valida si una cadena contiene hacia final un caracter especifico.
        /// </summary>
        /// <param name="cadena">String</param>
        /// <param name="caracter">Caracter a buscar.</param>
        /// <param name="pos">Posición.</param>
        /// <returns>bool</returns>
        public static bool tieneCaracterAlFinal(string cadena, string caracter, int pos) 
        {
            if(cadena.Substring(cadena.Length - pos, 1) == caracter)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

       /// <summary>
        /// Resta entre 2 fechas.
        /// </summary>
        /// <param name="fecha_1">Fecha inicial.</param>
        /// <param name="fecha_2">Fecha final.</param>
        /// <returns>int</returns>
        public static int obtener_dias_fechas(string fecha_1, string fecha_2) 
        {
            try
            {
                fecha_1 = fecha_1.Replace("/", "-");
                fecha_2 = fecha_2.Replace("/", "-");

                string[] data_1 = fecha_1.Split('-');
                string[] data_2 = fecha_2.Split('-');

                try
                {
                    DateTime ingreso;
                    DateTime salida;

                    if(data_1[0].Length == 4)
                    {
                        ingreso = DateTime.Parse(data_1[0] + "-" + data_1[1] + "-" + data_1[2]);
                        salida = DateTime.Parse(data_2[0] + "-" + data_2[1] + "-" + data_2[2]);
                    }
                    else
                    {
                        ingreso = DateTime.Parse(data_1[2] + "-" + data_1[1] + "-" + data_1[0]);
                        salida = DateTime.Parse(data_2[2] + "-" + data_2[1] + "-" + data_2[0]);
                    }

                    string cantidad = Convert.ToString(salida.Subtract(ingreso));
                    int total_noches = Convert.ToInt32(cantidad.Substring(0, cantidad.IndexOf(".")));

                    return total_noches;
                }
                catch
                {
                    DateTime ingreso;
                    DateTime salida;

                    if (data_1[0].Length == 4)
                    {
                        ingreso = DateTime.Parse(data_1[0] + "-" + data_1[2] + "-" + data_1[1]);
                        salida = DateTime.Parse(data_2[0] + "-" + data_2[2] + "-" + data_2[1]);
                    }
                    else 
                    {
                        ingreso = DateTime.Parse(data_1[0] + "-" + data_1[1] + "-" + data_1[2]);
                        salida = DateTime.Parse(data_2[0] + "-" + data_2[1] + "-" + data_2[2]);
                    }

                    string cantidad = Convert.ToString(salida.Subtract(ingreso));
                    int total_noches = Convert.ToInt32(cantidad.Substring(0, cantidad.IndexOf(".")));

                    return total_noches;
                }
            }
            catch 
            {
                return 0;
            }
        }

        //Se formatea una fecha aaaa-mm-dd a dd-mm-aaaa
        /// <summary>
        /// Formatea una fecha aaaa-mm-dd a dd-mm-aaaa.
        /// </summary>
        /// <param name="fecha">String</param>
        /// <returns>string.</returns>
        public static string formatearFechaDMA(string fecha) 
        {
            try
            {
                fecha = fecha.Replace("/", "-");
                string[] data = fecha.Split('-');

                return data[2] + "-" + data[1] + "-" + data[0];
            }
            catch 
            {
                return fecha;
            }
        }

        //Se formatea una fecha aaaa-mm-dd a dd-mm-aaaa
        public static string fechaAMD(string fecha)
        {
            try
            {
                fecha = fecha.Replace("/", "-");

                string[] data = fecha.Split('-');

                return data[2] + "-" + data[1] + "-" + data[0];
            }
            catch
            {
                return fecha;
            }
        }

        //Se obtiene la fecha actul en el formato AAAA-MM-DD hh:mm:ss
        public static string fechaActualAMD() 
        {
            DateTime fecha = DateTime.Now;

            string fecha_actual = fecha.Year + "-";
            if(fecha.Month < 10)
            {
                fecha_actual += "0" + fecha.Month;
            }
            else
            {
                fecha_actual += fecha.Month.ToString();
            }

            fecha_actual += "-";
            if (fecha.Day < 10)
            {
                fecha_actual += "0" + fecha.Day;
            }
            else
            {
                fecha_actual += fecha.Day.ToString();
            }

            fecha_actual += " ";

            if (fecha.Hour < 10)
            {
                fecha_actual += "0" + fecha.Hour;
            }
            else
            {
                fecha_actual += fecha.Hour.ToString();
            }

            fecha_actual += ":";
            if (fecha.Minute < 10)
            {
                fecha_actual += "0" + fecha.Minute;
            }
            else
            {
                fecha_actual += fecha.Minute.ToString();
            }

            fecha_actual += ":";
            if (fecha.Second < 10)
            {
                fecha_actual += "0" + fecha.Second;
            }
            else
            {
                fecha_actual += fecha.Second.ToString();
            }

            return fecha_actual;
        }

        //Se obtiene la fecha actul en el formato DD-MM-AAAA hh:mm:ss
        public static string fechaActualDMA()
        {
            DateTime fecha = DateTime.Now;

            string fecha_actual = "";
            
            if (fecha.Day < 10)
            {
                fecha_actual += "0" + fecha.Day;
            }
            else
            {
                fecha_actual += fecha.Day.ToString();
            }

            fecha_actual += "-";

            if (fecha.Month < 10)
            {
                fecha_actual += "0" + fecha.Month;
            }
            else
            {
                fecha_actual += fecha.Month.ToString();
            }

            fecha_actual += "-" + fecha.Year;

            fecha_actual += " ";

            if (fecha.Hour < 10)
            {
                fecha_actual += "0" + fecha.Hour;
            }
            else
            {
                fecha_actual += fecha.Hour.ToString();
            }

            fecha_actual += ":";
            if (fecha.Minute < 10)
            {
                fecha_actual += "0" + fecha.Minute;
            }
            else
            {
                fecha_actual += fecha.Minute.ToString();
            }

            fecha_actual += ":";
            if (fecha.Second < 10)
            {
                fecha_actual += "0" + fecha.Second;
            }
            else
            {
                fecha_actual += fecha.Second.ToString();
            }

            return fecha_actual;
        }

        //Se da el formato al numero de tarjeta de credito xxxxx-4567
        public static string formatoTarjetaCredito(string numero_tarjeta) 
        {
            string nuevo_numero = "XXXX-XXXX-XXXX-";

            nuevo_numero += numero_tarjeta.Substring(numero_tarjeta.Length - 4, 4);

            return nuevo_numero;
        }

        //Se realiza la cuenta del numero de veces que un caracter aparece en la cadena
        public static int numeroRepeticionesCaracter(string cadena, string caracter)
        {
            int contador = 0;
            try{
                for(int i=0;i < cadena.Length;i++)
                {
                    if(cadena.Substring(i, 1) == caracter)
                    {
                        contador += 1;
                    }
                }

                return contador;
            }
            catch
            {
                return 0;
            }
        }

        //Se limpia el final de una cadena
        public static string limpiar_fin(string cadena, string eliminar)
        {
            if (cadena.EndsWith(eliminar))
            {
                cadena = cadena.Substring(0, cadena.LastIndexOf(eliminar));

                return cadena;
            }
            else
            {
                return cadena;
            }
        }

        //Se obtiene la fecha solo numeros en formato aaaammdd
        public static string fechaAMDnumeros(string fecha)
        {
            fecha = Formatos.fechaAMD(fecha);

            fecha = fecha.Replace("/", "");
            fecha = fecha.Replace("-", "");

            return fecha;
        }

        //Se obtiene la hora de una fecha especifica
        public static string obtener_hora_fecha(string fecha) 
        {
            try 
            {
                string[] data = fecha.Split(' ');
                if((data != null) && (data.Length > 0))
                {
                    string[] hora = data[1].Split(':');

                    if ((hora != null) && (hora.Length > 0))
                    {
                        return hora[0] + ":" + hora[1];
                    }
                    else
                    {
                        return "00:00";
                    }
                }
                else
                {
                    return "00:00";
                }
            }
            catch //(Exception ex)
            {
                return "00:00";
            }
        }

        
       
        

        //Se realiza la conversion de una fecha de un determinado formato a otro
        public static string formatearFecha(string fecha, string formato_actual, string formato_nuevo) 
        {
            string[] data;
            string fecha_nueva = "";
            bool separador_slash = false;
            bool separador_guion = false;

            if(fecha.Trim() == "")
            {
                return "";
            }

            if(fecha.Contains("/"))
            {
                fecha = fecha.Replace("/", "-");                
            }
            fecha = fecha.Trim();

            if(formato_nuevo.Contains("/"))
            {
                separador_slash = true;
            }
            else if(formato_nuevo.Contains("-"))
            {
                separador_guion = true;
            }

            switch(formato_actual)
            {
                case "dd-mm-aaaa":
                case "ddmmaaaa":
                    switch(formato_nuevo)
                    {
                        case "dd-mm-aaaa":
                        case "ddmmaaaa":
                            data = Formatos.fechaSeparar(fecha, formato_actual);
                            fecha_nueva = data[0] + "-" + data[1] + "-" + data[2];
                            break;
                        case "mm-dd-aaaa":
                        case "mmddaaaa":
                            data = Formatos.fechaSeparar(fecha, formato_actual);
                            fecha_nueva = data[1] + "-" + data[0] + "-" + data[2];
                            break;
                        case "aaaa-mm-dd":
                        case "aaaammdd":
                            data = Formatos.fechaSeparar(fecha, formato_actual);
                            fecha_nueva = data[2] + "-" + data[1] + "-" + data[0];
                            break;
                    }
                    break;
                case "mm-dd-aaaa":
                case "mmddaaaa":
                    switch (formato_nuevo)
                    {
                        case "dd-mm-aaaa":
                        case "ddmmaaaa":
                            data = Formatos.fechaSeparar(fecha, formato_actual);
                            fecha_nueva = data[1] + "-" + data[0] + "-" + data[2];
                            break;
                        case "mm-dd-aaaa":
                        case "mmddaaaa":
                            data = Formatos.fechaSeparar(fecha, formato_actual);
                            fecha_nueva = data[0] + "-" + data[1] + "-" + data[2];
                            break;
                        case "aaaa-mm-dd":
                        case "aaaammdd":
                            data = Formatos.fechaSeparar(fecha, formato_actual);
                            fecha_nueva = data[2] + "-" + data[0] + "-" + data[1];
                            break;
                    }
                    break;
                case "aaaa-mm-dd":
                case "aaaammdd":
                    switch (formato_nuevo)
                    {
                        case "dd-mm-aaaa":
                        case "ddmmaaaa":
                            data = Formatos.fechaSeparar(fecha, formato_actual);
                            fecha_nueva = data[2] + "-" + data[1] + "-" + data[0];
                            break;
                        case "mm-dd-aaaa":
                        case "mmddaaaa":
                            data = Formatos.fechaSeparar(fecha, formato_actual);
                            fecha_nueva = data[1] + "-" + data[2] + "-" + data[0];
                            break;
                        case "aaaa-mm-dd":
                        case "aaaammdd":
                            data = Formatos.fechaSeparar(fecha, formato_actual);
                            fecha_nueva = data[0] + "-" + data[1] + "-" + data[2];
                            break;
                    }
                    break;
            }        

            if(separador_slash == true)
            {
                fecha_nueva = fecha_nueva.Replace("-", "/");
            }
            else if(separador_guion == false)
            {
                fecha_nueva = fecha_nueva.Replace("-", "");
            }

            return fecha_nueva;
        }

        //Se separa la fecha de acuerdo al formato
        public static string[] fechaSeparar(string fecha, string formato_fecha) 
        {
            string[] data;

            if (fecha.Contains("-"))
            {
                data = fecha.Split('-');
            }
            else
            {
                data = new string[3];
                switch(formato_fecha){
                    case "ddmmaaaa":
                        data[0] = fecha.Substring(0, 2);
                        data[1] = fecha.Substring(2, 2);
                        data[2] = fecha.Substring(4, 4);
                        break;
                    case "mmddaaaa":
                        data[0] = fecha.Substring(0, 2);
                        data[1] = fecha.Substring(2, 2);
                        data[2] = fecha.Substring(4, 4);
                        break;
                    case "aaaammdd":
                        data[0] = fecha.Substring(0, 4);
                        data[1] = fecha.Substring(4, 2);
                        data[2] = fecha.Substring(6, 2);
                        break;
                }
            }

            return data;
        }
        /// <summary>
        /// Para a mayúscula la primera letra de una palabra.
        /// </summary>
        /// <param name="s">String.</param>
        /// <returns>String.</returns>
        public static string MayusculaPrimeraLetra(string s)
        {
            // Revisa si string es vacio.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Retorna letra mayúscula + resto de la palabra.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        public static string separadorDeMiles(string valor)
        {
            string numeroString = valor;
            double N = Convert.ToDouble(numeroString);
            string s = String.Format("{0:C}", N);
            string[] separarSigno,separarComas;

            separarSigno = s.Split(' ');
            separarComas = separarSigno[1].Split(',');
            if (separarComas[1] == "00")
            {
                return separarComas[0];
            }
            else
            {
                return separarSigno[1];
            }
        }

        public static string encodeUTF8(string valor)
        {
            UTF8Encoding utf8 = new UTF8Encoding();

            byte[] encodedBytes = utf8.GetBytes(valor);

            string message = utf8.GetString(encodedBytes);

            return message;
        }
    
    }
}