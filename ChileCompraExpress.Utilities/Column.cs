﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Utilities
{
    public class Column
    {
        public string Valor { get; set; }
        public string Cssclass { get; set; }
        public string HtmlId { get; set; }
        public string Rowspan { get; set; }
        public string Columnspan { get; set; }
        public string Attribute { get; set; }
    }
}
