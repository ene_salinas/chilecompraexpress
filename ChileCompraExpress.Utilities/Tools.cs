﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Utilities
{
	public class Tools
	{

        public static bool esMultiplo(int n1, int n2)
        {
            try
            {
                if (n1 > 0 && n2 > 0)
                {
                    int result = n2 % n1;
                    if (result == 0)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

		public static int Mayor(int[]numeros)
		{
			// Primero asignamos al entero mayor el primer
			// elemento del array
			int mayor = numeros[0];
			//Ahora recorremos el array como en el ejemplo 1
			for(int i = 0 ; i< numeros.Length ; i++)
			// Vamos preguntando en cada posicion del array si el elemento que contiene es mayor que la variable "mayor".
			if(numeros[i] > mayor)
			// Si se cumple, entonces asignamos a mayor este nuevo elemento.
				 mayor = numeros[i];
			// Cuando terminamos de recorrer el array, retornamos la variable mayor.
			return mayor;
		}

        public static int Menor(int[] numeros)
        {           
            int menor = numeros[0];
  
            for (int i = 0; i < numeros.Length; i++)

                if (numeros[i] < menor)
                 
                    menor = numeros[i];
           
            return menor;
        }
	}
}
