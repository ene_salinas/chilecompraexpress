﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace ChileCompraExpress.Utilities
{
    public class Mailer
    {

        MailMessage message = new MailMessage();
        SmtpClient clienteSmtp;
        Attachment at ;
        int puerto;
        string user, password;

        //public Mailer(string user,string password,int puerto,Attachment att)
        //{
        //    this.user = user;
        //    this.password = password;
        //    this.puerto = puerto;
        //    this.at = att;
            
        //}
 
        public bool validarEmail(string email)
        {
                string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
 
                if (Regex.IsMatch(email, expresion))
                {
                    if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                    { return true; }
                    else
                    { return false; }
                }
                else
                { return false; }
        }
 
        public void ConfigurarMail(string from, string usuario, string contraseña, int puerto_salida, string smtp)
        {
                message = new MailMessage();
 
                message.From = new MailAddress(from);
 
                clienteSmtp = new SmtpClient(smtp);
 
                user = usuario;
 
                password = contraseña;
 
                clienteSmtp.Port = puerto_salida;
 
        }

        public bool EnviarMail(string to, List<string> cc, string asunto, string mensaje, string ruta_archivo_adjunto)
        {

            try
            {
                if (!String.IsNullOrEmpty(ruta_archivo_adjunto))
                {
                    at = new Attachment(ruta_archivo_adjunto);
                    message.Attachments.Add(at);
                }

                if (cc != null && cc.Count > 0)
                {
                    for (int i = 0; i < cc.Count; i++)
                    {
                        message.CC.Add(cc.ElementAt(i));
                    }
                }

                if (!String.IsNullOrEmpty(to))
                {
                    message.To.Add(to);
                }

                message.Subject = asunto;

                message.IsBodyHtml = true; //el texto del mensaje lo pueden poner en HTML y darle formato

                message.Body = mensaje;

                //Establezco que usare seguridad (ssl = Secure Sockets Layer) 
                clienteSmtp.EnableSsl = true;

                clienteSmtp.UseDefaultCredentials = false;

                clienteSmtp.Credentials = new NetworkCredential(user, password);

                clienteSmtp.Send(message);

                return true;

            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }
    }
}
