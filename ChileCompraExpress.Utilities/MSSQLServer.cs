using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Configuration;

namespace ChileCompraExpress.Utilities
{
    public class MSSQLServer : IBaseDeDatos
    {
        OleDbConnection conn;


        public MSSQLServer()
            : base()
        {
 
        }

        public object connect(string bd)
        {
            try
            {
                string mssql = bd;

                conn = new OleDbConnection(mssql);

                conn.Open();

                return (object)conn;
            }
            catch (Exception ex)
            {
                throw (ex); 
            }
        }

        public DataSet getData(IProcedure objCmd)
        {
            try
            {
                DataSet ds = new DataSet();
                OleDbDataAdapter da = new OleDbDataAdapter((OleDbCommand)objCmd.addObjectCommand());                
                da.Fill(ds);
                da.SelectCommand.Connection.Close();

                return ds;


            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void closeConn()
        {
            try
            {
                conn.Close();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        

    }
}
