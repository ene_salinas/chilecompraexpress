using System;
using System.Collections.Generic;
using System.Text;
using System.Data;


namespace ChileCompraExpress.Utilities
{
    public interface IBaseDeDatos
    {

        object connect(string bd);
        DataSet getData(IProcedure proc);
        void closeConn();        

    }
}
