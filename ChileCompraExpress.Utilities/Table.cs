﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Utilities
{
    public class Table
    {
        public int Id { get; set; }        
        private List<Row> Rows = new List<Row>();
        public string Attribute { get; set; }
        public int[] Position { get; set; }

        public void addRow(Row row)
        {
            Rows.Add(row);
        }

        public List<Row> getRows()
        {
            return this.Rows;
        }
    }
}
