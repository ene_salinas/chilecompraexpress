﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Utilities
{
    public interface ITable
    {
         string buildHeader();
         string buildBody();


    }
}
