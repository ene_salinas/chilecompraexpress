using System;
using System.Collections.Generic;
using System.Text;


namespace ChileCompraExpress.Utilities
{
    public interface IProcedure
    {
        object addObjectCommand();
        object[] getParameter();

    }
}
