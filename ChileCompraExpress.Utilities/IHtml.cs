﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Utilities
{
    public interface IHtml
    {

         

        /*string buildAttributeStyle();*/
        string buildAttributeClass();
        string buildAttributeId();
        string tagTableOpen();
        string tagTrOpen();
        string tagTdOpen();
        string tagTableClose();
        string tagTrClose();
        string tagTdClose();

        /////////ATRIBUTOS DE TAG'S////////////

        void setBorder(string border);
        void setAtt(string attr);


    }
}
