﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Utilities
{
    public class Row
    {
        public string Valor { get; set; }
        public string Attribute { get; set; }
        public string Cssclass { get; set; }
        public string HtmlId { get; set; }
        public string Rowspan { get; set; }
        public string Columnspan { get; set; }
        private List<Column> columns = new List<Column>();

        public void addColumn(Column column)
        {
            columns.Add(column);
        }

        public List<Column> getColumns()
        {
            return columns; 
        }
    }
}
