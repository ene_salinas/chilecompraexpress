﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Utilities
{
    public class TagHtml:IHtml
    {
        string FontSize { get; set; }
        string FontFamily { get; set; }
        string FontStyle { get; set; }
        string Display { get; set; }
        string Height { get; set; }
        string Width { get; set; }
        string Border { get; set; }
        string BorderTable { get; set; }
        string Background { get; set; }
        string Margin { get; set; }
        string Padding { get; set; }
        string Visibility { get; set; }
        string IdTag { get; set; }
        string ClassTag { get; set; }
        string ColSpan { get; set; }
        string Rowspan { get; set; }
        string Attr { get; set; }

        public string buildAttributesTags()
        {
            string str = "";

            if (Attr != null)
            {
                str += (String.IsNullOrEmpty(Attr)) ? "" : " " + Attr + " ";
            }

            return str;
        }

        public string buildAttributesBorder()
        {
            string str = "";

            if (BorderTable != null)
            {
                str += (String.IsNullOrEmpty(BorderTable)) ? "" : " border='" + BorderTable + "' ";
            }

            return str;
        }

        
        public string buildAttributeRowSpan()
        {
            string att = "";

            if(Rowspan != null)
            {
                att = " rowspan='" + Rowspan + "'";
            }

            return att;
        }

        public string buildAttributeColSpan()
        {
            string att = "";

            if (ColSpan != null)
            {
                att = " colspan='" + ColSpan + "'";
            }

            return att;
        }

        public string buildAttributeClass()
        {
            string style = "";

            if (ClassTag != null)
            {
                style = " class = '" + ClassTag + "'";
            }

            return style;
        }

        public string buildAttributeId()
        {
            string id = "";
            if (IdTag != null)
            {
                id = " id = '" + IdTag + "'";
            }
            return id;
        }

        public string tagTableOpen()
        {
            return "<table " + this.buildAttributesTags() + this.buildAttributesBorder() + this.buildAttributeId() + buildAttributeClass() +  ">";
        }

        public string tagTrOpen()
        {
            return "<tr " + this.buildAttributesTags() + this.buildAttributeId() + buildAttributeClass() +  ">";
        }

        public string tagTdOpen()
        {
            return "<td " + this.buildAttributesTags() + this.buildAttributeColSpan() + this.buildAttributeRowSpan() + this.buildAttributeId() + buildAttributeClass() +  ">";
        }

        public string tagTableClose()
        {
            return "</table>";
        }

        public string tagTrClose()
        {
            return "</tr>";
        }

        public string tagTdClose()
        {
            return "</td>";
        }
        
        public void setBorder(string border)
        {
            this.BorderTable = border;
        }

        public void setAtt(string att)
        {
            this.Attr = att;
        }
    }
}
