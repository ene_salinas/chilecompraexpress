﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.DataAccess;

namespace ChileCompraExpress.Business
{
    public class RegionesBsn
    {
        public string insertRegion(string sesion, string userCode, string idregiones)
        {
            RegionesDao dao = new RegionesDao();
            return dao.insertRegion(sesion, userCode, idregiones);
        }
    }
}
