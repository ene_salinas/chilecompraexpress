﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Entities;
using ChileCompraExpress.DataAccess;

namespace ChileCompraExpress.Business
{
    public class CategoriaBsn
    {
        private CategoriaDao categoriaDao = new CategoriaDao();

        public List<Categoria> getCategorias(TipoCategoria cat)
        {
            return this.categoriaDao.getCategorias(cat);
        }

        public List<SubCategoria> getSubCategoriasN2(int id)
        {
            Categoria cat = this.categoriaDao.getSubCategoriasN2(id);

            //List<Categoria> categorias = new List<Categoria>();

            //Categoria categ = null;

            //for (int i = 0; i < cat.subCategoriasN2.Count; i++)
            //{
            //    categ = new Categoria();
            //    categ.subCategoriasN2 = cat.subCategoriasN2;
            //    categ.subCategoriasN3 = getSubCategoriasN3(cat.subCategoriasN2.ElementAt(i).Id);
            //    categorias.Add(categ);
            //}

            return cat.getSubCategoriasN2();
        }
        
        public List<SubCategoria> getSubCategoriasN3(int id)
        {

            List<SubCategoria> cat = this.categoriaDao.getSubCategoriasN3(id);
            return cat;
        }
    }
}
