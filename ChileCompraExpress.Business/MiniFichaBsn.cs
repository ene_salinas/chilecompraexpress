﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Entities;
using ChileCompraExpress.DataAccess;

namespace ChileCompraExpress.Business
{
    public class MiniFichaBsn
    {
        MiniFichaDao miniDao = new MiniFichaDao();

        public Producto Detalles(Producto prd)
        {
            return miniDao.getMiniFicha(prd);
        }

        public List<Producto> ExportExcel(Producto prd,string idSession)
        {
            return miniDao.ExportExcel(prd,idSession);
        }
    }
}
