﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.DataAccess;
using ChileCompraExpress.Entities;

namespace ChileCompraExpress.Business
{
    public class OfertasBsn
    {
        private OfertasDao ofertaDao = new OfertasDao();

        public IEnumerable<Ofertas> getOfertasCarrusel(string sesion)
        {

            return this.ofertaDao.getOfertasCarrusel(sesion);
        }
    }
}
