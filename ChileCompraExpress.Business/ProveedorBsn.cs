﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Entities;
using ChileCompraExpress.DataAccess;

namespace ChileCompraExpress.Business
{
    public class ProveedorBsn
    {
        public Proveedor[] proveedores(int idProducto, int idConvenio, string session)
        {
            ProveedorDao proveedorDao = new ProveedorDao();
            return proveedorDao.getProveedores(idProducto, idConvenio, session);
        }

        public Proveedor[] proveedoresByText(string text)
        {
            ProveedorDao proveedorDao = new ProveedorDao();
            return proveedorDao.getProveedoresByText(text);            
        }

    }
}
