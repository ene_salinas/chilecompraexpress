﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Entities;
using ChileCompraExpress.DataAccess;

namespace ChileCompraExpress.Business
{
    public class InformacionBsn
    {
        public IEnumerable<Informacion> GetInformaciones()
        {
            try
            {
                InformacionDao infoDao = new InformacionDao();
                return infoDao.GetInformaciones();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Informacion GetInformacionesById(int id)
        {
            try
            {
                InformacionDao infoDao = new InformacionDao();
                return infoDao.GetInformacionesById(id);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
