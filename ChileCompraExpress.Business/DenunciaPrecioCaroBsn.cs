﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ChileCompraExpress.DataAccess;
using ChileCompraExpress.Entities;
using ChileCompraExpress.Utilities;
using System.Configuration;

namespace ChileCompraExpress.Business
{
    public class DenunciaPrecioCaroBsn
    {

        public List<string> obtenerCorreosProveedor(int idEmpresa)
        {
            try
            {
                DenunciaPrecioCaroDao denDao = new DenunciaPrecioCaroDao();
                return denDao.obtenerCorreosProveedor(idEmpresa);
            }
            catch (Exception ex)
            {
                throw(ex);
            }
        }

        public string SaveDenuncia(Denuncia denuncia)
        {
            try
            {
                string mensaje;
                int idPrecioCaro = 0;

                //OBTIENE LOS DATOS DEL PRODUCTO
                Producto producto = new Producto();
                producto.IdProveedor = denuncia.IdProveedor;
                producto.IdConvenio = denuncia.IdConvenio;
                producto.Id = denuncia.IdProducto;
                ProductoBsn prodBsn = new ProductoBsn();
                Producto prodReturn = prodBsn.GetBaseProducto(producto);

                denuncia.NombreProducto = prodReturn.Nombre;
                denuncia.NombreProveedor = prodReturn.Proveedor;

                DenunciaPrecioCaroDao denDao = new DenunciaPrecioCaroDao();
                if (denDao.SaveDenuncia(denuncia, out mensaje,out idPrecioCaro))
                {
                    denDao.GenerarTicket(idPrecioCaro);
                }

                return mensaje;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
