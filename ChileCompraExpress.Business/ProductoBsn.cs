﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Entities;
using ChileCompraExpress.DataAccess;

namespace ChileCompraExpress.Business
{
    public class ProductoBsn
    {
        private ProductoDao productoDao = new ProductoDao();

        public IEnumerable<Producto> getProductosRecienVisitados(string codeUser)
        {
            return productoDao.getProductosRecienVisitados(codeUser);
        }

        public IEnumerable<Producto> getProductosMasVisitados(string codeUser)
        {
            return productoDao.getProductosMasVisitados(codeUser);
        }




        public Entities.Producto InsertarHit(Producto prd)
        {
            try
            {
                ProductoDao dao = new ProductoDao();
                return dao.InsertarHit(prd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Producto GetBaseProducto(Producto prd)
        {
            try
            {
                ProductoDao dao = new ProductoDao();
                return dao.GetBaseProducto(prd);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public string getProductosMasVisitadosResultPage(string user)
        {
            try
            {
                //EXAMPLE"IDP:922364|IDP:848476";
                ProductoDao dao = new ProductoDao();
                IEnumerable<Producto> productos = dao.getProductosMasVisitadosResultPage(user);
                string resultado = "";
                
                for (int i = 0; i < productos.ToList().Count; i++)
                {
                    resultado += "IDP:";

                    resultado += productos.ElementAt(i).Id.ToString();

                    if ((productos.ToList().Count > 0) && (productos.ToList().Count -1 != i))
                    {
                        resultado += "|";
                    }
                }

                return resultado;

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public IEnumerable<Producto> getProductosLosMasVisitados(string codeUser)
        {
            return productoDao.getProductosLosMasVisitados(codeUser);
        }
    }
}
