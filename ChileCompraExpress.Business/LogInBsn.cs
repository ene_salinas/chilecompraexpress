﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Entities;
using ChileCompraExpress.DataAccess;

namespace ChileCompraExpress.Business
{
    public class LogInBsn
    {
        private LogInDao logDao = new LogInDao();

        /// <summary>
        /// Obtiene un Id Session por defecto , solo uso para pruebas
        /// </summary>
        /// <returns></returns>
        public LogIn getIdSessionTest()
        {
            LogIn logIn = new LogIn();
            logIn.IdSession = logDao.getIdSession();

            return logIn;
        }
        
    }
}
