﻿namespace ChileCompraExpress.Common.Entities
{
    public enum CategoriaTipo
    { 
        Productos,
        Servicios,
        Salud,
        Todos,
        Ninguno
    }
}
