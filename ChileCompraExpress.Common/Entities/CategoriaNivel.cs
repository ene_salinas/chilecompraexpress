﻿namespace ChileCompraExpress.Common.Entities
{
    public enum CategoriaNivel
    { 
        Primero,
        Segundo,
        Tercero,
        Todos,
        Ninguno
    }
}
