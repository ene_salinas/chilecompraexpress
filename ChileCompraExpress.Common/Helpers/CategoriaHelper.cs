﻿using ChileCompraExpress.Common.Entities;

namespace ChileCompraExpress.Common.Helpers
{
    public static class CategoriaHelper 
    {
        public static CategoriaTipo GetType(int type)
        {
            switch (type)
            {
                case 1:
                    return CategoriaTipo.Productos;
                case 2:
                    return CategoriaTipo.Servicios;
                case 4:
                    return CategoriaTipo.Salud;
                default:
                    return CategoriaTipo.Ninguno;
            }
        }

        public static int GetType(CategoriaTipo type)
        {
            switch(type)
            {
                case CategoriaTipo.Productos:
                    return 1;
                case CategoriaTipo.Servicios:
                    return 2;
                case CategoriaTipo.Salud:
                    return 3;
                default:
                    return 0;
            }
        }

        public static CategoriaNivel GetLevel(int level)
        {
            switch (level)
            {
                case 1:
                    return CategoriaNivel.Primero;
                case 2:
                    return CategoriaNivel.Segundo;
                case 3:
                    return CategoriaNivel.Tercero;
                default:
                    return CategoriaNivel.Ninguno;
            }
        }
    }
}
