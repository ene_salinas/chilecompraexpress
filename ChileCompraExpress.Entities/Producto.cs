﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Entities
{
    public class Producto
    {

        private List<Sello> sellos = new List<Sello>();

        /// <summary>
        /// Agregar un sello al producto
        /// </summary>
        /// <param name="prod"></param>
        public void addSello(Sello sello)
        {
            sellos.Add(sello);
        }



        /// <summary>
        /// Retorna los sellos asociados al producto
        /// </summary>
        /// <returns></returns>
        public List<Sello> getSellos()
        {
            return this.sellos;
        }

        public string Moneda { get; set; }

        public int Id { get;  set; }
        public string PrecioAnterior { get;  set; }
        public string Precio { get;  set; }
        public string IdUser { get; set; }
        public string CodeUsrOrg { get; set; }

        public int IdMarca { get; set; }
        public string Modelo { get; set; }

        private string filtroGrl;

        public string FiltroGrl
        {
            get { return filtroGrl; }
            set { filtroGrl = value; }
        }


        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }

        private int idRegion;

        public int IdRegion
        {
            get { return idRegion; }
            set { idRegion = value; }
        }

        private string region;

        public string Region
        {
            get { return region; }
            set { region = value; }
        }

        private int idRango;

        public int IdRango
        {
            get { return idRango; }
            set { idRango = value; }
        }

        private string rango;

        public string Rango
        {
            get { return rango; }
            set { rango = value; }
        }

        private int idProveedor;

        public int IdProveedor
        {
            get { return idProveedor; }
            set { idProveedor = value; }
        }

        private string proveedor;

        public string Proveedor
        {
            get { return proveedor; }
            set { proveedor = value; }
        }


        private int idConvenio;

        public int IdConvenio
        {
            get { return idConvenio; }
            set { idConvenio = value; }
        }

        private string convenio;

        public string Convenio
        {
            get { return convenio; }
            set { convenio = value; }
        }



        //public int Descuento
        //{
        //    get
        //    {
        //        if (PrecioAnterior == 0) return 0;

        //        int intref = (int)(((PrecioAnterior - Precio) / PrecioAnterior) * 100);
        //        return intref / 5 * 5;
        //    }
        //}

        public int Hits { get; set; }

        public string UrlImagen
        {
            get;
            set;
        }

        public string Descripcion { get; set; }

        public List<Proveedor> proveedores = new List<Proveedor>();

        public void AddProveedor(Proveedor proveedor)
        {
            proveedores.Add(proveedor);
        }

        public List<Proveedor> GetProveedores()
        {
            return this.proveedores;
        }

        public int IdFormato { get; set; }
    }
}
