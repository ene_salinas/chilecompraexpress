﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Entities
{
    public class Proveedor
    {

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Nombre { get; set; }
        public int Ranking { get; set; }
        public string PrecioAnterior { get; set; }
        public string PrecioActual { get; set; }
        public string Regiones { get; set; }
        public int Sello { get; set; }
        public int IdProducto { get; set; }
        public int IdConvenio { get; set; }
        public List<Sello> Sellos = new List<Sello>();
        public string Moneda { get; set; }
    }
}
