﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Entities
{
    public class Sello
    {
        public int IdSello { get; set; }
        public string PathSello { get; set; }
        public string NombreSello { get; set; }
        public string DescripcionSello { get; set; }
    }
}
