﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ChileCompraExpress.Entities
{
    public class Usuario
    {

        public Region[] Regiones { get; set; }
        public int AcceptEmail { get; set; }
        public int AcceptPager { get; set; }
        public int AcceptSMS { get; set; }
        public int ApplyDST { get; set; }
        public string Code { get; set; }
        public DateTime DateTimeLoged { get; set; }
        public string Email { get; set; }
        public string EmailAlternative { get; set; }
        public int FailedAttempts { get; set; }
        public string Fax { get; set; }
        public string FirstName { get; set; }
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }
        public bool IsTest { get; set; }
        
        public int languageCode { get; set; }
        public string LastName { get; set; }
        public List<UserOrganization> ListUserOrganizations { get; set; }
        public string Login { get; set; }
        public int MailFormat { get; set; }
        public string Mobile { get; set; }
        public Organization Organization { get; set; }
        public string OrganizationDefault { get; set; }
        public string OrgCodeList { get; set; }
        public string PagerAddress { get; set; }
        public string Password { get; set; }
        public DateTime PasswordChanged { get; set; }
        public string Phone { get; set; }
        public string Position { get; set; }
        public long SessionId { get; set; }
        public string TaxId { get; set; }
        public int TimeZone { get; set; }
        public string UserDefined1 { get; set; }
        public string UserDefined2 { get; set; }
        public string UserDefined3 { get; set; }
        [XmlIgnore]
        public Dictionary<int, UserRole> UserRoles { get; set; }



    }
}
