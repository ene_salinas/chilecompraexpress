﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Entities
{
    public class Denuncia
    {
        public int Id { get; set; }
        public int IdProducto { get; set; }
        public int IdProveedor { get; set; }
        public int IdUsuario { get; set; }
        public string NombreProducto { get; set; }
        public string DescripcionProducto { get; set; }
        public string Observacion { get; set; }
        public string FechaIngreso { get; set; }

        public int IdConvenio { get; set; }
        public string NombreProveedor { get; set; }
        public string CodeEnt { get; set; }
        public string NameEnt { get; set; }
        public string UserName { get; set; }
       
    }
}
