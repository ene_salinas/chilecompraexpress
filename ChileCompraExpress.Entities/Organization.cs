﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace ChileCompraExpress.Entities
{
    [Serializable]
    public class Organization
    {
        //[DebuggerNonUserCode]
        //public Organization();

        public string Code { get; set; }
        public string entCode { get; set; }
        public string Name { get; set; }

        

        public int orgID { get; set; }
        public short orgIsActive { get; set; }
        public string orgCode { get; set; }
        public short orgIsTest { get; set; }
        public string orgEnterprise { get; set; }
        public string orgParentOrganization { get; set; }
        public int orgClass { get; set; }
        public int orgType { get; set; }
        public int orgSubType { get; set; }
        public string orgName { get; set; }
        public string orgLegalName { get; set; }
        public string orgTaxID { get; set; }
        public string orgMarketplaceID { get; set; }
        public string orgActivity { get; set; }
        public string orgUrl { get; set; }
        public string orgUserDefined1 { get; set; }
        public string orgUserDefined2 { get; set; }
        public string orgUserDefined3 { get; set; }
        public short orgInformation { get; set; }
        public long rowNumber { get; set; }
        public string entName { get; set; }
        public string orgName2 { get; set; }
    }
}
