﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace ChileCompraExpress.Entities
{
    [Serializable]
    public abstract class FormatInfoLanguage
    {
        protected const string LANG_SEPARATOR_BETWEEN_DATE_AND_TIME = " ";

        protected readonly string _dateFormat;
        protected readonly string _dateFormatMinutePrecision;
        protected readonly NumberFormatInfo _NFIDefault;
        protected readonly string _timeFormatMinutePrecision;

        //protected FormatInfoLanguage(char chrThousandSeparator, char chrDecimalSeparator, string strDateFormat);
    }
}
