﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Entities
{
    public class Informacion
    {
        public int Id { get; set; }
        public string Texto { get; set; }
        public string Titulo { get; set; }
        public string FechaCreacion { get; set; }
    }
}
