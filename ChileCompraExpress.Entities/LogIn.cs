﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Entities
{
    public class LogIn
    {
        private string idSession;

        public string IdSession
        {
            get { return idSession; }
            set { idSession = value; }
        }
    }
}
