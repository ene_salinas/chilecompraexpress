﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Entities
{
    public class Region
    {
        public int Existe { get; set; }
        public string Nombre { get; set; }
        public int idRegion { get; set; }
        public string Abreviacion { get; set; }
        public string Numero { get; set; }
    }
}
