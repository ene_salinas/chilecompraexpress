﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Entities
{
    public class Categoria
    {
        public List<SubCategoria> subCategoriasN2 = new List<SubCategoria>();
        public List<SubCategoria> subCategoriasN3 = new List<SubCategoria>();
        public TipoCategoria tipoCategoria = new TipoCategoria();
                

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string NombreTruncate { get; set; }

        public void addSubCategoriaN3(SubCategoria subCat)
        {
            this.subCategoriasN3.Add(subCat);
        }

        public List<SubCategoria> getSubCategoriasN3()
        {
            return this.subCategoriasN3;
        }

        public void addSubCategoriaN2(SubCategoria subCat)
        {
            this.subCategoriasN2.Add(subCat);
        }

        public List<SubCategoria> getSubCategoriasN2()
        {
            return this.subCategoriasN2;
        }
        
        
    }
}
