﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.Entities
{
    public class Ofertas
    {

        public string TipoMoneda { get; set; }
        private int idProveedor;

        public int IdProveedor
        {
            get { return idProveedor; }
            set { idProveedor = value; }
        }

        private int idProducto;

        public int Id
        {
            get { return idProducto; }
            set { idProducto = value; }
        }

        private int idConvenioMarco;

        public int IdConvenio
        {
            get { return idConvenioMarco; }
            set { idConvenioMarco = value; }
        }

        private string urlImagen;

        public string UrlImagen
        {
            get { return urlImagen; }
            set { urlImagen = value; }
        }

        private string nombreProducto;

        public string Nombre
        {
            get { return nombreProducto; }
            set { nombreProducto = value; }
        }

        private string precioOrigen;

        public string PrecioAnterior
        {
            get { return precioOrigen; }
            set { precioOrigen = value; }
        }

        private string precio;

        public string Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        private string proveedor;

        public string Proveedor
        {
            get { return proveedor; }
            set { proveedor = value; }
        }

        private int idTipoCategoria;

        public int IdTipoCategoria
        {
            get { return idTipoCategoria; }
            set { idTipoCategoria = value; }
        }

        private int idFormato;

        public int IdFormato
        {
            get { return idFormato; }
            set { idFormato = value; }
        }





        public int Descuento { get; set; }
        
        
        
        
    }
}
