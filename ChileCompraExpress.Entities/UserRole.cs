﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace ChileCompraExpress.Entities
{
    [Serializable]
    public class UserRole
    {
        //[DebuggerNonUserCode]
        //public UserRole();

        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public string Organization { get; set; }
        public int Role { get; set; }
        public string User { get; set; }
    }
}
