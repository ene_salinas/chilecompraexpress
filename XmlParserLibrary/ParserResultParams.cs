﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.XmlParserLibrary
{
    public class ParserResultParams
    {
        public int count { get; set; }
        public int minPrice { get; set; }
        public int maxPrice { get; set; }
        public string suggestionHtml { get; set; }
    }
}
