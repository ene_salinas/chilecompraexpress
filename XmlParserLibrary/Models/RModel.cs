﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.XmlParserLibrary.Models
{
    public class RModel
    {
        public string N;
        public string L;
        public TagModel UObject = new TagModel();
        public TagModel UEObject = new TagModel();
        public TagModel UDObject = new TagModel();
        public TagModel TObject = new TagModel();
        public TagModel RKObject = new TagModel();
        public TagModel CRAWLDATEObject = new TagModel();
        public TagModel ENT_SOURCEObject = new TagModel();
        public TagModel SObject = new TagModel();
        public TagModel LANGObject = new TagModel();
        public FSModel FSObject = new FSModel();
        public List<MTModel> MTObjects = new List<MTModel>();
        public HASModel HASObject = new HASModel();
        public HNModel HNObject = new HNModel();
    }
}
