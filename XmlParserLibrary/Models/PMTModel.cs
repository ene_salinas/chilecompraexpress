﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.XmlParserLibrary.Models
{
    public class PMTModel
    {
        public string NM;
        public string DN;
        public string IR;
        public string T;
        public List<PVModel> PVObjects = new List<PVModel>();
    }
}
