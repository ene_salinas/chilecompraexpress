﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChileCompraExpress.XmlParserLibrary.Models
{
    public class SearchParamModel
    {
        public string name;
        public string value;
        public string original_value;
    }
}
