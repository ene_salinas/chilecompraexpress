﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.XmlParserLibrary.Models;
using System.Xml.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace ChileCompraExpress.XmlParserLibrary
{
    public class XmlParser
    {

        

        public ParserResultParams ParsebyUrl(string url, string sortBy, bool sortReverse, int starts, int take, out ParmModel parm, out List<RModel> products, out List<SearchParamModel> searchParams)
        {
            int elementsCount = 0;
            int minPrice = int.MaxValue;
            int maxPrice = int.MinValue;

            if (sortBy != "")
                url = url + "&start=0&num=900";
            else
            {
                url = url + "&start="+starts+"&num="+take;
            }

            XElement xelement = null;

            using (WebClient webClient = new WebClient())
            {
                webClient.Encoding = System.Text.Encoding.UTF8;                
                string xml = webClient.DownloadString(url);                               
                xelement = XElement.Load(url);
            }

            var suggestionHtml = (from p in xelement.Elements("Spelling")                                 
                                 select p).ToList();

            string suggestion = (suggestionHtml != null && suggestionHtml.Count > 0) ? suggestionHtml[0].Value.Replace("<b>", "") : "1";
            suggestion = (suggestionHtml != null && suggestionHtml.Count > 0) ? suggestion.Replace("<i>", "") : "";
            suggestion = (suggestionHtml != null && suggestionHtml.Count > 0) ? suggestion.Replace("</i>", "") : "";
            suggestion = (suggestionHtml != null && suggestionHtml.Count > 0) ? suggestion.Replace("</b>", "") : "";
                              

            searchParams = (from p in xelement.Elements("PARAM")
                           select new SearchParamModel()
                           {
                               name = p.Attribute("name").Value,
                               value = p.Attribute("value").Value,
                               original_value = p.Attribute("original_value").Value
                           }).ToList();

            // getting RES
            IEnumerable<XElement> RES = from p in xelement.Elements("RES") select p;

            var M = RES.Elements("M").FirstOrDefault();
            if (M != null)
                elementsCount = Convert.ToInt32(M.Value);

            // Getting PARMs Consindering RES will always be One.
            IEnumerable<XElement> PARM = from p in RES.Elements("PARM") select p;

            // Getting PMTs from PARM
            IEnumerable<XElement> PMTs = from p in PARM.Elements("PMT") select p;

            // assuming PMTs will be more then one. 
            parm = new ParmModel();
            foreach (XElement PMT in PMTs)
            {

                PMTModel PMTObject = new PMTModel();

                PMTObject.DN = PMT.Attribute("DN").Value;
                PMTObject.IR = PMT.Attribute("IR").Value;
                PMTObject.NM = PMT.Attribute("NM").Value;
                PMTObject.T = PMT.Attribute("T").Value;

                foreach (XElement PV in PMT.Elements("PV"))
                {

                    PVModel PVObject = new PVModel();
                    PVObject.C = PV.Attribute("C").Value;
                    PVObject.H = PV.Attribute("H").Value;
                    PVObject.L = PV.Attribute("L").Value;
                    PVObject.V = PV.Attribute("V").Value;
                    PMTObject.PVObjects.Add(PVObject);
                }

                parm.PMTObjects.Add(PMTObject);

            }

            // fill out the Products
            IEnumerable<XElement> Rs = from p in RES.Elements("R") select p;

            // expecting it to be multiple
            var objects = new List<KeyValuePair<string, RModel>>();
            foreach (XElement R in Rs)
            {
                string sortingValue = string.Empty;

                RModel RObject = new RModel();
                RObject.N = R.Attribute("N").Value;
                RObject.L = R.Attribute("L") != null ? R.Attribute("L").Value : "";

                RObject.CRAWLDATEObject.innerText = R.Element("CRAWLDATE") != null ? R.Element("CRAWLDATE").Value : "";
                RObject.ENT_SOURCEObject.innerText = R.Element("ENT_SOURCE").Value;
                RObject.FSObject.NAME = R.Element("FS").Attribute("NAME").Value;
                RObject.FSObject.VALUE = R.Element("FS").Attribute("VALUE").Value;

                foreach (XElement MT in R.Elements("MT"))
                {

                    MTModel MTObject = new MTModel();
                    MTObject.N = MT.Attribute("N").Value;
                    MTObject.V = MT.Attribute("V").Value;

                    if ((MTObject.N == sortBy) && string.IsNullOrEmpty(sortingValue))
                        sortingValue = MTObject.V;

                    if (MTObject.N == "PM")
                    {
                        string value = MTObject.V;
                        decimal price = Decimal.Parse(value);
                        int intPrice = Convert.ToInt32(price);
                        if (intPrice > 0)
                        {
                            if (intPrice > maxPrice)
                                maxPrice = intPrice;
                            if (intPrice < minPrice)
                                minPrice = intPrice;
                        }                        
                        
                    }



                    if (MTObject.N == "DP")
                    {
                        string[] lines = Regex.Split(MTObject.V, "<CR/LF>");
                        //SETEA EL VALOR CON LA DESCRIPCION DEL PRODUCTO
                        MTObject.V = lines[lines.Length - 2];
                       
                    }

                    //if (MTObject.N == "NOMPROV")
                    //{
                    //    string[] lines = Regex.Split(MTObject.V, "#");
                    //    MTObject.V = lines[0];
                    //}

                    RObject.MTObjects.Add(MTObject);
                }

                RObject.SObject.innerText = R.Element("S").Value;
                RObject.LANGObject.innerText = R.Element("LANG").Value;

                XElement HASElement = R.Element("HAS");

                CModel CObject = new CModel();
                CObject.CID = HASElement.Element("C").Attribute("CID").Value;
                CObject.ENC = HASElement.Element("C").Attribute("ENC").Value;
                CObject.SZ = HASElement.Element("C").Attribute("SZ").Value;

                RObject.HASObject.C = CObject;

                RObject.HNObject.innerText = R.Element("HN") != null ? R.Element("HN").Value : "";

                objects.Add(new KeyValuePair<string,RModel>(sortingValue, RObject));
            }
            if (sortBy != "")
            {
                if (sortReverse)
                {
                    if (sortBy == "PM")
                        products = objects.OrderByDescending(x => Convert.ToDecimal(x.Key)).Select(x => x.Value).Skip(starts).Take(take).ToList();
                    else
                        products = objects.OrderByDescending(x => x.Key).Select(x => x.Value).Skip(starts).Take(take).ToList();
                }
                else
                {
                    if (sortBy == "PM")
                        products = objects.OrderBy(x => Convert.ToDecimal(x.Key)).Select(x => x.Value).Skip(starts).Take(take).ToList();
                    else
                        products = objects.OrderBy(x => x.Key).Select(x => x.Value).Skip(starts).Take(take).ToList();
                }
            }
            else
            {
                products = objects.Select(x => x.Value).ToList();
            }

            return new ParserResultParams() 
            {
                count = elementsCount,
                minPrice = minPrice,
                maxPrice = maxPrice,
                suggestionHtml = suggestion
            }; 
        }
    }
}
