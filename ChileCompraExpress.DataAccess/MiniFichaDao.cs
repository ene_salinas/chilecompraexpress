﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using ChileCompraExpress.Utilities;
using ChileCompraExpress.Entities;
using System.Data.OleDb;
using System.Data;

namespace ChileCompraExpress.DataAccess
{
    public class MiniFichaDao
    {

        public DataSet getSellosProveedor(int idConvenio, int idProveedor)
        {
            try
            {
                IBaseDeDatos mssql = new MSSQLServer();
                OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                string[] paramName = { "id_proveed", "id_convenio" };
                string[] paramValue = { idProveedor.ToString(), idConvenio.ToString() };
                OleDbType[] type = { OleDbType.Integer, OleDbType.Integer };
                IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_GSA_prod_proveed_sello", paramName, paramValue, null, type, null);

                DataSet ds = mssql.getData(proc);
                mssql.closeConn();

                return ds;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public DataSet getSellosProducto(int idMarca, string modelo)
        {
            try
            {
                IBaseDeDatos mssql = new MSSQLServer();
                OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);


                string[] paramName = { "idMarca", "modelo" };
                string[] paramValue = { idMarca.ToString(), modelo };
                OleDbType[] type = { OleDbType.Integer, OleDbType.VarChar };
                IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_GSA_prod_sello", paramName, paramValue, null, type, null);

                DataSet ds = mssql.getData(proc);
                mssql.closeConn();

                return ds;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }



        public Producto getMiniFicha(Producto prd)
        {
            try
            {
                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true"))?true:false;
                Entities.Producto prod = null; 

                if (!datosdummies)
                {
                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);
                    
                    string[] paramName = { "idProducto", "convenio" , "idProveedor" };
                    string[] paramValue = { prd.Id.ToString(), prd.IdConvenio.ToString(), prd.IdProveedor.ToString() };
                    OleDbType[] type = { OleDbType.Integer, OleDbType.Integer, OleDbType.Integer };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getMiniFicha", paramName, paramValue, null, type, null);
                    DataSet ds = mssql.getData(proc);

                    mssql.closeConn();


                    if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                    {
                        prod = new Entities.Producto();
                        prod.Id = int.Parse(ds.Tables[0].Rows[0]["ID_PRODUCTO"].ToString());
                        prod.IdMarca = int.Parse(ds.Tables[0].Rows[0]["idMarca"].ToString());
                        prod.Modelo = ds.Tables[0].Rows[0]["Modelo"].ToString();
                        prod.Nombre = ds.Tables[0].Rows[0]["NOMBRE"].ToString();
                        prod.UrlImagen = ConfigurationSettings.AppSettings["UrlImagen"].ToString()
                                                    + ds.Tables[0].Rows[0]["IMAGEN"].ToString().Replace("../", ""); ;
                        prod.Descripcion = ds.Tables[0].Rows[0]["DESCRIPCION"].ToString();
                        prod.Marca = ds.Tables[0].Rows[0]["MARCA"].ToString();
                        prod.Convenio = ds.Tables[0].Rows[0]["NOMBRE_CONVENIO"].ToString();
                        prod.IdConvenio = int.Parse(ds.Tables[0].Rows[0]["ID_CONVENIO"].ToString());
                        prod.IdProveedor = int.Parse(ds.Tables[0].Rows[0]["ID_PROVEEDOR"].ToString());
                        

                        //SE ASOCIAN LOS SELLOS DEL PRODUCTO
                        DataSet dsSellosProducto = getSellosProducto(prod.IdMarca, prod.Modelo);

                        if (dsSellosProducto.Tables[0] != null && dsSellosProducto.Tables[0].Rows.Count > 0)
                        {
                            Sello sello;
                            for (int s = 0; s < dsSellosProducto.Tables[0].Rows.Count; s++)
                            {
                                sello = new Sello();
                                sello.IdSello = int.Parse(dsSellosProducto.Tables[0].Rows[s]["idSello"].ToString()); 
                                sello.NombreSello = dsSellosProducto.Tables[0].Rows[s]["Nombre"].ToString();                                
                                sello.PathSello = dsSellosProducto.Tables[0].Rows[s]["Path"].ToString();                                
                                sello.DescripcionSello = dsSellosProducto.Tables[0].Rows[s]["Descripcion"].ToString();
                                

                                //SE ASOCIA EL SELLO AL PRODUCTO
                                prod.addSello(sello);
                            }
                        }


                        Entities.Proveedor proveedor = new Entities.Proveedor();
                        proveedor.Id = int.Parse(ds.Tables[0].Rows[0]["ID_PROVEEDOR"].ToString());
                        proveedor.Nombre = ds.Tables[0].Rows[0]["NOMBRE_PROVEEDOR"].ToString();
                        proveedor.Regiones = ds.Tables[0].Rows[0]["REGIONES_DESPACHO"].ToString();
                        proveedor.PrecioAnterior = ds.Tables[0].Rows[0]["PRECIO"].ToString();
                        proveedor.PrecioActual = ds.Tables[0].Rows[0]["PRECIO_OFERTA"].ToString();
                        proveedor.Ranking = int.Parse(ds.Tables[0].Rows[0]["ESTRELLAS"].ToString()); 
                        proveedor.Sello = 1;

                        if (proveedor.PrecioActual == "" || proveedor.PrecioActual == "0")
                        {
                            proveedor.PrecioActual = proveedor.PrecioAnterior;
                            proveedor.PrecioAnterior = "";
                        }


                        //SE ASOCIAN LOS SELLOS DEL PROVEEDOR
                        DataSet dsSellos = getSellosProveedor(prod.IdConvenio, proveedor.Id);

                        if (dsSellos != null && dsSellos.Tables[0].Rows.Count > 0)
                        {
                            List<Sello> sellos = new List<Sello>();
                            Sello sello;
                            for (int j = 0; j < dsSellos.Tables[0].Rows.Count; j++)
                            {
                                sello = new Sello();
                                sello.IdSello = int.Parse(dsSellos.Tables[0].Rows[j]["ID_SELLO"].ToString());
                                sello.NombreSello = dsSellos.Tables[0].Rows[j]["NOMBRE_SELLO"].ToString();
                                sello.PathSello = dsSellos.Tables[0].Rows[j]["PATH_SELLO"].ToString();
                                sello.DescripcionSello = dsSellos.Tables[0].Rows[j]["DESC_SELLO"].ToString();
                                sellos.Add(sello);
                            }

                            proveedor.Sellos = sellos;

                        }

                        prod.AddProveedor(proveedor);
                    }

                }
                else
                {
                    prod = new Entities.Producto();
                    prod.Id = 1;
                    prod.Nombre = "nombre producto test";                    
                    prod.UrlImagen = "";
                    prod.Descripcion = "Descripcion del producto Descripcion del producto Descripcion del producto Descripcion del producto";
                    prod.Marca = "Marca del producto";
                    prod.Convenio = "Convenio del producto";                    

                    Entities.Proveedor proveedor = new Entities.Proveedor();
                    proveedor.Id = 132;
                    proveedor.Nombre = "nombre proveedor test";
                    proveedor.Regiones = "I,II,III,IV";
                    proveedor.PrecioAnterior = "200";
                    proveedor.PrecioActual = "100";
                    proveedor.Ranking = 2;
                    proveedor.Sello = 1;

                    if (proveedor.PrecioActual == "" || proveedor.PrecioActual == "0")
                    {
                        proveedor.PrecioActual = proveedor.PrecioAnterior;
                        proveedor.PrecioAnterior = "";
                    }

                    prod.AddProveedor(proveedor);
                    
                }

                return prod;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        public List<Producto> ExportExcel(Producto prd, string idSession)
        {
            try
            {
                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;
                Entities.Producto prod = null;
                List<Producto> productos = new List<Producto>();

                if (!datosdummies)
                {
                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                    string[] paramName = { "convenio", "IdSesion" };
                    string[] paramValue = { prd.IdConvenio.ToString(), idSession };
                    OleDbType[] type = {  OleDbType.Integer,OleDbType.VarChar };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getMiniFichaExcel", paramName, paramValue, null, type, null);
                    DataSet ds = mssql.getData(proc);
                    

                    mssql.closeConn();


                    if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                    {

                        for (int i = 0;i < ds.Tables[0].Rows.Count; i++)
                        {
                            prod = new Entities.Producto();
                            prod.Id = int.Parse(ds.Tables[0].Rows[i]["ID_PRODUCTO"].ToString());
                            prod.IdMarca = int.Parse(ds.Tables[0].Rows[i]["idMarca"].ToString());
                            prod.Modelo = ds.Tables[0].Rows[i]["Modelo"].ToString();
                            prod.Nombre = ds.Tables[0].Rows[i]["NOMBRE"].ToString();
                            prod.UrlImagen = ConfigurationSettings.AppSettings["UrlImagen"].ToString()
                                                        + ds.Tables[0].Rows[i]["IMAGEN"].ToString().Replace("../", ""); ;
                            prod.Descripcion = ds.Tables[0].Rows[i]["DESCRIPCION"].ToString();
                            prod.Marca = ds.Tables[0].Rows[i]["MARCA"].ToString();
                            prod.Convenio = ds.Tables[0].Rows[i]["NOMBRE_CONVENIO"].ToString();
                            prod.IdConvenio = int.Parse(ds.Tables[0].Rows[i]["ID_CONVENIO"].ToString());
                            prod.IdProveedor = int.Parse(ds.Tables[0].Rows[i]["ID_PROVEEDOR"].ToString());


                            //SE ASOCIAN LOS SELLOS DEL PRODUCTO
                            DataSet dsSellosProducto = getSellosProducto(prod.IdMarca, prod.Modelo);

                            if (dsSellosProducto.Tables[0] != null && dsSellosProducto.Tables[0].Rows.Count > 0)
                            {
                                Sello sello;
                                for (int s = 0; s < dsSellosProducto.Tables[0].Rows.Count; s++)
                                {
                                    sello = new Sello();
                                    sello.IdSello = int.Parse(dsSellosProducto.Tables[0].Rows[s]["idSello"].ToString());
                                    sello.NombreSello = dsSellosProducto.Tables[0].Rows[s]["Nombre"].ToString();
                                    sello.PathSello = dsSellosProducto.Tables[0].Rows[s]["Path"].ToString();
                                    sello.DescripcionSello = dsSellosProducto.Tables[0].Rows[s]["Descripcion"].ToString();


                                    //SE ASOCIA EL SELLO AL PRODUCTO
                                    prod.addSello(sello);
                                }
                            }


                            Entities.Proveedor proveedor = new Entities.Proveedor();
                            proveedor.Id = int.Parse(ds.Tables[0].Rows[i]["ID_PROVEEDOR"].ToString());
                            proveedor.Nombre = ds.Tables[0].Rows[i]["NOMBRE_PROVEEDOR"].ToString();
                            proveedor.Regiones = ds.Tables[0].Rows[i]["REGIONES_DESPACHO"].ToString();
                            proveedor.PrecioAnterior = ds.Tables[0].Rows[i]["PRECIO"].ToString();
                            proveedor.PrecioActual = ds.Tables[0].Rows[i]["PRECIO_OFERTA"].ToString();
                            proveedor.Ranking = int.Parse(ds.Tables[0].Rows[i]["ESTRELLAS"].ToString());
                            proveedor.Sello = 1;

                            if (proveedor.PrecioActual == "" || proveedor.PrecioActual == "0")
                            {
                                proveedor.PrecioActual = proveedor.PrecioAnterior;
                                proveedor.PrecioAnterior = "";
                            }


                            //SE ASOCIAN LOS SELLOS DEL PROVEEDOR
                            DataSet dsSellos = getSellosProveedor(prod.IdConvenio, proveedor.Id);

                            if (dsSellos != null && dsSellos.Tables[0].Rows.Count > 0)
                            {
                                List<Sello> sellos = new List<Sello>();
                                Sello sello;
                                for (int j = 0; j < dsSellos.Tables[0].Rows.Count; j++)
                                {
                                    sello = new Sello();
                                    sello.IdSello = int.Parse(dsSellos.Tables[0].Rows[j]["ID_SELLO"].ToString());
                                    sello.NombreSello = dsSellos.Tables[0].Rows[j]["NOMBRE_SELLO"].ToString();
                                    sello.PathSello = dsSellos.Tables[0].Rows[j]["PATH_SELLO"].ToString();
                                    sello.DescripcionSello = dsSellos.Tables[0].Rows[j]["DESC_SELLO"].ToString();
                                    sellos.Add(sello);
                                }

                                proveedor.Sellos = sellos;

                            }

                            prod.AddProveedor(proveedor);
                            productos.Add(prod);
                        }
                    }

                }
                else
                {
                    prod = new Entities.Producto();
                    prod.Id = 1;
                    prod.Nombre = "nombre producto test";
                    prod.UrlImagen = "";
                    prod.Descripcion = "Descripcion del producto Descripcion del producto Descripcion del producto Descripcion del producto";
                    prod.Marca = "Marca del producto";
                    prod.Convenio = "Convenio del producto";

                    Entities.Proveedor proveedor = new Entities.Proveedor();
                    proveedor.Id = 132;
                    proveedor.Nombre = "nombre proveedor test";
                    proveedor.Regiones = "I,II,III,IV";
                    proveedor.PrecioAnterior = "200";
                    proveedor.PrecioActual = "100";
                    proveedor.Ranking = 2;
                    proveedor.Sello = 1;

                    if (proveedor.PrecioActual == "" || proveedor.PrecioActual == "0")
                    {
                        proveedor.PrecioActual = proveedor.PrecioAnterior;
                        proveedor.PrecioAnterior = "";
                    }

                    prod.AddProveedor(proveedor);

                }

                return productos;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
