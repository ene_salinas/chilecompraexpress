﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using ChileCompraExpress.Utilities;
using System.Data.OleDb;
using System.Data;

namespace ChileCompraExpress.DataAccess
{
    public class InformacionDao
    {

        public IEnumerable<Entities.Informacion> GetInformaciones()
        {
            try
            {
                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true"))?true:false;
                Entities.Informacion info = null;
                Entities.Informacion[] infos = null;


                if (!datosdummies)
                {
                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                    string[] paramName = null;
                    string[] paramValue = null;
                    OleDbType[] type = null;
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getInformaciones", paramName, paramValue, null, type, null);
                    
                    DataSet ds = mssql.getData(proc);
                    mssql.closeConn();

                    if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                    {
                        infos = new Entities.Informacion[ds.Tables[0].Rows.Count];
                        

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            info = new Entities.Informacion();
                            info.Id = int.Parse(ds.Tables[0].Rows[i]["id"].ToString());
                            info.Texto = ds.Tables[0].Rows[i]["descrip"].ToString();
                            info.Titulo = ds.Tables[0].Rows[i]["titulo"].ToString();
                            info.FechaCreacion = ds.Tables[0].Rows[i]["fecha_creacion"].ToString();

                            //SE AGREGA CADA INFORMACION AL ARREGLO
                            infos[i] = info;
                        }
                    }

                    return infos;

                }
                else
                {

                    return new[] {
                        new Entities.Informacion {
                            Id=1,
                            Titulo = "Nuevas ofertas especiales",
                            Texto = "A partir de hoy nuevas ofertas especiales. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                        },
                        new Entities.Informacion {
                            Id=2,
                            Titulo = "Cambios en la plataforma de pago",
                            Texto = "Por recomendaciones de seguridad hemos realizado cambios en la plataforma de pago. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                        },
                        new Entities.Informacion {
                            Id=3,
                            Titulo = "Disponible version 4.5 de la aplicación ChilecompraExpress en Windows Store",
                            Texto = "Ya se encuentra disponible la versión 4.5 de la aplicación ChilecompraExpress en la tienda de Windows. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                        },
                        new Entities.Informacion {
                            Id=4,
                            Titulo = "Descuento de 15% para envíos en Atacama",
                            Texto = "Desde el 10 de junio se encuentra vigente un descuento especial de 15% para envíos en Atacama. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                        },
                        new Entities.Informacion {
                            Id=5,
                            Titulo = "Celebramos nuestro 10mo aniversario",
                            Texto = "El proximo 25 de mayo celebraremos el 10mo aniversario del portal. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                        },
                        new Entities.Informacion {
                            Id=6,
                            Titulo = "Terminadas las reparaciones en el servicio",
                            Texto = "Se han completado las reparaciones en el servicio de la Tienda, todas las opciones se encuentra disponibles una vez más. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                        },
                    };
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Entities.Informacion GetInformacionesById(int id)
        {
            try
            {
                return new Entities.Informacion
                {
                    Id = 1,
                    Titulo = "Nuevas ofertas especiales",
                    Texto = "Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                };
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
