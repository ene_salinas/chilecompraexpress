﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using ChileCompraExpress.Utilities;
using System.Data.OleDb;
using System.Data;

namespace ChileCompraExpress.DataAccess
{
    public class RegionesDao
    {
        public string insertRegion(string sesion, string userCode, string idregiones)
        {
            string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
            bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;
            bool retorno = false;
            string mensaje = "Se ha insertado exitosamente.";

            if (!datosdummies)
            {
                if (!string.IsNullOrEmpty(idregiones))
                {
                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                    string[] paramName = { "idSesion", "idUsuario", "idsRegiones" };
                    string[] paramValue = { sesion, userCode, idregiones };
                    OleDbType[] type = { OleDbType.VarChar, OleDbType.VarChar, OleDbType.VarChar };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "ING01_Stp_CHC_ListaRegiones_INS", paramName, paramValue, null, type, null);
                    DataSet ds = mssql.getData(proc);
                }
            }

            return mensaje;
        }
    }
}
