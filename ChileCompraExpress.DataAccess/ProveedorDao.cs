﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Entities;
using ChileCompraExpress.Utilities;
using System.Data.OleDb;
using System.Configuration;
using System.Data;

namespace ChileCompraExpress.DataAccess
{
    public class ProveedorDao
    {

        public DataSet getSellosProveedor(int idConvenio, int idProveedor)
        {
            try
            {
                IBaseDeDatos mssql = new MSSQLServer();
                OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                string[] paramName = { "id_proveed", "id_convenio" };
                string[] paramValue = { idProveedor.ToString(), idConvenio.ToString() };
                OleDbType[] type = { OleDbType.Integer, OleDbType.Integer };
                IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_GSA_prod_proveed_sello", paramName, paramValue, null, type, null);

                DataSet ds = mssql.getData(proc);
                mssql.closeConn();

                return ds;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public Proveedor[] getProveedores(int idProducto, int convenio,string session)
        {
            try
            {
                IBaseDeDatos mssql = new MSSQLServer();
                OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);
                Entities.Proveedor[] proveedores = null;


                string fecha_actual = Formatos.fechaActualAMD();
                //"IdSesion" - session 
                string[] paramName = { "idProducto", "idConvenioMarco", "IdSesion" };
                string[] paramValue = { idProducto.ToString(), convenio.ToString(), session };

                OleDbType[] type = { OleDbType.Integer, OleDbType.Integer, OleDbType.VarChar };
                IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getProductoConvenioProveedor", paramName, paramValue, null, type, null);

                DataSet ds = mssql.getData(proc);
                mssql.closeConn();

                if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                {
                    Proveedor proveedor = null;
                    proveedores = new Proveedor[ds.Tables[0].Rows.Count];
                    
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        
                            proveedor = new Proveedor();
                            proveedor.IdConvenio = int.Parse(ds.Tables[0].Rows[i]["idConvenioMarco"].ToString());
                            proveedor.IdProducto = int.Parse(ds.Tables[0].Rows[i]["idProducto"].ToString());
                            proveedor.Id = int.Parse(ds.Tables[0].Rows[i]["idProveedor"].ToString());
                            proveedor.Nombre = ds.Tables[0].Rows[i]["NombreFantasia"].ToString();
                            proveedor.Regiones = ds.Tables[0].Rows[i]["RegionesDespacho"].ToString();
                            proveedor.PrecioAnterior = ds.Tables[0].Rows[i]["PRECIO_ANTERIOR"].ToString();
                            proveedor.PrecioActual = ds.Tables[0].Rows[i]["PRECIO_ACTUAL"].ToString();
                            proveedor.Ranking = int.Parse(ds.Tables[0].Rows[i]["ESTRELLAS"].ToString());
                            proveedor.Moneda = ds.Tables[0].Rows[i]["MONEDA"].ToString();
                            DataSet dsSellos = getSellosProveedor(proveedor.IdConvenio, proveedor.Id);

                            if (dsSellos != null && dsSellos.Tables[0].Rows.Count > 0)
                            {
                                List<Sello> sellos  = new List<Sello>();
                                Sello sello;
                                for (int j = 0; j < dsSellos.Tables[0].Rows.Count; j++)
                                {
                                    sello = new Sello();
                                    sello.IdSello = int.Parse(dsSellos.Tables[0].Rows[j]["ID_SELLO"].ToString());
                                    sello.NombreSello = dsSellos.Tables[0].Rows[j]["NOMBRE_SELLO"].ToString();
                                    sello.PathSello = dsSellos.Tables[0].Rows[j]["PATH_SELLO"].ToString();
                                    sello.DescripcionSello = dsSellos.Tables[0].Rows[j]["DESC_SELLO"].ToString();
                                    sellos.Add(sello);
                                }

                                proveedor.Sellos = sellos;
                                
                            }

                            if (proveedor.PrecioActual == "" || proveedor.PrecioActual == "0")
                            {
                                proveedor.PrecioActual = proveedor.PrecioAnterior;
                                proveedor.PrecioAnterior = "";
                            }
                           

                            proveedores[i] = proveedor;
                        
                    }

                }

                return proveedores;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Proveedor[] getProveedoresByText(string text)
        {
            try
            {
                IBaseDeDatos mssql = new MSSQLServer();
                OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);
                Entities.Proveedor[] proveedores = null;


                string fecha_actual = Formatos.fechaActualAMD();

                string[] paramName = {"text"};
                string[] paramValue = {text};

                OleDbType[] type = { OleDbType.VarChar };
                IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getProveedores", paramName, paramValue, null, type, null);

                DataSet ds = mssql.getData(proc);
                mssql.closeConn();

                if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                {
                    Proveedor proveedor = null;
                    proveedores = new Proveedor[ds.Tables[0].Rows.Count];

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        proveedor = new Proveedor();
                        proveedor.Id = int.Parse(ds.Tables[0].Rows[i]["idProveedor"].ToString());
                        proveedor.Nombre = ds.Tables[0].Rows[i]["NombreFantasia"].ToString();                        

                        proveedores[i] = proveedor;
                    }

                }

                return proveedores;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
