﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Utilities;
using ChileCompraExpress.Entities;
using System.Data.OleDb;
using System.Configuration;
using System.Data;
using System.Web;
using System.IO;


namespace ChileCompraExpress.DataAccess
{
    public class DenunciaPrecioCaroDao
    {


        public bool SaveDenuncia(Denuncia denuncia,out string mensaje,out int idPrecioCaro)
        {
            string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
            bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;
            bool retorno = false;
            mensaje = "La denuncia no se ha logrado realizar, vuelva a intentarlo.";
            idPrecioCaro = 0;

            if (!datosdummies)
            {
                IBaseDeDatos mssql = new MSSQLServer();
                OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["DCCPProcurement"]);

                denuncia.Observacion = String.IsNullOrEmpty(denuncia.Observacion) ? "" : denuncia.Observacion;

                string[] paramName = { "idProducto", 
                                       "idConvenio" ,
                                       "nombreProducto",
                                       "codeProveedor", 
                                       "nombreProveedor",
                                       "codeComprador",
                                       "nombreComprador",
                                       "usrCode", 
                                       "usrName",                                       
                                       "observacion",                                      
                                       
                                     };

                string[] paramValue = { denuncia.IdProducto.ToString(), 
                                        denuncia.IdConvenio.ToString(),
                                        denuncia.NombreProducto,
                                        denuncia.IdProveedor.ToString(), 
                                        denuncia.NombreProveedor,
                                        denuncia.CodeEnt.ToString() ,
                                        denuncia.NameEnt,
                                        denuncia.IdUsuario.ToString(),
                                        denuncia.UserName,
                                        denuncia.Observacion 
                                      };

                OleDbType[] type = { OleDbType.Integer, 
                                     OleDbType.Integer, 
                                     OleDbType.VarChar, 
                                     OleDbType.VarChar, 
                                     OleDbType.VarChar,
                                     OleDbType.VarChar,
                                     OleDbType.VarChar,
                                     OleDbType.VarChar,
                                     OleDbType.VarChar,
                                     OleDbType.VarChar,
                                   };

                IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_spInsDenuncia", paramName, paramValue, null, type, null);
                DataSet ds = mssql.getData(proc);

                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    mensaje = ds.Tables[0].Rows[0]["RESULTADO"].ToString();
                    idPrecioCaro = int.Parse(ds.Tables[0].Rows[0]["ID_PRECIO"].ToString());
                    retorno = (idPrecioCaro == 0) ? false : true;
                    
                }
            }

            return retorno;
        }

        public List<string> obtenerCorreosProveedor(int idEmpresa)
        {
            string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
            bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;
            List<string> retorno = new List<string>();

            if (!datosdummies)
            {
                IBaseDeDatos mssql = new MSSQLServer();
                OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["DCCPPlatform"]);

                string[] paramName = { "idEmpresa" };
                string[] paramValue = { idEmpresa.ToString() };
                OleDbType[] type = { OleDbType.Integer };
                IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getCorreoProveedor", paramName, paramValue, null, type, null);
                DataSet ds = mssql.getData(proc);

                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    string correo = "";
                    string correoAlternativo = "";                    

                    for(int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        correo = ds.Tables[0].Rows[0]["ectEmail"].ToString();
                        correoAlternativo = ds.Tables[0].Rows[0]["ectEmailAlternative"].ToString();
                        
                        retorno.Add(correo);                       
                        retorno.Add(correoAlternativo);
                    }                    
                }
            }

            return retorno;
        }

        public void GenerarTicket(int idPrecioCaro)
        {
            IBaseDeDatos mssql = new MSSQLServer();
            OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["DCCPNotify"]);

            string[] paramName = { "TypeId",                                     
                                   "SourceId",
                                   "SourceCode",
                                   "IsProcesed",
                                   "Param1",
                                   "Param2",
                                   "Param3" ,
                                   "resultID"
                                 };

            string[] paramValue = { ConfigurationSettings.AppSettings["tipo_ticket"].ToString(),
                                    idPrecioCaro.ToString(),"", "0", "" , "", "", ""};

            OleDbType[] type = { OleDbType.Integer, 
                                 OleDbType.Integer,
                                 OleDbType.VarChar,
                                 OleDbType.VarChar,
                                 OleDbType.VarChar,
                                 OleDbType.VarChar,
                                 OleDbType.VarChar,
                                 OleDbType.VarChar
                                };

            
            IProcedure proc = new ProcedureMSQLServer(mssqlConn, "SpcCreateTicket", paramName, paramValue, null, type, null);
            DataSet ds = mssql.getData(proc);
        }
    }
}