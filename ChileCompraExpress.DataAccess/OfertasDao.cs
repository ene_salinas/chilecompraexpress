﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Utilities;
using System.Data.OleDb;
using System.Data;
using System.Configuration;

namespace ChileCompraExpress.DataAccess
{
    public class OfertasDao
    {
        public IEnumerable<Entities.Ofertas> getOfertasCarrusel(string idSession)
        {

            try
            {

                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true"))?true:false;
                Entities.Ofertas[] ofertas = null;

                if (!datosdummies)
                {

                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);
                   

                    string[] paramName = { "IdSesion" };
                    string[] paramValue = { idSession };
                    OleDbType[] type = { OleDbType.VarChar };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getOfertasEspec", paramName, paramValue, null, type, null);

                    DataSet ds = mssql.getData(proc);
                    mssql.closeConn();

                    if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                    {
                        Entities.Ofertas oferta = null;
                        double descuento = 0;
                        ofertas = new Entities.Ofertas[ds.Tables[0].Rows.Count];

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            double sinComas = double.Parse( ds.Tables[0].Rows[i]["DESCUENTO"].ToString());
                            oferta = new Entities.Ofertas();
                            oferta.Id = int.Parse(ds.Tables[0].Rows[i]["ID_PRODUCTO"].ToString());
                            oferta.TipoMoneda = ds.Tables[0].Rows[i]["MONEDA"].ToString();
                            oferta.Nombre = ds.Tables[0].Rows[i]["NOMBRE"].ToString().Substring(0, 15);
                            oferta.IdProveedor = int.Parse(ds.Tables[0].Rows[i]["ID_PROVEEDOR"].ToString());
                            oferta.Proveedor = ds.Tables[0].Rows[i]["NOMBRE_PROVEEDOR"].ToString();
                            oferta.PrecioAnterior = oferta.TipoMoneda + " " + Formatos.formatoDecimal(ds.Tables[0].Rows[i]["PRECIO"].ToString());
                            oferta.Precio = oferta.TipoMoneda + " " + Formatos.formatoDecimal(ds.Tables[0].Rows[i]["PRECIO_OFERTA"].ToString());
                            oferta.IdConvenio = int.Parse(ds.Tables[0].Rows[i]["ID_CONVENIO_MARCO"].ToString());
                            oferta.Descuento = int.Parse(String.Format("{0:0}", sinComas));
                            oferta.UrlImagen = ConfigurationSettings.AppSettings["UrlImagen"].ToString()
                                                    + ds.Tables[0].Rows[i]["IMAGEN"].ToString().Replace("../", "");
                            
                            ofertas[i] = oferta;

                        }
                    }

                }
                else
                {
                    Entities.Ofertas oferta = null;
                    double descuento = 0;
                    ofertas = new Entities.Ofertas[10];

                    for (int i = 0; i < 10; i++)
                    {
                        oferta = new Entities.Ofertas();
                        oferta.Id = i;
                        oferta.Nombre = "test nombre " + i;
                        oferta.Proveedor = "proveedor " + i;
                        oferta.PrecioAnterior = "10";
                        oferta.Precio = "1200";
                        descuento = 10;
                        oferta.UrlImagen = "";
                        ofertas[i] = oferta;

                    }
                }

                return ofertas;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
