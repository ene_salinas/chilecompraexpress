﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Utilities;
using ChileCompraExpress.Entities;
using System.Data.OleDb;
using System.Configuration;
using System.Data;

namespace ChileCompraExpress.DataAccess
{
    public class ProductoDao
    {


        public IEnumerable<Producto> getProductosRecienVisitados(string codeUser)
        {
            try
            {

                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true"))?true:false;
                Entities.Producto[] productos = null;

                if (!datosdummies)
                {

                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);
                    

                    string fecha_actual = Formatos.fechaActualAMD();

                    string[] paramName = { "idUsuario", "fecha", "restarDias" };
                    string[] paramValue = { codeUser, fecha_actual, ConfigurationSettings.AppSettings["resta_dias"].ToString() };

                    OleDbType[] type = { OleDbType.VarChar, OleDbType.VarChar, OleDbType.SmallInt };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getPrdRecienVistos", paramName, paramValue, null, type, null);

                    DataSet ds = mssql.getData(proc);
                    mssql.closeConn();

                    if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                    {
                        Producto prod = null;
                        productos = new Entities.Producto[ds.Tables[0].Rows.Count];
                        int descuento = 0;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            prod = new Producto();
                            prod.Id = int.Parse(ds.Tables[0].Rows[i]["idProducto"].ToString());
                            prod.IdConvenio = int.Parse(ds.Tables[0].Rows[i]["idConvenioMarco"].ToString());
                            prod.Nombre = ds.Tables[0].Rows[i]["NombreProducto"].ToString().Substring(0, 15);
                            prod.IdProveedor = int.Parse(ds.Tables[0].Rows[i]["idProveedor"].ToString());
                            prod.Proveedor = ds.Tables[0].Rows[i]["NombreFantasia"].ToString();
                            prod.Moneda = ds.Tables[0].Rows[i]["Moneda"].ToString();
                            prod.PrecioAnterior = ds.Tables[0].Rows[i]["PRECIO_ANTERIOR"].ToString();
                            prod.Precio = ds.Tables[0].Rows[i]["PRECIO_ACTUAL"].ToString();
                            prod.Hits = (ds.Tables[0].Rows[i]["Hits"].ToString().Equals(""))?0:int.Parse(ds.Tables[0].Rows[i]["Hits"].ToString());
                            /*descuento = prod.Descuento;*/
                            prod.UrlImagen = ConfigurationSettings.AppSettings["UrlImagen"].ToString()
                                                    + ds.Tables[0].Rows[i]["ImagenPequena"].ToString().Replace("../", "");
                            if (double.Parse(prod.Precio) == 0)
                            {
                                prod.Precio = Formatos.formatoDecimal(prod.PrecioAnterior);
                                prod.Precio = ds.Tables[0].Rows[i]["Moneda"].ToString() + prod.Precio;
                                prod.PrecioAnterior = "0";
                            }
                            else
                            {
                                prod.Precio = ds.Tables[0].Rows[i]["Moneda"].ToString() + Formatos.formatoDecimal(prod.Precio);
                                prod.PrecioAnterior = ds.Tables[0].Rows[i]["Moneda"].ToString() + Formatos.formatoDecimal(prod.PrecioAnterior);
                            }

                            productos[i] = prod;
                        }

                    }
                }
                else
                {
                    Producto prod = null;
                    productos = new Entities.Producto[10];
                    int descuento = 0;
                    for (int i = 0; i < 10; i++)
                    {
                        prod = new Producto();
                        prod.Id = i;
                        prod.Nombre = "RecienVisitados " + i;
                        prod.Proveedor = "Proveedor RecienVisitados";
                        prod.PrecioAnterior = "100";
                        prod.Precio = "123";
                        prod.Hits = 1;
                        descuento = 100;
                        prod.UrlImagen = "";
                        prod.IdProveedor = i + 10;
                        prod.IdConvenio = i + 20;
                        productos[i] = prod;
                    }
                }

                return productos;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IEnumerable<Producto> getProductosMasVisitados(string codeUser)
        {
            try
            {


                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true"))?true:false;
                Entities.Producto[] productos = null;

                if (!datosdummies)
                {


                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);
                    

                    string fecha_actual = Formatos.fechaActualAMD();

                    string[] paramName = { "idUsuario" };
                    string[] paramValue = { codeUser };

                    OleDbType[] type = { OleDbType.VarChar, OleDbType.VarChar, OleDbType.SmallInt };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getPrdMasVistos", paramName, paramValue, null, type, null);

                    DataSet ds = mssql.getData(proc);
                    mssql.closeConn();

                    if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                    {
                        Producto prod = null;
                        productos = new Entities.Producto[ds.Tables[0].Rows.Count];
                        int descuento = 0;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            prod = new Producto();
                            prod.Id = int.Parse(ds.Tables[0].Rows[i]["idProducto"].ToString());
                            prod.IdConvenio = int.Parse(ds.Tables[0].Rows[i]["idConvenioMarco"].ToString());
                            prod.IdProveedor = int.Parse(ds.Tables[0].Rows[i]["idProveedor"].ToString());
                            prod.Nombre = ds.Tables[0].Rows[i]["NombreProducto"].ToString().Substring(0, 15);
                            prod.Proveedor = ds.Tables[0].Rows[i]["NombreFantasia"].ToString();
                            prod.PrecioAnterior = ds.Tables[0].Rows[i]["PRECIO_ANTERIOR"].ToString();
                            prod.Precio = ds.Tables[0].Rows[i]["PRECIO_ACTUAL"].ToString();
                            prod.Hits = int.Parse(ds.Tables[0].Rows[i]["Hits"].ToString());
                            /*descuento = prod.Descuento;*/
                            prod.UrlImagen = ConfigurationSettings.AppSettings["UrlImagen"].ToString()
                                                    + ds.Tables[0].Rows[i]["ImagenPequena"].ToString().Replace("../", "");
                            if (double.Parse(prod.Precio) == 0)
                            {
                                prod.Precio = Formatos.formatoDecimal(prod.PrecioAnterior);
                                prod.Precio = ds.Tables[0].Rows[i]["Moneda"].ToString() + prod.Precio;
                                prod.PrecioAnterior = "0";
                            }
                            else
                            {
                                prod.Precio = ds.Tables[0].Rows[i]["Moneda"].ToString() + Formatos.formatoDecimal(prod.Precio);
                                prod.PrecioAnterior = ds.Tables[0].Rows[i]["Moneda"].ToString() + Formatos.formatoDecimal(prod.PrecioAnterior);
                            }

                            productos[i] = prod;
                            

                        }

                    }

                }
                else
                {
                    Producto prod = null;
                    productos = new Entities.Producto[10];
                    int descuento = 0;
                    for (int i = 0; i < 10; i++)
                    {
                        prod = new Producto();
                        prod.Id = i + 100;
                        prod.IdProveedor = i + 10;
                        prod.IdConvenio = i + 20;
                        prod.Nombre = "MasVisitados test";
                        prod.Proveedor = "nombre proveedor MasVisitados";
                        prod.PrecioAnterior = "100";
                        prod.Precio = "123";
                        prod.Hits = 1;
                        descuento = 100;
                        prod.UrlImagen = "";
                        productos[i] = prod;
                    }
                }

                return productos;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Entities.Producto InsertarHit(Producto prd)
        {
            try
            {

                Entities.Producto producto = null;
                IBaseDeDatos mssql = new MSSQLServer();
                OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                string[] paramName = { "idUsuario", "idProducto", "idConvenioMarco" };
                string[] paramValue = { prd.IdUser , prd.Id.ToString() , prd.IdConvenio.ToString() };

                OleDbType[] type = { OleDbType.VarChar, OleDbType.Integer, OleDbType.Integer };
                IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_insertVistos", paramName, paramValue, null, type, null);

                DataSet ds = mssql.getData(proc);
                
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    producto = new Producto();
                    producto.IdFormato = int.Parse(ds.Tables[0].Rows[0]["idformato"].ToString());
                    producto.Id = int.Parse(ds.Tables[0].Rows[0]["idProducto"].ToString());
                    producto.IdConvenio = int.Parse(ds.Tables[0].Rows[0]["idConvenioMarco"].ToString());                    
                }

                return producto;
            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }

        public Producto GetBaseProducto(Producto prd)
        {
            try
            {
                Entities.Producto producto = null;
                IBaseDeDatos mssql = new MSSQLServer();
                OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);
                prd.IdProveedor = (String.IsNullOrEmpty(prd.IdProveedor.ToString()) || prd.IdProveedor == 0) ? -1 : prd.IdProveedor;


                string[] paramName = { "idProducto", "idConvenioMarco" , "idProveedor"};
                string[] paramValue = { prd.Id.ToString(), prd.IdConvenio.ToString(), prd.IdProveedor.ToString() };

                OleDbType[] type = { OleDbType.Integer, OleDbType.Integer, OleDbType.Integer };
                IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getBaseProducto", paramName, paramValue, null, type, null);
                DataSet ds = mssql.getData(proc);

                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    producto = new Producto();
                    producto.IdFormato = int.Parse(ds.Tables[0].Rows[0]["idformato"].ToString());
                    producto.Id = int.Parse(ds.Tables[0].Rows[0]["idProducto"].ToString());
                    producto.IdConvenio = int.Parse(ds.Tables[0].Rows[0]["idConvenioMarco"].ToString());
                    producto.IdProveedor = int.Parse(ds.Tables[0].Rows[0]["idProveedor"].ToString());
                    producto.Nombre = ds.Tables[0].Rows[0]["NOMBRE_PRODUCTO"].ToString();
                    producto.Proveedor = ds.Tables[0].Rows[0]["NOMBRE_PROVEEDOR"].ToString();
                }

                return producto;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IEnumerable<Producto> getProductosMasVisitadosResultPage(string user)
        {
            try
            {


                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;
                Entities.Producto[] productos = null;

                if (!datosdummies)
                {


                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);


                    string fecha_actual = Formatos.fechaActualAMD();

                    string[] paramName = { "idUsuario", "selectTop" };
                    string[] paramValue = { user ,  ConfigurationSettings.AppSettings["max_prod_visitados"].ToString() };

                    OleDbType[] type = { OleDbType.VarChar, OleDbType.Integer };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getPrdMasVistosResult", paramName, paramValue, null, type, null);

                    DataSet ds = mssql.getData(proc);
                    mssql.closeConn();

                    if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                    {
                        Producto prod = null;
                        productos = new Entities.Producto[ds.Tables[0].Rows.Count];
                        
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            prod = new Producto();
                            prod.Id = int.Parse(ds.Tables[0].Rows[i]["idProducto"].ToString());
                            
                            productos[i] = prod;


                        }

                    }

                }
                else
                {
                    Producto prod = null;
                    productos = new Entities.Producto[10];
                    int descuento = 0;
                    for (int i = 0; i < 10; i++)
                    {
                        prod = new Producto();
                        prod.Id = i + 100;
                        prod.IdProveedor = i + 10;
                        prod.IdConvenio = i + 20;
                        prod.Nombre = "MasVisitados test";
                        prod.Proveedor = "nombre proveedor MasVisitados";
                        prod.PrecioAnterior = "100";
                        prod.Precio = "123";
                        prod.Hits = 1;
                        descuento = 100;
                        prod.UrlImagen = "";
                        productos[i] = prod;
                    }
                }

                return productos;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public IEnumerable<Producto> getProductosLosMasVisitados(string codeUser)
        {
            try
            {


                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;
                Entities.Producto[] productos = null;

                if (!datosdummies)
                {


                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);


                    string fecha_actual = Formatos.fechaActualAMD();

                    string[] paramName = { "idUsuario" };
                    string[] paramValue = { codeUser };

                    OleDbType[] type = { OleDbType.VarChar, OleDbType.VarChar, OleDbType.SmallInt };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getPrdLosMasVistos", paramName, paramValue, null, type, null);

                    DataSet ds = mssql.getData(proc);
                    mssql.closeConn();

                    if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                    {
                        Producto prod = null;
                        productos = new Entities.Producto[ds.Tables[0].Rows.Count];
                        int descuento = 0;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            prod = new Producto();
                            prod.Id = int.Parse(ds.Tables[0].Rows[i]["idProducto"].ToString());
                            prod.IdConvenio = int.Parse(ds.Tables[0].Rows[i]["idConvenioMarco"].ToString());
                            prod.IdProveedor = int.Parse(ds.Tables[0].Rows[i]["idProveedor"].ToString());
                            prod.Nombre = ds.Tables[0].Rows[i]["NombreProducto"].ToString().Substring(0, 15);
                            prod.Proveedor = ds.Tables[0].Rows[i]["NombreFantasia"].ToString();
                            prod.PrecioAnterior = ds.Tables[0].Rows[i]["PRECIO_ANTERIOR"].ToString();
                            prod.Precio = ds.Tables[0].Rows[i]["PRECIO_ACTUAL"].ToString();
                            prod.Hits = int.Parse(ds.Tables[0].Rows[i]["Hits"].ToString());
                            /*descuento = prod.Descuento;*/
                            prod.UrlImagen = ConfigurationSettings.AppSettings["UrlImagen"].ToString()
                                                    + ds.Tables[0].Rows[i]["ImagenPequena"].ToString().Replace("../", "");
                            if (double.Parse(prod.Precio) == 0)
                            {
                                prod.Precio = Formatos.formatoDecimal(prod.PrecioAnterior);
                                prod.Precio = ds.Tables[0].Rows[i]["Moneda"].ToString() + Formatos.formatoDecimal(prod.Precio);
                                prod.PrecioAnterior = "0";
                            }
                            else
                            {
                                prod.Precio = ds.Tables[0].Rows[i]["Moneda"].ToString() + Formatos.formatoDecimal(prod.Precio);
                                prod.PrecioAnterior = ds.Tables[0].Rows[i]["Moneda"].ToString() + Formatos.formatoDecimal(prod.PrecioAnterior);
                            }

                            productos[i] = prod;


                        }

                    }

                }
                else
                {
                    Producto prod = null;
                    productos = new Entities.Producto[10];
                    int descuento = 0;
                    for (int i = 0; i < 10; i++)
                    {
                        prod = new Producto();
                        prod.Id = i + 100;
                        prod.IdProveedor = i + 10;
                        prod.IdConvenio = i + 20;
                        prod.Nombre = "LosMasVisitados test";
                        prod.Proveedor = "nombre proveedor LosMasVisitados";
                        prod.PrecioAnterior = "100";
                        prod.Precio = "123";
                        prod.Hits = 1;
                        descuento = 100;
                        prod.UrlImagen = "";
                        productos[i] = prod;
                    }
                }

                return productos;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
