﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Utilities;
using System.Data.OleDb;
using System.Configuration;
using System.Data;

namespace ChileCompraExpress.DataAccess
{
    public class LogInDao
    {
        /// <summary>
        /// Obtiene un Id Session por defecto , solo uso para pruebas
        /// </summary>
        /// <returns></returns>
        public string getIdSession()
        {
            try
            {

                 string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;
                string id = "";

                

                if (!datosdummies)
                {

                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                    string[] paramName = null;
                    string[] paramValue = null;
                    OleDbType[] type = null;
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_idSession_test", paramName, paramValue, null, type, null);

                    DataSet ds = mssql.getData(proc);
                    mssql.closeConn();

                    id = ds.Tables[0].Rows[0]["ID_SESSION"].ToString();

                }
                else
                {
                    id = "dummie";
                }

                return id;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
