﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChileCompraExpress.Entities;
using ChileCompraExpress.Utilities;
using System.Data.OleDb;
using System.Configuration;
using System.Data;

namespace ChileCompraExpress.DataAccess
{
    public class CategoriaDao
    {
        public List<Categoria> getCategorias(TipoCategoria categTipo)
        {
            try
            {

                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true"))?true:false;
                List<Categoria> categorias = new List<Categoria>();

                if (!datosdummies)
                {

                    
                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                    string[] paramName = { "IdTipo", "IdNivel" };
                    string[] paramValue = { categTipo.Id.ToString(), "1" };
                    OleDbType[] type = { OleDbType.Integer, OleDbType.Integer };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "ING01_Stp_CHC_ListaCategoriasPorTipoNivel", paramName, paramValue, null, type, null);

                    DataSet ds = mssql.getData(proc);
                    mssql.closeConn();

                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        Categoria cat = null;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            cat = new Categoria();
                            cat.Id = int.Parse(ds.Tables[0].Rows[i]["idCategoria"].ToString());
                            cat.Nombre =  ds.Tables[0].Rows[i]["Menu"].ToString();
                            cat.NombreTruncate = ds.Tables[0].Rows[i]["Menu"].ToString();
                            categorias.Add(cat);
                        }
                    }

                }
                else
                {
                    Categoria cat = null;
                    for (int i = 0; i < 10; i++)
                    {
                        cat = new Categoria();
                        cat.Id = i;
                        cat.Nombre = "Test Categoria Test Categoria Test Categoria Test Categoria" + i;
                        cat.NombreTruncate = StringTool.Truncate("Test Categoria Test Categoria Test Categoria Test Categoria" + i,27);                        
                        
                        categorias.Add(cat);
                    }
                }

                return categorias;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        public List<SubCategoria> getSubCategoriasN3(int id)
        {
            try
            {

                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;
                List<SubCategoria> subcategorias = new List<SubCategoria>();

                if (!datosdummies)
                {


                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                    string[] paramName = { "idCategoriaN2" };
                    string[] paramValue = { id.ToString() };
                    OleDbType[] type = { OleDbType.Integer, OleDbType.VarChar };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getMenuCatNivel3", paramName, paramValue, null, type, null);

                    DataSet ds = mssql.getData(proc);

                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {

                        SubCategoria sub = null;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sub = new SubCategoria();
                            sub.Id = int.Parse(ds.Tables[0].Rows[i]["idCategoriaN3"].ToString());
                            sub.Nombre = ds.Tables[0].Rows[i]["nombreCategoriaN3"].ToString();
                            subcategorias.Add(sub);
                        }
                    }

                }
                else
                {
                    SubCategoria sub = null;
                    for (int i = 0; i < 5; i++)
                    {
                        sub = new SubCategoria();
                        sub.Id = i;
                        sub.Nombre = "testN3 " + i.ToString();
                        subcategorias.Add(sub);
                    }
                }

                return subcategorias;
                
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Categoria getSubCategoriasN2(int id)
        {
            try
            {

                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;
                Categoria categoria = new Categoria();
                
                if (!datosdummies)
                {

                    List<SubCategoria> subcategorias = new List<SubCategoria>();
                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                    string[] paramName = { "idCategoriaN1" };
                    string[] paramValue = { id.ToString() };
                    OleDbType[] type = { OleDbType.Integer, OleDbType.VarChar };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_getCatNivel2", paramName, paramValue, null, type, null);

                    DataSet ds = mssql.getData(proc);
                                        
                    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {

                        SubCategoria sub = null;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            sub = new SubCategoria();
                            sub.Id = int.Parse(ds.Tables[0].Rows[i]["idCategoriaN2"].ToString());
                            sub.Nombre = ds.Tables[0].Rows[i]["nombreCategoriaN2"].ToString();                            
                            categoria.addSubCategoriaN2(sub);
                        }
                    }

                }
                else
                {                    
                    SubCategoria sub = null;
                    for (int i = 0; i < 10; i++)
                    {
                        sub = new SubCategoria();
                        sub.Id = i;
                        sub.Nombre = "Test " + i;
                        categoria.addSubCategoriaN2(sub);
                    }
                }

                return categoria;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
