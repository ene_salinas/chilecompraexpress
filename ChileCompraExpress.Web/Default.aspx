﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ChileCompraExpress.Web.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="Scripts/jQuery/jquery-1.9.0.min.js"></script>
    <link href="Content/Estilos/Css/Base.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <div id="contenido" class="centro">
        
    </div>
    <script type="text/javascript" language="javascript">
        $("#productos .menu_tienda_listas_desp").click(function (evento) {
            evento.preventDefault();
            $(".menu_tiendas_listas").slideToggle("medium"
               ,
                function () {
                    if ($('#productos .menu_tiendas_listas').is(':hidden')) {
                        $(".btn_lista_desp").attr('src', 'Content/Estilos/Imagenes/botones/btn_mas.png');
                    }
                    else {
                        $(".btn_lista_desp").attr('src', 'Content/Estilos/Imagenes/botones/btn_menos.png');
                    }
                    return false;
                }

               );
            });

        $("#servicios .menu_tienda_listas_desp").click(function (evento) {
            evento.preventDefault();
            $(".menu_tiendas_listas").slideToggle("medium"
            ,
            function () {
                if ($('#servicios .menu_tiendas_listas').is(':hidden')) {
                    $(".btn_lista_desp").attr('src', 'Content/Estilos/Imagenes/botones/btn_mas.png');
                }
                else {
                    $(".btn_lista_desp").attr('src', 'Content/Estilos/Imagenes/botones/btn_menos.png');
                }
                return false;
            }

            );
        });

        $("#salud .menu_tienda_listas_desp").click(function (evento) {
            evento.preventDefault();
            $(".menu_tiendas_listas").slideToggle("medium"
            ,
            function () {
                if ($('#salud .menu_tiendas_listas').is(':hidden')) {
                    $(".btn_lista_desp").attr('src', 'Content/Estilos/Imagenes/botones/btn_mas.png');
                }
                else {
                    $(".btn_lista_desp").attr('src', 'Content/Estilos/Imagenes/botones/btn_menos.png');
                }
                return false;
            }

            );
        });

        $("#productos_link").click(function (evento) {
            evento.preventDefault();
            $("#servicios").slideUp(300).delay(800).fadeOut(400);
            $("#salud").slideUp(300).delay(800).fadeOut(400);
            $("#productos").slideUp(300).delay(800).fadeIn(400);
        });

        $("#servicios_link").click(function (evento) {
            evento.preventDefault();
            $("#productos").slideUp(300).delay(800).fadeOut(400);
            $("#salud").slideUp(300).delay(800).fadeOut(400);
            $("#servicios").slideUp(300).delay(800).fadeIn(400);
        });

        $("#salud_link").click(function (evento) {
            evento.preventDefault();
            $("#productos").slideUp(300).delay(800).fadeOut(400);
            $("#servicios").slideUp(300).delay(800).fadeOut(400);
            $("#salud").slideUp(300).delay(800).fadeIn(400);
        });


    </script>
    </form>
</body>
</html>
