﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChileCompraExpress.Web.ViewModels
{
    public class Producto
    {
        public int Id { get;  set; }        
        public double PrecioAnterior { get;  set; }
        public double Precio { get;  set; }

        private string filtroGrl;

        public string FiltroGrl
        {
            get { return filtroGrl; }
            set { filtroGrl = value; }
        }       


        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        

        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }

        private int idRegion;

        public int IdRegion
        {
            get { return idRegion; }
            set { idRegion = value; }
        }

        private string region;

        public string Region
        {
            get { return region; }
            set { region = value; }
        }

        private int idRango;

        public int IdRango
        {
            get { return idRango; }
            set { idRango = value; }
        }

        private string rango;

        public string Rango
        {
            get { return rango; }
            set { rango = value; }
        }

        private int idProveedor;

        public int IdProveedor
        {
            get { return idProveedor; }
            set { idProveedor = value; }
        }

        private string proveedor;

        public string Proveedor
        {
            get { return proveedor; }
            set { proveedor = value; }
        }
        
        
        private int idConvenio;

        public int IdConvenio
        {
            get { return idConvenio; }
            set { idConvenio = value; }
        }

        private string convenio;

        public string Convenio
        {
            get { return convenio; }
            set { convenio = value; }
        }
        
        

        public int Descuento
        {
            get
            {
                if (PrecioAnterior == 0) return 0;

                int intref = (int)(((PrecioAnterior - Precio) / PrecioAnterior) * 100);
                return intref / 5 * 5;
            }
        }

        public string UrlImagen
        {
            get
            {
               return "Content/images/products/producto_test.jpg";
            }
        }

        public static Producto Muestra()
        {
            return new Producto
            {
                Id = 235623,
                Nombre = "Catridge Negro Laser",
                Proveedor = "Transportes Arauco",
                PrecioAnterior = 160000,
                Precio = 120000,
            };
        }

        public static IEnumerable<Producto> Flujo()
        {
            return new Producto[] { 
                new Producto{ 
                    Id=235623,
                    Nombre = "Catridge Negro Laser",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 130000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Yogurt Surlat",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 140000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Mortadela Lisa",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 110000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Jamón Ahumado Cerdo",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 100000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Ibuprofeno 800mg",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio =80000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Jabón Palmolive",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 90000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Neumáticos",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 110000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Lubricante de autos",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 100000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Colector de vapores",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 110000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Queso Mozarella",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 120000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Atún enlatado",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 110000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Galletas de sal",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 90000,
                },
                new Producto{ 
                    Id=235623,
                    Nombre = "Salsa de soja",
                    Proveedor = "Transportes Arauco",
                    PrecioAnterior = 160000,
                    Precio = 50000,
                }
            };
        }
    }
}