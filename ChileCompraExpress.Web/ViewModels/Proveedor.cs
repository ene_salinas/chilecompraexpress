﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChileCompraExpress.Web.ViewModels
{
    public class Proveedor
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int IdProducto { get; set; }
        public string Precio { get; set; }
        public bool Check { get; set; }
        public string CaracteristicaEspecial  { get; set; }
        public string AtributoTexto { get; set; }
        public string AtributoValor { get; set; }

        public Proveedor[] FlujoProveedores()
        {
            Proveedor[] proveedores = new Proveedor[10];
            Proveedor proveedor = null;
            for (int i = 0; i < 10; i++)
            {
                proveedor = new Proveedor();
                proveedor.Descripcion = "Descripcion " + i;
                proveedor.Id = i;
                proveedor.Precio = "100";

                //CARACTERISTICAS ESPECIALES
                proveedor.Check = true;
                proveedor.CaracteristicaEspecial = "caracteristica " + i ;
                proveedor.AtributoTexto = "TEXTO " + i;
                proveedor.AtributoValor = "VALOR" + i;

                proveedores[i] = proveedor;
            }

            return proveedores;
 
        }
    }
}