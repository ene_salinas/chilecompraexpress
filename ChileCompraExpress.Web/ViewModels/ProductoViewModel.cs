﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace ChileCompraExpress.Web.ViewModels
{
    public class ProductoViewModel
    {
        public List<Producto> getProductos(Producto prodcto)
        {
            string filtro = "";
            filtro += (!String.IsNullOrEmpty(prodcto.Marca)) ? prodcto.Marca + "+inmeta:RR%3D" : "";
            filtro += ((!String.IsNullOrEmpty(prodcto.Region))
                        &&(prodcto.Region != "Todas las regiones")) ? prodcto.Region + "+inmeta:RR%3D" : "";
            filtro += (!String.IsNullOrEmpty(prodcto.Proveedor)) ? prodcto.Proveedor + "+inmeta:RR%3D" : "";
            filtro += (!String.IsNullOrEmpty(prodcto.Convenio)) ? prodcto.Convenio + "+inmeta:RR%3D" : "";
            filtro += (!String.IsNullOrEmpty(prodcto.FiltroGrl)) ? prodcto.FiltroGrl + "+inmeta:RR%3D" : "";
            filtro += ((!String.IsNullOrEmpty(prodcto.Rango))
                        && (prodcto.Rango != "Todos los precios")) ? prodcto.Rango : "";

            if (!String.IsNullOrEmpty(filtro))
            {
                filtro = "q=" + filtro + "&";
            }
            else
            {
                filtro = "q=servidor&";
            }

            List<Producto> productos = new List<Producto>();
            XDocument XDoc = new XDocument();
            string url = "http://201.236.79.132/search?" + filtro + "btnG=Google+Search&client=publico&sort=date%3AD%3AL%3Ad1&oe=UTF-8&ie=UTF-8&ud=1&getfields=*&getfields=*&exclude_apps=1&site=publico&filter=0&proxyreload=1";
            XDoc = XDocument.Load(url);
            
            var itemsTopic = XDoc.Descendants("MT")
              .Where(node => (string)node.Attribute("N") == "C")
              .Select(node => node.Attribute("V"))
              .ToList();
            var itemsWebLink = XDoc.Descendants("MT")
               .Where(node => (string)node.Attribute("N") == "S")
               .Select(node => node.Attribute("V"))
               .ToList();
            var itemsS = XDoc.Descendants("R")
                //.Where(node => (string)node.Element("S") == "S")
               .Select(node => node.Element("S"))
               .ToList();
            var itemsDocPath = XDoc.Descendants("MT")
               .Where(node => (string)node.Attribute("N") == "DocPath")
               .Select(node => node.Attribute("V"))
               .ToList();

            Producto prod = null;
            for (int i = 0; i < itemsTopic.Count; i++)
            {
                prod = new Producto();
                prod.Marca = itemsTopic[i].Value;
                prod.Nombre = itemsWebLink[i].Value;
                prod.Proveedor = itemsS[i].Value;
            }
            

            return productos;
        }
    }
}