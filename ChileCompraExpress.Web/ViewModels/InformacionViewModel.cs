﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChileCompraExpress.Web.ViewModels
{
    public class InformacionViewModel
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public string UrlDescarga { get; set; }

        public string UrlImagen
        {
            get
            {
                return "Content/images/info_placeholder.png";
            }
        }

        public static InformacionViewModel Muestra()
        {
            return new InformacionViewModel
            {
                Id = 1,
                Titulo = "Nuevas ofertas especiales",
                Texto = "Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
            };
        }

        public static IEnumerable<InformacionViewModel> Flujo()
        {
            return new[] {
                new InformacionViewModel {
                    Id=1,
                    Titulo = "Nuevas ofertas especiales",
                    Texto = "A partir de hoy nuevas ofertas especiales. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                },
                new InformacionViewModel {
                    Id=2,
                    Titulo = "Cambios en la plataforma de pago",
                    Texto = "Por recomendaciones de seguridad hemos realizado cambios en la plataforma de pago. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                },
                new InformacionViewModel {
                    Id=3,
                    Titulo = "Disponible version 4.5 de la aplicación ChilecompraExpress en Windows Store",
                    Texto = "Ya se encuentra disponible la versión 4.5 de la aplicación ChilecompraExpress en la tienda de Windows. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                },
                new InformacionViewModel {
                    Id=4,
                    Titulo = "Descuento de 15% para envíos en Atacama",
                    Texto = "Desde el 10 de junio se encuentra vigente un descuento especial de 15% para envíos en Atacama. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                },
                new InformacionViewModel {
                    Id=5,
                    Titulo = "Celebramos nuestro 10mo aniversario",
                    Texto = "El proximo 25 de mayo celebraremos el 10mo aniversario del portal. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                },
                new InformacionViewModel {
                    Id=6,
                    Titulo = "Terminadas las reparaciones en el servicio",
                    Texto = "Se han completado las reparaciones en el servicio de la Tienda, todas las opciones se encuentra disponibles una vez más. Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.",
                },
            };
        }
    }
}