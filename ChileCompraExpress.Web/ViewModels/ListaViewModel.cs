﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChileCompraExpress.Web.ViewModels
{
    public class ListaViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public static ListaViewModel Muestra()
        {
            

            return new ListaViewModel
            {
                Id = 0,
                Nombre = "Lista de muestra",
                Descripcion = "Una lista de muestra."
            };
        }

        public static IEnumerable<ListaViewModel> Flujo()
        {
            

            return new[]
            {
                new ListaViewModel
                {
                    Id = 1,
                    Nombre="Víveres"
                },
                new ListaViewModel
                {
                    Id = 2,
                    Nombre="Materiales de oficina"
                },
                new ListaViewModel
                {
                    Id = 3,
                    Nombre="Materiales de taller"
                },

            };
        }
    }
}