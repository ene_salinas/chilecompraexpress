﻿using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using ChileCompraExpress.Common.Helpers;
using ChileCompraExpress.Web.Helpers;
using System.Web.SessionState;
using System.Web;
using System.Web.Http;
using System.Configuration;

namespace ChileCompraExpress.Web.Controllers
{

    public abstract class BaseController:Controller
    {
        private SessionHelper _sessionHelper;
             
        private SessionHelper SessionHelper
        {
            get
            { 
                if (_sessionHelper == null)
                    _sessionHelper = new SessionHelper();
                
                return _sessionHelper;
            }
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return Json(data, contentType, contentEncoding, JsonRequestBehavior.AllowGet);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            //TraceHelper.Error(filterContext.Exception);

            if (filterContext.ExceptionHandled)
                return;

            filterContext.Result = new ViewResult { ViewName = "~/Views/Shared/Error.cshtml" };
            filterContext.ExceptionHandled = true;
        }


        protected override void OnAuthorization(AuthorizationContext filterContext)
        {

            string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
            bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;

            if (!datosdummies)
            {

                bool? checkedRoles = (bool?)Session["checkedroles"];
                checkedRoles = false;
                if (!checkedRoles.HasValue || (checkedRoles.HasValue && !checkedRoles.Value))
                {
                    // checkear roles
                    if (!SessionHelper.CheckRoles())
                    {
                        filterContext.Result = new EmptyResult();
                        FormsAuthentication.SignOut();
                        FormsAuthentication.RedirectToLoginPage();
                        return;
                    }

                    Session["checkedroles"] = true;

                }
            }
        }

     
    }
}
