﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using ChileCompraExpress.Entities;
using ChileCompraExpress.Business;


namespace ChileCompraExpress.Web.Controllers
{
    public class RegionesDespachoController : ApiController
    {
        //
        // GET: /RegionesDespacho/

        public IEnumerable<Region> Get()
        {
            Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
            ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();
            return user.Regiones;
        }

        public string Get(string idregiones)
        {
            Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
            ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();
            if (idregiones == null)
            {
                idregiones = "";
            }

            RegionesBsn regionBsn = new RegionesBsn();
            string mensaje = regionBsn.insertRegion(HttpContext.Current.Session.SessionID, user.Code + "_" + user.Organization.Code, idregiones);

            return mensaje;
        }

    }
}
