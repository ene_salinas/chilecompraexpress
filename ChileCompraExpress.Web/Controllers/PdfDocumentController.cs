﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using System.Text;

namespace ChileCompraExpress.Web.Controllers
{
    public class PdfDocumentController : Controller
    {
        //
        // GET: /PdfDocument/

        public ActionResult Index(string title,string texto)
        {

            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            MemoryStream memStream = new MemoryStream();
          
            PdfWriter wri = PdfWriter.GetInstance(doc,memStream);
            //Open Document to write          
            doc.Open();
            Paragraph titleP = new Paragraph();
            titleP.Alignment = Element.ALIGN_CENTER;
            titleP.Add(title);
            doc.Add(titleP);

            Paragraph saltoLinea = new Paragraph(" ");
            doc.Add(saltoLinea);

            Paragraph paragraph = new Paragraph(texto);
            doc.Add(paragraph);

            doc.Close();

               

            Byte[] bytes = Encoding.Convert(Encoding.UTF8, Encoding.Default, memStream.ToArray());
            memStream.Flush();
            memStream.Close();

            //byte[] documentData;

            return File(memStream.ToArray(), "application/pdf");
        }

    }
}
