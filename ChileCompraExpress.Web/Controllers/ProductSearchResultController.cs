﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChileCompraExpress.Web.ViewModels;

namespace ChileCompraExpress.Web.Controllers
{
    public class ProductSearchResultController : ApiController
    {
        // GET api/ProductSearchResult
        public IEnumerable<Producto> Get()
        {
            return null;// Producto.SearchResult();
        }

        // GET api/ProductSearchResult/5
        public Producto Get(int id)
        {
            return Producto.Muestra();
        }

        // POST api/ProductSearchResult
        public void Post([FromBody]Producto value)
        {
        }

        // PUT api/ProductSearchResult/5
        public void Put(int id, [FromBody]Producto value)
        {
        }

        // DELETE api/ProductSearchResult/5
        public void Delete(int id)
        {
        }
    }
}
