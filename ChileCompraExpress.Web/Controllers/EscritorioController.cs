﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChileCompraExpress.Web.ViewModels;

namespace ChileCompraExpress.Web.Controllers
{
    public class EscritorioController :Controller//: BaseController
    {
        //
        // GET: /Escritorio/

        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch(Exception ex)
            {
                throw (ex);
            }

        }

        public PartialViewResult Loading()
        {
            try
            {
                return PartialView("~/Views/Escritorio/Loader.cshtml");
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public void SetSessionItems(string count)
        {
            Helpers.SessionHelper sesionCountItem = new Helpers.SessionHelper();
            sesionCountItem.SetCountItems(count);
        }
    }
}
