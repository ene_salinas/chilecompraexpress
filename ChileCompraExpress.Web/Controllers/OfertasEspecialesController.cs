﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChileCompraExpress.Web.ViewModels;
using ChileCompraExpress.Entities;
using ChileCompraExpress.Business;
using System.Web;
using System.Web.Mvc;
using ChileCompraExpress.HttpHandlers;



namespace ChileCompraExpress.Web.Controllers
{
    public class OfertasEspecialesController : ApiController
    {

        //private HttpContext _context = new HttpContext( ;

        // GET api/ofertasespeciales
        public IEnumerable<Ofertas> Get()
        {            
            OfertasBsn ofertas = new OfertasBsn();
            return ofertas.getOfertasCarrusel(HttpContext.Current.Session.SessionID);            
        }

    

        // GET api/ofertasespeciales/5
        public Entities.Producto Get(int id)
        {
            return null;//Producto.Muestra();
        }

        // POST api/ofertasespeciales
        public void Post([FromBody]Entities.Producto value)
        {
        }

        // PUT api/ofertasespeciales/5
        public void Put(int id, [FromBody]Entities.Producto value)
        {
        }

        // DELETE api/ofertasespeciales/5
        public void Delete(int id)
        {
        }
    }
}
