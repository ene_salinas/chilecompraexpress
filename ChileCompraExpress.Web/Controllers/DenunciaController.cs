﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChileCompraExpress.Entities;
using ChileCompraExpress.Business;
using System.Web.Script.Serialization;

namespace ChileCompraExpress.Web.Controllers
{
    public class DenunciaController : Controller
    {
        //
        // GET: /Denuncia/
        [HttpPost]
        public string Index(Denuncia denuncia)
        {
            DenunciaPrecioCaroBsn denunciaBsn = new DenunciaPrecioCaroBsn();
            Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
            ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();
                        
            denuncia.IdUsuario = int.Parse(user.Code);
            denuncia.UserName = user.FirstName;
            denuncia.CodeEnt = user.Organization.entCode;
            denuncia.NameEnt = user.Organization.entName;

            string resp = denunciaBsn.SaveDenuncia(denuncia);            

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(resp);            
        }

    }
}
