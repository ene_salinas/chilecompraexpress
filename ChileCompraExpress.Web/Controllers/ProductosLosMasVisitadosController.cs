﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChileCompraExpress.Business;

namespace ChileCompraExpress.Web.Controllers
{
    public class ProductosLosMasVisitadosController : System.Web.Http.ApiController
    {
        //
        // GET: /ProductosLosMasVisitados/

        // GET api/ProductosLosMasVisitados
        public IEnumerable<Entities.Producto> Get()
        {
            ProductoBsn prodBC = new ProductoBsn();
            Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
            ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();
            return prodBC.getProductosLosMasVisitados(user.Code);
        }

    }
}
