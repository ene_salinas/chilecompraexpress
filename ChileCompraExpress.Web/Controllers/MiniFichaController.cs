﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChileCompraExpress.Entities;
using ChileCompraExpress.Business;
using System.IO;
using System.Text;
using ChileCompraExpress.Utilities;

namespace ChileCompraExpress.Web.Controllers
{
    public class MiniFichaController : Controller
    {
        //
        // GET: /MiniFicha/

        public PartialViewResult Index(Producto minificha)
        {
            MiniFichaBsn miniFichaBsn = new MiniFichaBsn();
            Producto prd = miniFichaBsn.Detalles(minificha);
            
            return PartialView("~/Views/Escritorio/MiniFicha.cshtml", prd);
        }

        public PartialViewResult GetPopOverMisListas(Producto minificha)
        {
            
            return PartialView("~/Views/Shared/MisListasPopOver.cshtml",minificha);
        }

        public void ExportExcel(Producto minificha)
        {
            MiniFichaBsn miniFichaBsn = new MiniFichaBsn();
            List<Producto> prd = miniFichaBsn.ExportExcel(minificha, HttpContext.Session.SessionID);

            if (prd != null)
            {
                /* SE CREA LA TABLA */
                Table tbl = new Table();
                tbl.Attribute = "border='1px solid'";
                Row row = null;
                Column col = null;

                //CABECERA
                row = new Row();
                row.Attribute = "style='font-weight: bold'";

                col = new Column();
                col.Valor = "Id de producto";
                row.addColumn(col);

                col = new Column();
                col.Valor = "Proveedor";
                row.addColumn(col);

                col = new Column();
                col.Valor = "Producto";
                row.addColumn(col);

                col = new Column();
                col.Valor = "Precio";
                row.addColumn(col);

                col = new Column();
                col.Valor = "Convenio";
                row.addColumn(col);

                tbl.addRow(row);

                for (int i = 0; i < prd.Count; i++)
                {
                    //BODY EXCEL
                    row = new Row();

                    col = new Column();
                    col.Valor = prd.ElementAt(i).Id.ToString();
                    row.addColumn(col);

                    col = new Column();
                    col.Valor = (prd.ElementAt(i).proveedores != null && prd.ElementAt(i).proveedores.Count > 0) ? prd.ElementAt(i).proveedores.ElementAt(0).Nombre : "";
                    row.addColumn(col);

                    col = new Column();
                    col.Valor = prd.ElementAt(i).Nombre;
                    row.addColumn(col);

                    col = new Column();
                    col.Valor = (prd.ElementAt(i).proveedores != null && prd.ElementAt(i).proveedores.Count > 0) ? "" + prd.ElementAt(i).proveedores.ElementAt(0).PrecioActual : "0";
                    row.addColumn(col);

                    col = new Column();
                    col.Valor = prd.ElementAt(i).Convenio;
                    row.addColumn(col);

                    tbl.addRow(row);
                }

                ITable table = new AdvancedTable(tbl, null);

                MemoryStream ms = new MemoryStream();
                StreamWriter sw = new StreamWriter(ms);

                sw.Write(table.ToString());
                sw.Flush();
                sw.Close();

                Byte[] bytes = Encoding.Convert(Encoding.UTF8, Encoding.Default, ms.ToArray());
                ms.Flush();
                ms.Close();
                string now = DateTime.Now.ToString().Replace("/", "_");
                now = now.Replace(":", "_");
                now = now.Replace(" ", "_");
                this.ControllerContext.HttpContext.Response.Clear();

                this.ControllerContext.HttpContext.Response.Buffer = true;
                this.ControllerContext.HttpContext.Response.ContentType = "application/vnd.ms-excel";
                this.ControllerContext.HttpContext.Response.Charset = String.Empty;
                this.ControllerContext.HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=resultados.xls");
                this.ControllerContext.HttpContext.Response.AddHeader("Content-Length", bytes.Length.ToString());

                this.ControllerContext.HttpContext.Response.BinaryWrite(bytes);
                this.ControllerContext.HttpContext.Response.Flush();
                this.ControllerContext.HttpContext.Response.Close();
            }
            
        }

    }
}
