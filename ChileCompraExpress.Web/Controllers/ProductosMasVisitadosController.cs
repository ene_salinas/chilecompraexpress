﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChileCompraExpress.Business;
using ChileCompraExpress.Entities;

namespace ChileCompraExpress.Web.Controllers
{
    public class ProductosMasVisitadosController : ApiController
    {
        // GET api/ofertasespeciales
        public IEnumerable<Producto> Get()
        {
            ProductoBsn prodBC = new ProductoBsn();
            Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
            ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();

            return prodBC.getProductosMasVisitados(user.Code);
            //return Producto.Flujo();
        }

        // GET api/ofertasespeciales/5
        public Producto Get(int id)
        {
            return null;//Producto.Muestra();
        }

        // POST api/ofertasespeciales
        public void Post([FromBody]Producto value)
        {
        }

        // PUT api/ofertasespeciales/5
        public void Put(int id, [FromBody]Producto value)
        {
        }

        // DELETE api/ofertasespeciales/5
        public void Delete(int id)
        {
        }
    }
}
