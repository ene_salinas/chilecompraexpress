﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChileCompraExpress.Web.ViewModels;
using ChileCompraExpress.Business;
using ChileCompraExpress.Entities;

namespace ChileCompraExpress.Web.Controllers
{
    public class InformacionesController : ApiController
    {
        // GET api/informaciones
        public IEnumerable<Informacion> Get()
        {
            InformacionBsn infoBsn = new InformacionBsn();
            return infoBsn.GetInformaciones();
            //return InformacionViewModel.Flujo();
        }

        // GET api/informaciones/5
        public Informacion Get(int id)
        {
            InformacionBsn infoBsn = new InformacionBsn();
            return infoBsn.GetInformacionesById(id);
        }

        // POST api/informaciones
        public void Post([FromBody]InformacionViewModel informacion)
        {
        }

        // PUT api/informaciones/5
        public void Put(int id, [FromBody]InformacionViewModel informacion)
        {
        }

        // DELETE api/informaciones/5
        public void Delete(int id)
        {
        }
    }
}
