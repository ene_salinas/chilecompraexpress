﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChileCompraExpress.Web.ViewModels;
using ChileCompraExpress.Utilities;
using System.Data.OleDb;
using System.Configuration;
using System.Data;
using System.Web;

namespace ChileCompraExpress.Web.Controllers
{
    public class ListasController : ApiController
    {
        // GET api/listas
        //public IEnumerable<ListaViewModel> Get()
        //{

        //    Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
        //    ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();


        //    var session = HttpContext.Current.Session;

        //    IBaseDeDatos mssql = new MSSQLServer();
        //    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

        //    string[] paramName = { "CurrentPage", "PageSize", "TotalRecords", "idUsuario", "idSession" };
        //    string[] paramValue = { "1", "100", "0", user.Code + "_" + user.Organization.Code, session.SessionID };
        //    ParameterDirection[] paramDirection = { ParameterDirection.Input, ParameterDirection.Input, ParameterDirection.Output, ParameterDirection.Input, ParameterDirection.Input };
        //    OleDbType[] type = { OleDbType.Integer, OleDbType.Integer, OleDbType.VarChar, OleDbType.VarChar };
        //    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "ING0_Stp_CHC_CompraFrecuenteCargaCompras", paramName, paramValue, paramDirection, type, null);

        //    DataSet ds = mssql.getData(proc);
        //    mssql.closeConn();

        //    if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
        //    {
        //        var item = ds.Tables[0].Rows[0];

        //        return new ListaViewModel
        //        {
        //            //Id = id,
        //            Nombre = item["Nombre"].ToString(),
        //            Descripcion = item["Descripcion"].ToString(),
        //        };
        //    }
        //}

        // GET api/listas/5
        public IEnumerable<ListaViewModel> Get()
        {

            string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
            bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;

            ListaViewModel[] list = null;

            if (!datosdummies)
            {

                Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
                ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();


                var session = HttpContext.Current.Session;
                               

                IBaseDeDatos mssql = new MSSQLServer();
                OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                string[] paramName = { "idUsuario", "CurrentPage", "PageSize", "TotalRecords", "idSession" };
                string[] paramValue = { user.Code + "_" + user.Organization.Code, "1", "100", "0", session.SessionID };
                ParameterDirection[] paramDirection = { ParameterDirection.Input, ParameterDirection.Input, ParameterDirection.Input, ParameterDirection.Output, ParameterDirection.Input };
                int[] size = { 100, 0, 0, 0, 500 };
                OleDbType[] type = { OleDbType.VarChar, OleDbType.Integer, OleDbType.Integer, OleDbType.Integer, OleDbType.VarChar };
                IProcedure proc = new ProcedureMSQLServer(mssqlConn, "ING0_Stp_CHC_CompraFrecuenteCargaCompras", paramName, paramValue, paramDirection, type, size);
                
                DataSet ds = mssql.getData(proc);
                mssql.closeConn();

                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    //var item = ds.Tables[0].Rows[0];
                    list = new ListaViewModel[ds.Tables[0].Rows.Count];


                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        list[i] = new ListaViewModel
                        {
                            Id = int.Parse(ds.Tables[0].Rows[i]["IdCompraFrecuente"].ToString()),
                            Nombre = ds.Tables[0].Rows[i]["Nombre"].ToString(),
                            Descripcion = ds.Tables[0].Rows[i]["Descripcion"].ToString(),
                        };
                    }


                }
            }
            else
            {
                list = new ListaViewModel[10];

                for (int i = 0; i < 10; i++)
                {

                    list[i] = new ListaViewModel
                    {
                        Id = i,
                        Nombre = "Nombre Test " + i.ToString(),
                        Descripcion = "Descripcion Test ",
                    };
                }
            }

            return list;
            //return ListaViewModel.Muestra();
        }

        // POST api/listas
        public int Post([FromBody]ListaViewModel lista)
        {
            try
            {

                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;

                string id = "";

                if (!datosdummies)
                {
                    Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
                    ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();

                    var session = HttpContext.Current.Session;

                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                    string[] paramName = { "IdCompraFrecuente", "IdUsuario", "Nombre", "Descripcion" };
                    string[] paramValue = { "0", user.Code + "_" + user.Organization.Code, lista.Nombre.ToString(), lista.Descripcion.ToString() };
                    ParameterDirection[] paramDirection = { ParameterDirection.Output, ParameterDirection.Input, ParameterDirection.Input, ParameterDirection.Input };

                    OleDbType[] type = { OleDbType.Integer, OleDbType.VarChar, OleDbType.VarChar, OleDbType.VarChar };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "ING0_Stp_CHC_CreaListaCompra", paramName, paramValue, paramDirection, type, null);

                    DataSet ds = mssql.getData(proc);

                    OleDbParameter[] parameter = (OleDbParameter[])proc.getParameter();

                    if (parameter != null && parameter.Length > 0)
                    {
                        for (int i = 0; i < parameter.Length; i++)
                        {
                            if (parameter[i].ParameterName != null && parameter[i].ParameterName == "@IdCompraFrecuente")
                            {
                                id = parameter[i].Value.ToString();
                            }
                        }
                    }

                    mssql.closeConn();

                    // TODO: HAY QUE RETORNAR EL ID DE LA NUEVA LISTA CREADA. ES OBLIGATORIO
                }
                else
                {
                    id = "1";
                }


                return int.Parse(id);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        // DELETE api/listas/5
        public int Delete(int id)
        {
            try
            {

                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;

                if (!datosdummies)
                {

                    var session = HttpContext.Current.Session;


                    IBaseDeDatos mssql = new MSSQLServer();
                    OleDbConnection mssqlConn = (OleDbConnection)mssql.connect(ConfigurationSettings.AppSettings["Tienda"]);

                    string[] paramName = { "idCompraFrecuente" };
                    string[] paramValue = { id.ToString() };
                    OleDbType[] type = { OleDbType.Integer };
                    IProcedure proc = new ProcedureMSQLServer(mssqlConn, "AMIN_delLista", paramName, paramValue, null, type, null);

                    DataSet ds = mssql.getData(proc);
                    mssql.closeConn();

                }

                return 1;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
