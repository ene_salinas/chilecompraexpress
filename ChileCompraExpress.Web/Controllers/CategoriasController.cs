﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChileCompraExpress.Entities;
using ChileCompraExpress.Business;
using System.Web.Script.Serialization;

namespace ChileCompraExpress.Web.Controllers
{
    public class CategoriasController : Controller
    {
        //
        // GET: /Categorias/      
        
        [HttpPost]
        public string Index(TipoCategoria categoria)
        {
            CategoriaBsn catBsn = new CategoriaBsn();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string result =  serializer.Serialize(catBsn.getCategorias(categoria));
            
            

            return result;
        }

        [HttpPost]
        public string CategoriaSubNivel(string id)
        {
            CategoriaBsn catBsn = new CategoriaBsn();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string result = serializer.Serialize(catBsn.getSubCategoriasN2(int.Parse(id)));

            return result;
        }

        [HttpPost]
        public string CategoriaSubNivelN3(string id)
        {
            CategoriaBsn catBsn = new CategoriaBsn();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string result = serializer.Serialize(catBsn.getSubCategoriasN3(int.Parse(id)));

            return result;
        }


        [HttpPost]
        public PartialViewResult GetCategorias(Producto prod)
        {
            ProveedorBsn proveedorBsn = new ProveedorBsn();
            //Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
            //ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();
            string session = Session.SessionID;

            Entities.Proveedor[] proveedor = proveedorBsn.proveedores(prod.Id, prod.IdConvenio, session);
            return PartialView("~/Views/Productos/Buscar/ResponseMasProveedores.cshtml", proveedor);
        }

    }
}
