﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using ChileCompraExpress.Web.ViewModels;

namespace ChileCompraExpress.Web.Controllers
{
    public class ProveedorController : ApiController
    {
        //
        // GET: /Proveedor/

        public IEnumerable<Proveedor> Get()
        {
            Proveedor prov = new Proveedor();
            return prov.FlujoProveedores();
            
        }

    }
}
