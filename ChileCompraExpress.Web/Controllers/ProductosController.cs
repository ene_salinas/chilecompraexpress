﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using ChileCompraExpress.Web.ViewModels;
using ChileCompraExpress.XmlParserLibrary.Models;
using ChileCompraExpress.XmlParserLibrary;
using System.Text.RegularExpressions;
using ChileCompraExpress.Web.Models;
using System.Diagnostics;
using ChileCompraExpress.Business;
using ChileCompraExpress.Utilities;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Configuration;


namespace ChileCompraExpress.Web.Controllers
{
    public class ProductosController : Controller
    {


  

        [HttpPost]
        public string GetBaseProducto(Entities.Producto prd)
        {
            ProductoBsn prdBsn = new ProductoBsn();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Entities.Producto producto = prdBsn.GetBaseProducto(prd);
            return serializer.Serialize(producto);
        }

        [HttpPost]
        public string VerFicha(Entities.Producto prd)
        {
            ProductoBsn prdBsn = new ProductoBsn();
            Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();
            prd.IdUser = user.Code;
            

            Entities.Producto producto = prdBsn.InsertarHit(prd);
            producto.IdUser = user.Code;
            producto.CodeUsrOrg = user.Code + "_" + user.Organization.Code;

            return serializer.Serialize(producto);
        }

        private List<KeyValuePair<string, string>> splitParams(string q)
        {
            var result = new List<KeyValuePair<string, string>>();

            q = q.Replace("+", "++") + "+";
            var param = new Regex(@"\+inmeta:(?<name>.*?)(%3D|:)(?<value>.*?)\+");

            foreach (Match p in param.Matches(q))
            {
                result.Add(new KeyValuePair<string, string>(p.Groups["name"].Value, p.Groups["value"].Value.Replace("%2520", "%20").Replace("%20", "").Replace("%252E", ".").Replace("%252D", "-")
                    .Replace("%C3%A1", "á").Replace("%C3%A9", "é").Replace("%C3%AD", "í").Replace("%C3%B3", "ó").Replace("%C3%BA", "ú")
                    .Replace("%C3%81", "Á").Replace("%C3%89", "É").Replace("%C3%8D", "Í").Replace("%C3%93", "Ó").Replace("%C3%9A", "Ú")
                    .Replace("%2F", "/")
                    .Replace("%2524","$")
                    ));
            }
            return result;
        }

        private List<KeyValuePair<string, string>> splitParamsRequiredFields(string q,string required)
        {
            var result = new List<KeyValuePair<string, string>>();

            //q = q.Replace("+", "++") + "+";
            var param = new Regex(@"\((?>[^()]+|    \( (?<number>)|    \) (?<-number>))*(?(number)(?!))\)");

            foreach (Match p in param.Matches(required))
            {                
                string[] splitDot = p.Value.Split(':');
                splitDot[0] = splitDot[0].Replace("(", "");
                splitDot[1] = splitDot[1].Replace(")", "").ToString();
                result.Add(new KeyValuePair<string, string>(splitDot[0], splitDot[1].Replace("%2520", "%20").Replace("%20", "").Replace("%252E", ".").Replace("%252D", "-")
                    .Replace("%C3%A1", "á").Replace("%C3%A9", "é").Replace("%C3%AD", "í").Replace("%C3%B3", "ó").Replace("%C3%BA", "ú")
                    .Replace("%C3%81", "Á").Replace("%C3%89", "É").Replace("%C3%8D", "Í").Replace("%C3%93", "Ó").Replace("%C3%9A", "Ú")
                    .Replace("%2F", "/")
                    .Replace("%24", "$")));
            }

            q = q.Replace("+", "++") + "+";
            q = "+" + q;
            var paramInmeta = new Regex(@"\+inmeta:(?<name>.*?)(%3D|:)(?<value>.*?)\+");

            foreach (Match p in paramInmeta.Matches(q))
            {
                result.Add(new KeyValuePair<string, string>(p.Groups["name"].Value, p.Groups["value"].Value.Replace("%2520", "%20").Replace("%20", "").Replace("%252E", ".").Replace("%252D", "-")
                    .Replace("%C3%A1", "á").Replace("%C3%A9", "é").Replace("%C3%AD", "í").Replace("%C3%B3", "ó").Replace("%C3%BA", "ú")
                    .Replace("%C3%81", "Á").Replace("%C3%89", "É").Replace("%C3%8D", "Í").Replace("%C3%93", "Ó").Replace("%C3%9A", "Ú")
                    .Replace("%2F", "/")));
            }

            return result;
        }

        [ActionName("Index"), HttpGet]
        public ActionResult Index()
        {
            return Index(" ");
        }

        [HttpPost]
        public PartialViewResult GetMasProveedores(Producto prod)
        {
            ProveedorBsn proveedorBsn = new ProveedorBsn();
            //Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();
            //ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();
            string session = Session.SessionID;

            Entities.Proveedor[] proveedor = proveedorBsn.proveedores(prod.Id, prod.IdConvenio, session);
            return PartialView("~/Views/Productos/Buscar/ResponseMasProveedores.cshtml", proveedor);
        }

        [HttpPost]
        public ActionResult GetProveedores(string text)
        {
            
            ProveedorBsn proveedorBsn = new ProveedorBsn();
            Entities.Proveedor[] provs = proveedorBsn.proveedoresByText(text);

            var proveedors = new List<KeyValuePair<string, string>>();
            if (provs != null && provs.Length > 0)
            {
                for (int i = 0; i < provs.Length; i++)
                {
                    proveedors.Add(new KeyValuePair<string, string>(provs.ElementAt(i).Id.ToString(), provs.ElementAt(i).Nombre.ToString()));
                }
            }
    
            
            var result = proveedors.Where(r => r.Value.ToLower().Contains(text.ToLower()))
                        .Select(r => new { id = r.Key, label = r.Value, name = r.Value });

          
            return Json(result);
        }

        
        [ActionName("Index"), HttpPost]
         public ActionResult Index(string q, int startsFrom = 0, int itemsPerPage = 10, string sortBy = "", string sortReverse = "false", string esMasVisto = "false",int ClickedItem=0,string input = "")
        {
            
            sortReverse = (sortReverse == null || sortReverse == "") ? "false" : sortReverse;
            bool sortRev = Convert.ToBoolean(sortReverse);
            if (!string.IsNullOrWhiteSpace(q) && (!bool.Parse(esMasVisto)))
            {
                string url = @"" + ConfigurationSettings.AppSettings["IP_GSA"] + "/search?client=publico&getfields=*&getfields=*&site=publico&filter=0&lr=lang_es&proxyreload=1&q=" + q;

                ViewBag.SliderReset = true;
                if (url.Contains(".."))
                {
                    ViewBag.SliderReset = false;
                }
                ParmModel PARMObject;
                List<RModel> RList;
                List<SearchParamModel> searchParams;

                var xmlParser = new XmlParser();
                var result = xmlParser.ParsebyUrl(url, sortBy, sortRev, ClickedItem, itemsPerPage, out PARMObject, out RList, out searchParams);
                ViewBag.itemsTotalCount = result.count;
                ViewBag.itemsCount = RList.Count;
                ViewBag.suggestionHtml = result.suggestionHtml;
                ViewBag.input = result.suggestionHtml;

                ViewBag.ClickedItem = ClickedItem;
                if (result.count < startsFrom)
                {
                    startsFrom = 0;
                }
                ViewBag.itemsStarts = startsFrom;
                ViewBag.Num = itemsPerPage;

                ViewBag.Products = RList;
                ViewBag.PARM = PARMObject;
                ViewBag.SearchParams = searchParams;

                //int minPrice = result.minPrice;
                //int maxPrice = result.maxPrice;

                var qParams = searchParams.Where(x => x.name == "q").SingleOrDefault();

                string SplitItem = qParams.original_value;
                string[] SplitItems = SplitItem.Split('+');

                string VItemName = SplitItems[0];
                List<KeyValuePair<string, string>> qParamsList = null;
                if (qParams != null)
                {
                    qParamsList = splitParams(qParams.original_value);
                }
                ViewBag.qParams = qParamsList;

                string urlItem = @"" + ConfigurationSettings.AppSettings["IP_GSA"] + "/search?btnG=Google+Search&client=publico&sort=date%3AD%3AL%3Ad1&oe=UTF-8&ie=UTF-8&ud=1&getfields=*&getfields=*&exclude_apps=1&site=publico&filter=0&lr=lang_es&proxyreload=1&q=" + VItemName;
                ParmModel PARMObjectItem;
                List<RModel> RListItem;
                List<SearchParamModel> searchParamsItem;

                var xmlParserItem = new XmlParser();
                var resultItem = xmlParser.ParsebyUrl(urlItem, sortBy, sortRev, startsFrom, itemsPerPage, out PARMObjectItem, out RListItem, out searchParamsItem);

                int minPrice = resultItem.minPrice;
                int maxPrice = resultItem.maxPrice;

                string min_value = minPrice.ToString();
                string max_value = maxPrice.ToString();
                if ((qParamsList as List<KeyValuePair<string, string>>).Where(x => x.Key == "PM").Count() > 0)
                {
                    string range = (qParamsList as List<KeyValuePair<string, string>>).Where(x => x.Key == "PM").First().Value;
                    min_value = range.Split('.')[0];
                    max_value = range.Split('.')[2];
                }

                ViewBag.min_value = min_value;
                ViewBag.max_value = max_value;

                ViewBag.minPrice = minPrice.ToString();
                ViewBag.maxPrice = maxPrice.ToString();

                ViewBag.slider = (maxPrice != int.MinValue && minPrice != int.MaxValue);
                ViewBag.sortReverse = sortReverse;
                ViewBag.sortBy = sortBy;
            }
            else if ((!String.IsNullOrEmpty(esMasVisto)) && (bool.Parse(esMasVisto)))
            {
                ProductoBsn idProductosBC = new ProductoBsn();
                Helpers.SessionHelper sessionHelper = new Helpers.SessionHelper();                
                ChileCompraExpress.Entities.Usuario user = sessionHelper.GetUser();
             

                string VItemName = idProductosBC.getProductosMasVisitadosResultPage(user.Code);

                ViewBag.q = input;

                string url = @"" + ConfigurationSettings.AppSettings["IP_GSA"] + "/search?btnG=Google+Search&client=publico&sort=date%3AD%3AL%3Ad1&oe=UTF-8&ie=UTF-8&ud=1&getfields=*&getfields=*&exclude_apps=1&site=publico&filter=0&proxyreload=1&lr=lang_es&requiredfields=" + VItemName + q;
                sortReverse = (sortReverse == null || sortReverse == "") ? "false" : sortReverse;
                sortRev = Convert.ToBoolean(sortReverse);

                ViewBag.SliderReset = true;
                if (url.Contains(".."))
                {
                    ViewBag.SliderReset = false;
                }
                ParmModel PARMObject;
                List<RModel> RList;
                List<SearchParamModel> searchParams;

                var xmlParser = new XmlParser();
                var result = xmlParser.ParsebyUrl(url, sortBy, sortRev, ClickedItem, itemsPerPage, out PARMObject, out RList, out searchParams);
                ViewBag.itemsTotalCount = result.count;
                ViewBag.itemsCount = RList.Count;
                ViewBag.suggestionHtml = result.suggestionHtml;

                ViewBag.ClickedItem = ClickedItem;
                if (result.count < startsFrom)
                {
                    startsFrom = 0;
                }
                ViewBag.itemsStarts = startsFrom;
                ViewBag.Num = itemsPerPage;

                ViewBag.Products = RList;
                ViewBag.PARM = PARMObject;
                ViewBag.SearchParams = searchParams;

                //int minPrice = result.minPrice;
                //int maxPrice = result.maxPrice;

                var RequiredParams = searchParams.Where(x => x.name == "requiredfields").SingleOrDefault();
                var qParams = searchParams.Where(x => x.name == "q").SingleOrDefault();

                string SplitItem = qParams.original_value;
                string[] SplitItems = SplitItem.Split('+');

                //SplitItems[0];
                List<KeyValuePair<string, string>> qParamsList = null;
                if (qParams != null)
                {
                    qParamsList = splitParamsRequiredFields(qParams.original_value, RequiredParams.original_value);
                }
                ViewBag.qParams = qParamsList;

                string urlItem = @""+ ConfigurationSettings.AppSettings["IP_GSA"] + "/search?btnG=Google+Search&client=publico&sort=date%3AD%3AL%3Ad1&oe=UTF-8&ie=UTF-8&ud=1&getfields=*&getfields=*&exclude_apps=1&site=publico&filter=0&proxyreload=1&requiredfields=" + VItemName;
                ParmModel PARMObjectItem;
                List<RModel> RListItem;
                List<SearchParamModel> searchParamsItem;

                var xmlParserItem = new XmlParser();
                var resultItem = xmlParser.ParsebyUrl(urlItem, sortBy, sortRev, startsFrom, itemsPerPage, out PARMObjectItem, out RListItem, out searchParamsItem);

                int minPrice = resultItem.minPrice;
                int maxPrice = resultItem.maxPrice;

                string min_value = minPrice.ToString();
                string max_value = maxPrice.ToString();
                if ((qParamsList as List<KeyValuePair<string, string>>).Where(x => x.Key == "PM").Count() > 0)
                {
                    string range = (qParamsList as List<KeyValuePair<string, string>>).Where(x => x.Key == "PM").First().Value;
                    min_value = range.Split('.')[0];
                    max_value = range.Split('.')[2];
                }

                ViewBag.min_value = min_value;
                ViewBag.max_value = max_value;

                ViewBag.minPrice = minPrice.ToString();
                ViewBag.maxPrice = maxPrice.ToString();

                ViewBag.slider = (maxPrice != int.MinValue && minPrice != int.MaxValue);
                ViewBag.sortReverse = sortReverse;
                ViewBag.sortBy = sortBy;
            }

            return View();
        }

        private string FindInnerText(RModel rmodel,string meta)
        {
            try
            {
                string retorno = "";
                for (int i = 0; i < rmodel.MTObjects.Count; i++)
                {
                    if (rmodel.MTObjects.ElementAt(i).N.Equals(meta))
                    {
                        retorno = rmodel.MTObjects.ElementAt(i).V;
                    }
                }

                return retorno;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            
        }

        public void ExportExcel(string q, int startsFrom = 0, int itemsPerPage = 900, string sortBy = "", string sortReverse = "false")
        {
            bool sortRev = Convert.ToBoolean(sortReverse);
            if (!string.IsNullOrWhiteSpace(q))
            {
                string[] qSplit = q.Split('&');
                string url = @"" + ConfigurationSettings.AppSettings["IP_GSA"] + "/search?btnG=Google+Search&client=publico&sort=date%3AD%3AL%3Ad1&oe=UTF-8&ie=UTF-8&ud=1&getfields=*&getfields=*&exclude_apps=1&site=publico&filter=0&proxyreload=1&lr=lang_es&q=" + qSplit[0] + "&requiredfields=" + qSplit[1];

                ParmModel PARMObject;
                List<RModel> RList;
                List<SearchParamModel> searchParams;

                var xmlParser = new XmlParser();
                var result = xmlParser.ParsebyUrl(url, sortBy, sortRev, startsFrom, itemsPerPage, out PARMObject, out RList, out searchParams);
                               

                /* SE CREA LA TABLA */
                Table tbl = new Table();
                tbl.Attribute = "border='1px solid'";
                Row row = null;
                Column col = null;

                //CABECERA
                row = new Row();
                row.Attribute = "style='font-weight: bold'";

                col = new Column();
                col.Valor = "Id de producto";
                row.addColumn(col);

                col = new Column();
                col.Valor = "Proveedor";
                row.addColumn(col);

                col = new Column();
                col.Valor = "Producto";
                row.addColumn(col);

                col = new Column();
                col.Valor = "Precio";
                row.addColumn(col);

                col = new Column();
                col.Valor = "Convenio";
                row.addColumn(col);

                tbl.addRow(row);


                for (int i = 0; i < RList.Count; i++)
                {
                    //BODY EXCEL
                    row = new Row();

                    //ID DEL PRODUCTO
                    col = new Column();
                    col.Valor = FindInnerText(RList.ElementAt(i), "IDP");
                    row.addColumn(col);

                    //PROVEEDOR MAS ECONOMICO
                    col = new Column();
                    col.Valor = FindInnerText(RList.ElementAt(i), "P");
                    row.addColumn(col);

                    //NOMBRE DEL PRODUCTO
                    col = new Column();
                    col.Valor = FindInnerText(RList.ElementAt(i), "NP");
                    row.addColumn(col);

                    ///////////////////////
                    string precio = FindInnerText(RList.ElementAt(i), "PR");
                    if (double.Parse(FindInnerText(RList.ElementAt(i), "PR").Replace(",", ".")) == 0)
                    {
                        precio = FindInnerText(RList.ElementAt(i), "PA");
                        
                    }                   


                    //PRECIO MAS ECONOMICO
                    col = new Column();
                    col.Valor = FindInnerText(RList.ElementAt(i), "MO") + " " + precio;
                    row.addColumn(col);

                    //CONVENIO
                    col = new Column();
                    col.Valor = FindInnerText(RList.ElementAt(i), "S");
                    row.addColumn(col);

                    tbl.addRow(row);
                }

                ITable table = new AdvancedTable(tbl, null);

                MemoryStream ms = new MemoryStream();
                StreamWriter sw = new StreamWriter(ms);

                sw.Write(table.ToString());
                sw.Flush();
                sw.Close();

                Byte[] bytes = Encoding.Convert(Encoding.UTF8, Encoding.Default, ms.ToArray());
                ms.Flush();
                ms.Close();
                string now = DateTime.Now.ToString().Replace("/", "_");
                now = now.Replace(":", "_");
                now = now.Replace(" ", "_");
                this.ControllerContext.HttpContext.Response.Clear();

                this.ControllerContext.HttpContext.Response.Buffer = true;
                this.ControllerContext.HttpContext.Response.ContentType = "application/vnd.ms-excel";
                this.ControllerContext.HttpContext.Response.Charset = String.Empty;
                this.ControllerContext.HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=resultados.xls");
                this.ControllerContext.HttpContext.Response.AddHeader("Content-Length", bytes.Length.ToString());

                this.ControllerContext.HttpContext.Response.BinaryWrite(bytes);
                this.ControllerContext.HttpContext.Response.Flush();
                this.ControllerContext.HttpContext.Response.Close();
                    
            }

        }
    }
}
