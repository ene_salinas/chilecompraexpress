﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using ChileCompraExpress.Web.Helpers;
using System.Security.Principal;
using System.Web.SessionState;
using System.Web.Http.WebHost;


namespace ChileCompraExpress.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            SessionStateServerSharedHelper.ChangeAppDomainAppId("Portal");

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);



        }
       

        //public override void Init()
        //{
        //    this.AuthenticateRequest += new EventHandler(WebApiApplication_AuthenticateRequest);
        //    base.Init();
        //}

        //protected void Application_PostAuthorizeRequest()
        //{
        //    if (IsWebApiRequest())
        //    {
        //        HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        //    }
        //}

        //private bool IsWebApiRequest()
        //{
        //    return HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith(WebApiConfig.UrlPrefixRelative);
        //}

        //void WebApiApplication_AuthenticateRequest(object sender, EventArgs e)
        //{
        //    HttpCookie cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
        //    if (cookie == null)
        //    {
        //        FormsAuthentication.SignOut();
        //            FormsAuthentication.RedirectToLoginPage();
        //            return;
        //    }
        //    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

        //    IIdentity id = new SampleIdentity(ticket);
        //    GenericPrincipal prin = new GenericPrincipal(id, null);


        //    //HttpContext.Current.Session = ;
        //    HttpContext.Current.User = prin;
        //    string x = HttpContext.Current.Session["checkedRoles"].ToString();

        //    bool? checkedRoles = (bool?)HttpContext.Current.Session["checkedRoles"];

        //    //checkedRoles = false;
        //    //if (!checkedRoles.HasValue || (checkedRoles.HasValue && !checkedRoles.Value))
        //    //{
        //    //    SessionHelper sessionHelper = new SessionHelper();
        //    //    // checkear roles
        //    //    if (!sessionHelper.CheckRoles())
        //    //    {
        //    //        //filterContext.Result = new EmptyResult();
        //    //        FormsAuthentication.SignOut();
        //    //        FormsAuthentication.RedirectToLoginPage();
        //    //        return;
        //    //    }

        //    //    Session["checkedroles"] = true;


        //    //}
        //    //FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);

        //    //SampleIdentity id = new SampleIdentity(ticket);
        //    //GenericPrincipal prin = new GenericPrincipal(id, null);

        //    //HttpContext.Current.User = prin;
        //}
    }

    public class MyHttpControllerHandler : HttpControllerHandler, IRequiresSessionState
    {
        public MyHttpControllerHandler(RouteData routeData)
            : base(routeData)
        {
        }
    }
    public class MyHttpControllerRouteHandler : HttpControllerRouteHandler
    {
        protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new MyHttpControllerHandler(requestContext.RouteData);
        }
    }
}