﻿
// Agrega una lista 
function nuevaLista() {

     
    var listas = bindings.listas.todas();
    var nueva = bindings.listas.nueva;

    if (!$.trim(nueva.Nombre())) return;

    var lista = { Nombre: nueva.Nombre(), Descripcion: nueva.Descripcion() };
    $.post(urls.listas, lista, function(id){
        lista.Id = id;
        listas.push(lista);
        bindings.listas.todas(listas);
    });

    bindings.listas.nueva.Nombre("");
    bindings.listas.nueva.Descripcion("");

    ocultarNuevaLista();
}

function ocultarNuevaLista() {
    $("#nuevaLista").hide();
    $("#nuevaListaLink").show();
}

//Visualiza la lista haciendo un llamado a Tienda2
function verLista(id,nombre)
{
    var paramHandlerIdLista = { "idLista": id , "nombreLista" : nombre};

    $.ajax({

            async: false,
            url: "/cmii/handlerGSA/HandlerAgregarALista.aspx",
            type: 'GET',
            cache: false,
            data: paramHandlerIdLista,
            success: function (data) {
                
               
                $.getJSON("/cmii/handlerGSA/HandlerGetSession.aspx")
                .done(function (items) {

                    $("#items").text(items);
                    var data = { "count": items };
                    $.get("/ChileCompraExpress.Web/Escritorio/SetSessionItems", data)
                    .done(function () {
                        iframe('/CMII/Tienda/frmListadoProductosEnLista.aspx');
                
                            $("#cerrar_modal").attr("data-dismiss", "modal");
                            $("#cerrar_modal").click();
                    });

                });
                   
                
                 
                
            }

        });
}

function agregarListasDeCompras(idLista,nombreLista)
{

     
    var params = { "idLista" : idLista , "nombreLista" : nombreLista };
     
    $.ajax({

            async: false,
            data: params,
            url: "/cmii/handlerGSA/HandlerListado.aspx",
            type: 'GET',
            cache: false,            
            success: function (data) {            
                $.getJSON("/cmii/handlerGSA/HandlerGetSession.aspx")
                .done(function (items) {

                    $("#items").text(items);
                    var data = { "count": items };
                    $.get("/ChileCompraExpress.Web/Escritorio/SetSessionItems", data)
                    .done(function () {
                        alert("La lista se ha asociado al carro correctamente.");
                    });

                });
               

            }

        });

    
}

// Elimina la lista de la posición i
function eliminarLista(i) {
    var listas = bindings.listas.todas();
    if (!confirm("¿Está seguro(a) que desea eliminar la lista "+ listas[i].Nombre +"?")) return;
    
    lista = listas[i];
    listas.splice(i, 1);
    bindings.listas.todas(listas);

    $.ajax({
        url : urls.listas + "/" + lista.Id,
        type: "DELETE",
    });
}