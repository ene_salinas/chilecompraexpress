﻿
jQuery(document).ready(function () {



    $("#tabs").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
    $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");


    $("#los_mas_visitados").click(function () {

        debugger;

        if (bindings.carruseles.inferiorLosMasVistos.objeto == null) {
            // Solicitando Los mas visitados
            $.ajax({
                url: "/ChileCompraExpress.Web/api/ProductosLosMasVisitados",
                dataType: 'json',
                async: false,
                success: function (respuesta) {
                    if (respuesta != null) {

                        debugger;

                        bindings.carruseles.inferiorLosMasVistos.objeto = $("#carruselInferiorLosMasVistos").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });

                        asignarRespuesta(respuesta, losMasVisitados);

                        actualizarCarrusel(losMasVisitados, bindings.carruseles.inferiorLosMasVistos);


                    }

                }
            });
        }
    });

    $("#recien_visitado").click(function () {


        if (bindings.carruseles.inferiorRecienVistos.objeto == null) {

            // Solicitando Productos Recien Vistos
            $.ajax({
                url: "/ChileCompraExpress.Web/api/ProductosRecienVisitados",
                dataType: 'json',
                async: false,
                success: function (respuesta) {


                    if (respuesta != null) {

                        bindings.carruseles.inferiorRecienVistos.objeto = $("#carruselInferiorRecienVistos").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });

                        asignarRespuesta(respuesta, recienVisitados);

                        actualizarCarrusel(recienVisitados, bindings.carruseles.inferiorRecienVistos);


                    }

                }
            });
        }

    });

    $("#mas_visitados").click(function () {

        if (bindings.carruseles.inferiorMasVistos.objeto == null) {

            // Solicitando Mas visitados por el usuario
            $.ajax({

                url: "/ChileCompraExpress.Web/api/ProductosMasVisitados/",
                dataType: 'json',
                async: false,
                success: function (respuesta) {
                    if (respuesta != null) {
                        debugger;                        

                        bindings.carruseles.inferiorMasVistos.objeto = $("#carruselInferiorMasVistos").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });

                        asignarRespuesta(respuesta, masVisitados);

                        actualizarCarrusel(masVisitados, bindings.carruseles.inferiorMasVistos);

                        //bindings.carruseles.inferiorMasVistos.objeto = $("#carruselInferiorMasVistos").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });



                    }

                }
            });
        }

    }
    );

});

// Extendiendo contenedor de urls para acceso en los scripts isolados
$.extend(urls, {

    ofertasEspeciales: "api/OfertasEspeciales",
    productosRecienVistos: "api/ProductosRecienVistos",
    productosMasVistos: "api/ProductosMasVistos",
    categorias: "api/Categorias",
    regionesDespacho: "api/RegionesDespacho",
    informaciones: "api/Informaciones"

});

// Cantidad de items visibles en el carrusel
var cantidad = 4;

// Datos recibidos en la consulta al API
var ofertasEspeciales = [];
var recienVisitados = [];
var masVisitados = [];
var losMasVisitados = [];

// Extendiendo bindings para knockoutjs
$.extend(bindings, {

    // Controladores de carruseles
    carruseles: {
        superior: { objeto: null, paquetes: ko.observableArray() },
        inferiorRecienVistos: { objeto: null, paquetes: ko.observableArray() },
        inferiorMasVistos: { objeto: null, paquetes: ko.observableArray() },
        inferiorLosMasVistos: { objeto: null, paquetes: ko.observableArray() }
    },

    // Controladores de regiones de despacho
    regiones: {
        confirmadas: [],
        propuestas: ko.observableArray()
    },

    //LifetimePoints: ko.observable((data != null) ? data.LifetimePoints : 0),

    // Controladores de informaciones
    informaciones: {
        todas: [],                         // Todas las informaciones recibidas por API
        mostradas: ko.observableArray(),    // Informaciones que están siendo mostradas en el sidebar de la derecha
        detallada: {
            Id: ko.observable(),
            Titulo: ko.observable(),
            Texto: ko.observable(),
            UrlImagen: ko.observable(),
            UrlDescarga: ko.observable()
        }

    }

});



function modalAcceptRegiones() {

    if (bindings.top_regiones.propuestas().length > 0) {
        var idRegion = $('select.region option:selected').val();

        var temp = bindings.top_regiones.propuestas();

        

        $("#btn_aceptar_regiones").attr("data-dismiss", "modal");

        var ids = "";
        for(var i = 0; i < bindings.top_regiones.propuestas().length;i++)
        {
            ids += temp[i].id;
            ids += (i == bindings.top_regiones.propuestas().length - 1) ? "" : ",";
        }

        
        var idregiones = { 'idregiones': ids };

        //SE INSERTAN LAS REGIONES SELECCIONADAS
        $.get("/ChileCompraExpress.Web/api/RegionesDespacho/", idregiones, function (respuesta) {
            alert(respuesta);



            if (($("#carruselSuperior").length) || ($("#carruselInferiorRecienVistos").length)
                || ($("#carruselInferiorMasVistos").length) || ($("#carruselInferiorLosMasVistos").length)) {


                var urlOferta = "Api/OfertasEspeciales"; //'<%= Url.Action("OfertasEspeciales", "Api" ) %>';

                // Solicitando ofertas especiales
                $.ajax({
                    url: urlOferta,
                    dataType: 'json',
                    async: false,
                    success: function (respuesta) {
                        if (respuesta != null) {
                            debugger;
                            if (bindings.carruseles.superior != null) {

                                asignarRespuesta(respuesta, ofertasEspeciales);

                                actualizarCarrusel(ofertasEspeciales, bindings.carruseles.superior);
                            }

                        }
                    }
                });

                if (bindings.carruseles.superior != null) {
                    bindings.carruseles.superior.objeto = $("#carruselSuperior").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });
                }

                debugger;
                bindings.carruseles.inferiorRecienVistos.objeto = $("#carruselInferiorRecienVistos").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });
//                bindings.carruseles.inferiorMasVistos.objeto = $("#carruselInferiorMasVistos").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });
//                bindings.carruseles.inferiorLosMasVistos.objeto = $("#carruselInferiorLosMasVistos").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });


                $(".slider").removeAttr("visibility");
                $(".slider").attr("style", "visibility: visible");
                $(".slider").slideDown(300).fadeIn(700);



            }
        }, "json");

        

        

    }
    else {

        alert("Debe seleccionar al menos una region para continuar.");

        return false;
    }

}

// Cancela todas las regiones propuestas y restaura las confirmadas
function cancelarRegionesPropuestas() {
    var propuestas = [];

    if (bindings.top_regiones.propuestas().length > 0) {
        for (i = 0; i < bindings.regiones.confirmadas.length; i++)
            propuestas.push({ id: bindings.regiones.confirmadas[i], text: regiones[bindings.regiones.confirmadas[i]] });
        bindings.regiones.propuestas(propuestas);
       
       if (($("#carruselSuperior").length) || ($("#carruselInferiorRecienVistos").length)
                || ($("#carruselInferiorMasVistos").length) || ($("#carruselInferiorLosMasVistos").length)) {
                

                var urlOferta = "Api/OfertasEspeciales";

                // Solicitando ofertas especiales
                $.ajax({
                    url: urlOferta,
                    dataType: 'json',
                    async: false,
                    success: function (respuesta) {
                        if (respuesta != null) {
                            debugger;
                            asignarRespuesta(respuesta, ofertasEspeciales);
                            actualizarCarrusel(ofertasEspeciales, bindings.carruseles.superior);

                        }
                    }
                });

                bindings.carruseles.superior.objeto = $("#carruselSuperior").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });
                bindings.carruseles.inferiorRecienVistos.objeto = $("#carruselInferiorRecienVistos").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });
                bindings.carruseles.inferiorMasVistos.objeto = $("#carruselInferiorMasVistos").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });
                bindings.carruseles.inferiorLosMasVistos.objeto = $("#carruselInferiorLosMasVistos").bxSlider({ infiniteLoop: true, hideControlOnEnd: true });

                $(".slider").removeAttr("visibility");
                $(".slider").attr("style", "visibility: visible");
                $(".slider").slideDown(300).fadeIn(700);
            }

        $("#cerrarModalRegiones").attr("data-dismiss", "modal");
        $("#btn_cerrar_regiones").attr("data-dismiss", "modal");
    }
    else {

        alert("Debe seleccionar al menos una region para continuar.");

        return false;
    }


}


function irAresultados(nombre) {


    $("#q").val(nombre);
    buscar('false');


}


function despliegeCategorias(tipo, idListaDespliegue) {

    debugger;

    var CurrIDOpenStatus = false;
    if ($("#despliegue_nivel2_" + idListaDespliegue).is(':hidden')) {
        CurrIDOpenStatus = true;
    }

    //Reset All
    $(".despliegue_nivel2Class").attr("style", "overflow:hidden; display:none;");
    $('.despliegue_nivel2Class').find('div').remove();
    $(".btn_lista_despClass").attr('src', 'Content/Estilos/Imagenes/botones/btn_mas.png');

    //$(".btn_lista_despClass").html();
    //$("p").remove(":contains('Hello')");

    $("#despliegue_nivel2_" + idListaDespliegue).slideToggle("fast",
    function () {
        
        debugger;
        if (CurrIDOpenStatus==true) {

            debugger;
            

            var id = { "id": idListaDespliegue };
            var countLi = $("#n2_" + idListaDespliegue + " li").size();


            $.ajax({
                async: false,
                url: "/ChileCompraExpress.Web/Categorias/CategoriaSubNivel",
                type: 'POST',
                data: id,
                dataType: "json",
                cache: false,
                success: function (data) {

                    debugger;
                    //Se carga el template de Categoria Nivel 2
                    $.get(
		                '/ChileCompraExpress.Web/Templates/Categorias/tmplCategoriasN2.html',
		                function (tmpl) {

		                    var output = Mustache.render(tmpl, data);

		                    $("#despliegue_nivel2_" + idListaDespliegue).append(output);

		                    $("#btn_lista_desp_" + idListaDespliegue).attr('src', 'Content/Estilos/Imagenes/botones/btn_menos.png');

		                    $("footer").attr("style", "margin:20px 0px");
		                }
                    );

                }
            });


        }
        else {

            $("#btn_lista_desp_" + idListaDespliegue).attr('src', 'Content/Estilos/Imagenes/botones/btn_mas.png');
            $("footer").attr("style", "margin:250px 0px");
            $("#despliegue_nivel2_" + idListaDespliegue).empty();


        }
       // $("#despliegue_nivel2_" + idListaDespliegue).remove(output); //rohit
        return false;
    }

    );


}

function despliegeCategoriasN3(idListaDespliegue) {


    var id = { "id": idListaDespliegue };
    debugger;

    //$('.izq mediano negrita size').find('li').remove();

    //$('.subr nivel3').remove();

    $("ul li.nivel3").remove();

    var countLi = $("#n2_" + idListaDespliegue + " li").size();

    if (countLi == 1) {
        $.ajax({
            async: false,
            url: "/ChileCompraExpress.Web/Categorias/CategoriaSubNivelN3",
            type: 'POST',
            data: id,
            dataType: "json",
            cache: false,
            success: function (data) {

                debugger;
                //Se carga el template de Categoria Nivel 3
                $.get(
		            '/ChileCompraExpress.Web/Templates/Categorias/tmplCategoriasN3.html',
		            function (tmpl) {
		                debugger;
		                var output = Mustache.render(tmpl, data);

		                $("#n2_" + idListaDespliegue).hide();
		                $("#n2_" + idListaDespliegue).append(output);
		                $("#n2_" + idListaDespliegue).show().slideDown("medium");

		            });
            }

        });
    }
	else {
		    $("#n2_" + idListaDespliegue + ".nivel3").empty();
    }

}