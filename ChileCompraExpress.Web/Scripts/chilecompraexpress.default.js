﻿/// <reference path="jquery-1.7.1.js" />
var ChileCompraExpress = {};

ChileCompraExpress.Helpers = (function () {

    //#region Private members
    var global = {};
    global.URL = "/";
    global.MouseIsInside = false;
    global.PortalLoginURL = "/portal/login.aspx";
    global.HandlerCarroCompraURL = "/cmii/tienda/carrocompra.sso";
    global.HandlerListaCompraURL = "/cmii/tienda/listacompra.sso";
    global.HandlerFichaProductoURL = "/cmii/handlerGSA/HandlerSession.aspx";

    var ShowMenu = function (event) {
        event.preventDefault();
        $(event.currentTarget).find("ul").css("visibility", "visible");
    }

    var HideMenu = function (event) {
        event.preventDefault();
        $(event.currentTarget).find("ul").css("visibility", "hidden");
    }

    var CrearLista = function (event) {
        event.preventDefault();

        var params = {
            "nombre": $("#crearListaModel_Nombre").val(),
            "descripcion": $("#crearListaModel_Descripcion").val()
        };

        var url = global.HandlerListaCompraURL + "?action=new&" + $.param(params);

        $.getJSON(url, function (data) {
            $("#nueva-lista-form").hide();

            $("#nueva-lista-confirmacion-respuesta").text(data.Error ?
                "Ha ocurrido un error al intentar crear la lista." :
                "La lista de compra se guardó con éxito.");

            $("#nueva-lista-confirmacion").show();

            if (data.LoggedOut) {
                alert(data.ErrorMessage);
                window.top.location = global.PortalLoginURL;
            }
        });
    }

    var ShowCrearLista = function (event) {
        event.preventDefault();
        $("#menu-nueva-lista").addClass("selected");
        $("#crearListaModel_Nombre").val("");
        $("#crearListaModel_Descripcion").val("");
        $("#nueva-lista-form").show();
        $("#nueva-lista-container")
            .css({ "visibility": "none", "display": "block" })
            .position({ my: "right top", at: "right bottom", collision: "fit", of: $("#header") })
            .show();
    }

    var HideCrearLista = function (event) {
        event.preventDefault();
        $("#menu-nueva-lista").removeClass("selected");
        $("#nueva-lista-confirmacion").hide();
        $("#nueva-lista-container").hide();
    }

    var ShowTooltip = function (event) {
        event.preventDefault();
        var id = public.ParseElementsId(event.currentTarget);
        $("#" + id + "-tooltip").show();
    }

    var HideTooltip = function (event) {
        event.preventDefault();
        var id = public.ParseElementsId(event.currentTarget);
        $("#" + id + "-tooltip").hide();
    }

    //#endregion

    //#region Public members

    var public = {};

    public.SetURL = function (url) {
        global.URL = url;
    },

    public.GetURL = function (path) {
        return global.URL + path;
    },

    public.ParseId = function (text) {
        var output = "";
        var index = text.lastIndexOf("_");

        if (index > -1)
            output = text.substring(index + 1, text.length);

        return output;
    },

    public.ParseElementsId = function (element) {
        var id = $(element).attr("id");
        return public.ParseId(id);
    },

    public.GetIdFromName = function (name) {
        return name.replace(".", "_");
    },

    public.GetPortalLoginURL = function () {
        return global.PortalLoginURL;
    },

    public.GetHandlerCarroCompraURL = function () {
        return global.HandlerCarroCompraURL;
    },

    public.GetHandlerListaCompraURL = function () {
        return global.HandlerListaCompraURL;
    },

    public.GetHandlerFichaProductoURL = function () {
        return global.HandlerFichaProductoURL;
    },

    public.HideModales = function () {
        $(".modal").each(function (index, element) {
            if ($("#buscador-avanzado").is(":visible"))
                $("#btn-buscador-avanzado").click();
            if ($("#nueva-lista-container").is(":visible"))
                $("#menu-nueva-lista a").click();
            $("#sello-info-container").hide();
            $(".tooltip").hide();
        });
    },

    public.Setup = function () {

        $.ajaxSetup({ cache: false });

        $("#menu .menu").hover(ShowMenu, HideMenu);

        // listas de compra
        $("#menu-nueva-lista a").toggle(ShowCrearLista, HideCrearLista);
        $("#btn-crear-lista").click(CrearLista);
        $("#btn-cerrar-lista").click(function (event) {
            event.preventDefault();
            $("#menu-nueva-lista a").click();
        });

        // carro de compras
        $.getJSON(global.HandlerCarroCompraURL, function (data) {
            if (data.LoggedOut) {
                alert(data.ErrorMessage);
                window.top.location = global.PortalLoginURL;
            }
            else {
                $("#carro").find("span").text("Mi carro (" + data.ProductsCount + ")");
            }
        });
    },

    public.SetupToolTips = function () {
        $(".btn-tooltip").mouseenter(ShowTooltip);
        $(".btn-tooltip").click(ShowTooltip);
        $(".tooltip a.btn-close").click(HideTooltip);

        // ventanas modales
        $(".modal").hover(function () {
            global.MouseIsInside = true;
        }, function () {
            global.MouseIsInside = false;
        });

        $("body").mouseup(function () {
            if (!global.MouseIsInside)
                public.HideModales();
        });
    }

    return public;

    //#endregion

} ());
