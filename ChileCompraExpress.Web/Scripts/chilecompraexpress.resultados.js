﻿/// <reference path="jquery-1.7.1.js" />
/// <reference path="chilecompraexpress.buscador.js" />
/// <reference path="chilecompraexpress.default.js" />

ChileCompraExpress.Resultados = (function () {

    //#region Private members

    var global = {};
    global.goto_input = "";
    global.fichaProductoURL = '/cmii/tienda/frmdetalleproducto.aspx';

    var AgregaCarroCompra = function (event) {
        event.preventDefault();

        var id = ChileCompraExpress.Helpers.ParseElementsId(event.currentTarget);
        var form = $("#frm-agregar-carro_" + id);
        var error = $("#frm-agregar-carro-error_" + id);
        var exito = $("#frm-agregar-carro-exito_" + id);

        var input = form.find('input[type="text"]');
        var cantidad = +input.val();

        if (!cantidad || cantidad < 0) {
            input.select().focus();
            error.show();
            return;
        }

        var url = ChileCompraExpress.Helpers.GetHandlerCarroCompraURL() + "?action=add&" + form.serialize();

        $.getJSON(url, function (data) {
            if (data.LoggedOut) {
                alert(data.ErrorMessage);
                window.top.location = ChileCompraExpress.Helpers.GetPortalLoginURL();
            }
            else {
                $("#carro").find("span").text("Mi carro (" + data.ProductsCount + ")");
            }
        });

        exito.show();
        error.hide();
        input.val("");
    }

    var SetupFichaProductoURL = function (id) {
        var values = id.split("|");

        var params = {
            "productoId": values[0],
            "convenioId": values[1],
            "formatoId": values[2],
            "categoriaId": values[3],
            "tipoCategoriaId": values[4]
        };

        return ChileCompraExpress.Helpers.GetHandlerFichaProductoURL() + "?" + $.param(params);
    }

    var DenunciaIngresada = function () {
        ChileCompraExpress.Resultados.HidePopupDenuncia()
        alert('Denuncia Ingresada');
    }

    //#endregion

    //#region Public members

    var public = {};

    public.ShowPopupSello = function (code) {
        var url = ChileCompraExpress.Helpers.GetURL("Search/Sello/" + code);

        $.getJSON(url, function (data) {
            if (!data.Error) {
                $("#sello-info-imagen").remove();
                $("#sello-info-nombre").text(data.Nombre);

                $("<img/>").attr({
                    "id": "sello-info-imagen",
                    "src": data.PathImagen,
                    "alt": data.Nombre
                }).insertBefore("#sello-info-descripcion");

                $("#sello-info-descripcion").text(data.Descripcion);

                if (window.PIE)
                    PIE.attach($("#sello-info")[0]);

                $("#sello-info-container")
                    .css({ "display": "block" })
                    .position({ my: "center", at: "center", of: $(window) })
                    .show();
            }
        });
    },

    public.HidePopupSello = function () {
        $("#sello-info-container").hide();

        if (window.PIE)
            PIE.detach($("#sello-info")[0]);
    },

    public.ShowPopupDenuncia = function (code, proveedor, element) {
        $("#proveedorId").val(proveedor)
        $("#productoId").val(code);

        $("#denuncia-tooltip")
            .css({ "display": "block" })
            .position({ my: "right bottom", of: $(element), offset: "50 500" })
            .show();
    },

    public.HidePopupDenuncia = function () {
        $("#proveedorId").val("")
        $("#productoId").val("");
        $("#denuncia").val("");

        $(".btn-delete").each(function () {
            $(this).click();
        });

        $("#denuncia-tooltip").hide();

        if (window.PIE)
            PIE.detach($("#denuncia-tooltip")[0]);
    },

    public.ToggleModifiers = function (element, code) {
        var modifiers = $("#modificadores-visible-false_" + code);

        if (modifiers.is(":visible")) {
            modifiers.hide();
            $(element).text("[+]");
        } else {
            modifiers.show();
            $(element).text("[-]");
        }
    },

    public.AddModifier = function (element, navigator, code) {
        ChileCompraExpress.Buscador.AddModifier(navigator, code);
    },

    public.RemoveModifier = function (element, navigator, code) {
        ChileCompraExpress.Buscador.RemoveModifier(navigator, code);
    },

    public.ChangeCurrentPage = function (element, event) {
        event = event || window.event;
        if (event.keyCode == "13") {
            var page = $(element).val();
            ChileCompraExpress.Buscador.ChangeCurrentPage(page);
        }
    },

    public.ShowFichaProducto = function (element, code) {
         
        var url = SetupFichaProductoURL(code);
        location.href = url;
//        $.getJSON(url, function (data) {
//            if (data.LoggedOut) {
//                alert(data.ErrorMessage);
//                window.top.location = ChileCompraExpress.Helpers.GetPortalLoginURL();
//            }
//            if (!data.Error)
//                window.location = '/cmii/tienda/frmdetalleproducto.aspx';
//        });
    },

    public.ShowFichaProductoProveedor = function (element, code) {
        var id = code.split("&");
        var url = SetupFichaProductoURL(id[0]);

        $.getJSON(url, function (data) {
            if (data.LoggedOut) {
                alert(data.ErrorMessage);
                window.top.location = ChileCompraExpress.Helpers.GetPortalLoginURL();
            }
            if (!data.Error)
                window.location = '/cmii/tienda/frmdetalleproducto.aspx#' + id[1];
        });
    },

    public.ToggleProveedores = function (element, code) {
        var proveedores = $("#mas-proveedores_" + code);

        if (proveedores.is(":visible")) {
            $(element).removeClass("opened");
            proveedores.hide();
        } else {
            $(element).addClass("opened");
            proveedores.show();
        }
    },

    public.Setup = function () {

        // campo ordenamiento
        $("#results-options-sort-dropdown .results-options-sort-dropdown-field").click(function (event) {
            event.preventDefault();
            var value = ChileCompraExpress.Helpers.ParseElementsId(event.currentTarget);
            $("#results-options-sort-dropdown a:first").text($(event.currentTarget).text());
            ChileCompraExpress.Buscador.ChangeSortingField(value);
        });

        // expande las ramas seleccionadas en el navegador convenio
        $("#navegador_conveniocategoriasnavigator .btn-contraer.selected").click();

        // botones "agregar al carro"
        $("#productos .oferta-formulario-input").find('input[type="text"]').keydown(function (event) {
            if (event.keyCode == "13") {
                event.preventDefault();
                AgregaCarroCompra(event);
            }
        });

        $("#productos .oferta-formulario-btn").click(AgregaCarroCompra);

        var options = {
            target: '',
            //            timeout: 3000,
            url: "/ChileCompraExpress.Web/Search/DenunciaPrecioCaro",
            success: function () {
                //ChileCompraExpress.Resultados.HidePopupDenuncia();
                alert("Reporte ingresado\n\nSu reporte ha sido ingresado satisfactoriamente.\nEl precio reportado será gestionado pero su variación no será automática.");
            },
            error: function () {
                //ChileCompraExpress.Resultados.HidePopupDenuncia();
                alert("No se ha podido ingresar su reporte. Inténtelo más tarde.");
            }
        };


        $(document).ready(function () {
            $('#form-denuncia').ajaxForm(options);
        });

        //        $(function () { // wait for document to load
        //            $('#files').MultiFile({
        //                afterFileAppend: function (element, value, master_element) {
        //                    if ($("#file-list .MultiFile-label").length > 0)
        //                        $("#file-list-title").show();
        //                    else
        //                        $("#file-list-title").hide();
        //                },
        //                afterFileRemove: function (element, value, master_element) {
        //                    if ($("#file-list .MultiFile-label").length > 0)
        //                        $("#file-list-title").show();
        //                    else
        //                        $("#file-list-title").hide();
        //                },
        //                STRING: {
        //                    remove: '<div class="btn-delete"></div>',
        //                    duplicate: 'El archivo "$file" ya ha sido seleccionado.'
        //                 },
        //                list: '#file-list',
        //                max: 3
        //            });
        //        });

    }

    return public;

    //#endregion

} ());
