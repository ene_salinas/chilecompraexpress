﻿var regiones = [
	
];

// Confirma todas las regiones propuestas
function confirmarRegionesPropuestas() {
    var propuestas = bindings.regiones.propuestas();
    bindings.regiones.confirmadas = [];
    for (i = 0; i < propuestas.length; i++) {
        bindings.regiones.confirmadas.push(propuestas[i].id);
    }

    
}

// Cancela todas las regiones propuestas y restaura las confirmadas
function cancelarRegionesPropuestas() {
     
    if (bindings.top_regiones.propuestas().length > 0) {
        var propuestas = [];
        for (i = 0; i < bindings.regiones.confirmadas.length; i++)
            propuestas.push({ id: bindings.regiones.confirmadas[i], text: regiones[bindings.regiones.confirmadas[i]] });
        bindings.regiones.propuestas(propuestas);

        $("#cerrarModalRegiones").attr("data-dismiss", "modal");
    }
    else {

        alert("Debe seleccionar al menos una region para continuar.");

        return false;

    }
}

// Agrega una región de despacho
function agregarRegion() {

     
    var regionesTemp = bindings.top_regiones.propuestas();
    var selectedItem = this.options[this.selectedIndex];

    if (selectedItem.value != "-1") {
        var repetido = false;
        var no_repetido = "";
        for (i = 0; i < regionesTemp.length; i++) {
            if (regionesTemp[i].val == selectedItem.value) {
                repetido = true;
                break;
            } else {
                no_repetido = regionesTemp[i].val;
            }

        }

        if (!repetido && selectedItem.value) {
            debugger;
            regionesTemp.push({ id: selectedItem.value, text: selectedItem.label, val: no_repetido });
            bindings.top_regiones.propuestas(regionesTemp);
        }

        this.selectedIndex = 0;
    }
}

function agregarTopRegion() {


    var regionesTemp = bindings.top_regiones.propuestas();
    var selectedItem = this.options[this.selectedIndex];
    debugger;
    if (selectedItem.value != "-1") {
        var repetido = false;
        var no_repetido = "";
        for (i = 0; i < regionesTemp.length; i++) {
            if (regionesTemp[i].id == selectedItem.value) {
                repetido = true;
                break;
            } else {
                no_repetido = regionesTemp[i].val;
            }         
        }

        if (!repetido && selectedItem.value) {
            debugger;
            regionesTemp.push({ id: selectedItem.value, text: selectedItem.label, val: no_repetido });
            bindings.top_regiones.propuestas(regionesTemp);
        }
        

        this.selectedIndex = 0;
    }
}

function agregarTopRango() {

    var regionesTemp = bindings.top_rango.propuestas();
    var selectedItem = this.options[this.selectedIndex];

    var repetido = false;
    for (i = 0; i < regionesTemp.length; i++)
        if (regionesTemp[i].id == selectedItem.value) {
            repetido = true;
            break;
        }

    if (!repetido && selectedItem.value) {
        regionesTemp.push({ id: selectedItem.value, text: selectedItem.label, val: selectedItem.value });
        bindings.top_rango.propuestas(regionesTemp);
    }

    this.selectedIndex = 0;
}
// Elimina una región de despacho
function eliminarRegion(id) {
     
    var regionesTemp = bindings.top_regiones.propuestas();
    for (i = 0; i < regionesTemp.length; i++)
        if (regionesTemp[i].id == id) {
            regionesTemp.splice(i, 1);
            break;
        }
    bindings.top_regiones.propuestas(regionesTemp);
}

$(function () {
//    $("[name=region]").each(function () {

//        debugger;
//        for (i = 1; i < regiones.length; i++)

//            this.options[i] = new Option(regiones[i], i);
//    });

//    $("[name=top_region]").each(function () {

//        debugger;
//        for (i = 1; i < regiones.length; i++)

//            this.options[i] = new Option(regiones[i], i);
//    });


    $("[name=region]").change(agregarRegion);
    $("[name=top_region]").change(agregarTopRegion);
    $("[name=rango]").change(agregarTopRango);
});


