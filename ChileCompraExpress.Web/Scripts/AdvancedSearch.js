﻿



var regiones = [
	"--------",
	"Arica y Parinacota",
	"Tarapacá",
	"Antofagasta",
	"Atacama",
	"Coquimbo",
	"Valparaíso",
	"Metropolitana",
	"Libertador Bernardo O'Higgins",
	"Maule",
	"Biobío",
	"Araucanía",
	"Los Ríos",
	"Los Lagos",
	"Aysén del General Carlos Ibáñez del Campo",
	"Magallanes y de la Antártica Chilena",
];





// Confirma todas las regiones propuestas
function confirmarRegionesPropuestas() {
    var propuestas = bindings.regiones.propuestas();
    bindings.regiones.confirmadas = [];
    for (i = 0; i < propuestas.length; i++)
        bindings.regiones.confirmadas.push(propuestas[i].id);
}



// Agrega una región de despacho
function agregarRegion() {

     
    var regionesTemp = bindings.top_regiones.propuestas();
    var selectedItem = this.options[this.selectedIndex];

    var repetido = false;
    for (i = 0; i < regionesTemp.length; i++)
        if (regionesTemp[i].id == selectedItem.value) {
            repetido = true;
            break;
        }

    if (!repetido && selectedItem.value) {
        regionesTemp.push({ id: selectedItem.value, text: selectedItem.label, val: selectedItem.value });
        bindings.top_regiones.propuestas(regionesTemp);
    }

    this.selectedIndex = 0;
}

function agregarTopRegion() {
     
    var regionesTemp = bindings.top_regiones.propuestas();
    var selectedItem = this.options[this.selectedIndex];

    var repetido = false;
    for (i = 0; i < regionesTemp.length; i++)
        
        if (regionesTemp[i].id == selectedItem.value) {
            repetido = true;
            break;
        }

    if (!repetido && selectedItem.value) {
        regionesTemp.push({ id: selectedItem.value, text: selectedItem.label, val: selectedItem.value });
        bindings.top_regiones.propuestas(regionesTemp);
    }

    this.selectedIndex = 0;
}

function agregarTopRango() {

    var regionesTemp = bindings.top_rango.propuestas();
    var selectedItem = this.options[this.selectedIndex];
    
    if(regionesTemp.length == 0)
    {
        var repetido = false;
        for (i = 0; i < regionesTemp.length; i++)
            if (regionesTemp[i].id == selectedItem.value) {
                repetido = true;
                break;
            }

        if (!repetido && selectedItem.value) {
            regionesTemp.push({ id: selectedItem.value, text: selectedItem.label, val: selectedItem.value });
            bindings.top_rango.propuestas(regionesTemp);
        }
    }

    this.selectedIndex = 0;
}

// Elimina una región de despacho
function eliminarRango(id) {
     
    var rangoTemp = bindings.top_rango.propuestas();
    for (i = 0; i < rangoTemp.length; i++)
        if (rangoTemp[i].id == id) {
            rangoTemp.splice(i, 1);
            break;
        }
    bindings.top_rango.propuestas(rangoTemp);
}

// Elimina una región de despacho
function eliminarRegion(id) {

    var regionesTemp = bindings.top_regiones.propuestas();

    alert(regionesTemp);
    for (i = 0; i < regionesTemp.length; i++)
        if (regionesTemp[i].id == id) {
            regionesTemp.splice(i, 1);
            break;
        }
        bindings.top_regiones.propuestas(regionesTemp);
}

function agregarProvidors(value, label) {

     
    var providorsTemp = bindings.providors.propuestas();

    var repetido = false;
    for (i = 0; i < providorsTemp.length; i++)
        if (providorsTemp[i].id == value) {
            repetido = true;
            break;
        }

    if (!repetido && value) {
        providorsTemp.push({ id: value, text: label, val: value });
        bindings.providors.propuestas(providorsTemp);
    }

}

// Elimina un proveedor
function eliminarProveedor(id) {
     
    var providorsTemp = bindings.providors.propuestas();
    for (i = 0; i < providorsTemp.length; i++)
        if (providorsTemp[i].id == id) {
             
            providorsTemp.splice(i, 1);
            break;
        }
        bindings.providors.propuestas(providorsTemp);
}

// Limpia Proveedores
function limpiarProveedores() {
    var providorsTemp = bindings.providors.propuestas();
    for (i = 0; i < providorsTemp.length; i++)
    {
        providorsTemp.splice(i, 1);
        
    }
    bindings.providors.propuestas(providorsTemp);
}

$(function () {

    $("[name=region]").change(agregarTopRegion);
    $("[name=top_region]").change(agregarTopRegion);
    $("[name=rango]").change(agregarTopRango);

    $('[name=proveedor]').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/ChileCompraExpress.Web/Productos/GetProveedores', type: "POST", dataType: "json",                
                data: { text: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.label, value: item.label, id: item.id };
                    }));

                }
            })
        },
        select: function (event, ui) {
            if (ui.item) {
                agregarProvidors(ui.item.id, ui.item.label);
            }
        }

    });
});


