﻿var offset_global = 0;

function trimInformationText(text) {
    return text.length > 55 ? $.trim(text.slice(0, 55)) + "..." : text;
}

function mostrarGrupoInformaciones(offset1, offset2) {
     
    offset_global += offset2;
    var grupo = bindings.informaciones.todas.slice(offset1, offset_global);
    bindings.informaciones.mostradas(grupo);
}

function descargarPdf() {

     
    var title = $('#titulo').text();
    var texto = $('#texto').text();
    var informacion_location = '/ChileCompraExpress.Web/PdfDocument/Index';
    var informacion = "title="+ title +"&texto=" + texto;
    $.download(informacion_location, informacion, "post");
    
}

jQuery.download = function (url, data, method) {
    //url and data options required
    if (url && data) {
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : jQuery.param(data);
        //split params into form inputs
        var inputs = '';
        jQuery.each(data.split('&'), function () {
            var pair = this.split('=');
            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
        });
        //send request
        jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>')
		.appendTo('body').submit().remove();
    };
};

function seleccionarInformacion(id) {


    for (var i = 0; i < bindings.informaciones.todas.length; i++)
        if (bindings.informaciones.todas[i].Id == id) {
            var info = bindings.informaciones.todas[i];
            bindings.informaciones.detallada.Id(info.Id);
            bindings.informaciones.detallada.Titulo(info.Titulo);
            bindings.informaciones.detallada.Texto(info.Texto);
            bindings.informaciones.detallada.UrlImagen(info.UrlImagen);
            bindings.informaciones.detallada.UrlDescarga(info.UrlDescarga);
            return;

        } 
}