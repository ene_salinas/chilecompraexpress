﻿ 
// Actualiza un carrusel en vivo
function actualizarCarrusel(fuente, carrusel) {

     
    if (fuente != null && fuente.length > 0 && carrusel != null) {

        //
        var paquetesTemp = [];
        for (i = 0; i < fuente.length; i += cantidad) {
            paquetesTemp.push({ paquete: fuente.slice(i, Math.min(i + cantidad, fuente.length)) });
        }
        
        // Actualizando los datos dinámicos en vivo con el paquete
        carrusel.paquetes(paquetesTemp);

        //Actualizando el objeto del carrusel
        if (carrusel.objeto != null && carrusel.objeto.length > 0) {
            carrusel.objeto.reloadSlider();
        }

        // Reaplicando las funciones para la imagen de los productos
        $(".product-image").hover(
        function () { $(this).children(".quick-view").removeClass("invisible"); },
        function () { $(this).children(".quick-view").addClass("invisible"); }
        );
    }
}

// Activa un tab del carrusel inferior
function activarTab(fuente,carrusel) {

//    if (!activar.filter(".inactive").length) return;

//    desactivar.addClass("inactive");
//    desactivar2.addClass("inactive");
    //    activar.removeClass("inactive");
    
    if ('recienVisitados' == carrusel) {

        $("#losmasvistos").removeClass("active");
        $("#masvistos").removeClass("active");

        $("#recienvistos").addClass("active");
        actualizarCarrusel(fuente, bindings.carruseles.inferiorRecienVistos);
    }
    else if ('masVisitados' == carrusel) {

        $("#recienvistos").removeClass("active");
        $("#losmasvistos").removeClass("active");

        $("#masvistos").addClass("active");
        actualizarCarrusel(fuente, bindings.carruseles.inferiorMasVistos);
    }
    else if ('losMasVisitados' == carrusel) {

        $("#recienvistos").removeClass("active");
        $("#masvistos").removeClass("active");

        $("#losmasvistos").addClass("active");
        actualizarCarrusel(fuente, bindings.carruseles.inferiorLosMasVistos);
    }
}

// Activa los productos recién visitados
function activarRecienVisitados() {
    activarTab(recienVisitados, 'recienVisitados');
}

// Activa los productos más visitados
function activarMasVisitados() {
    
    activarTab(masVisitados, 'masVisitados');
}

// Activa los productos más visitados
function activarLosMasVisitados() {

    activarTab(losMasVisitados, 'losMasVisitados');
}

// Repite los elementos de los carruseles para dar la sensación de que la lista es continua
function asignarRespuesta(fuente, destino) {
//    var iteraciones = 1;
//    var resto = fuente.length % cantidad;
//    //alert('cantidad :' + cantidad);
//     
//    while (resto != 0) {
//        resto = (resto + fuente.length) % cantidad;
//        iteraciones++;
//    }

//    for (i = 0; i < iteraciones; i++)
        for (j = 0; j < fuente.length; j++)
            destino.push(fuente[j]);

    
}