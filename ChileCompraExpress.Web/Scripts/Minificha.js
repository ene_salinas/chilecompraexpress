﻿

////FUNCIONES PARA VALIDAR INPUTS, DROPDOWN///////////
function checkDropDownList(o, value) {
    if (o.val() == value) {
        o.addClass("ui-state-error");
        return false;
    } else {
        o.removeClass("ui-state-error");
        o.addClass("inputbox");
        return true;
    }
}

function checkRegexp(o, regexp) {
    if (!(regexp.test(o.val()))) {
        o.addClass("ui-state-error");
        return false;
    } else {
        o.removeClass("ui-state-error");
        return true;
    }
}

function checkLength(o, min, max) {

    if (o.val().length > max || o.val().length < min) {
        o.addClass("ui-state-error");
        return false;
    }
    else {

        o.removeClass("ui-state-error");
        return true;
    }
}

///////////////////////////////////////////////////////


function getListas(idproducto,convenio,idProveedor) {
    var listas = bindings.listas.todas();
     

    if (listas.length > 0) {
        
        var divListas = "";
        $("#lista_li").empty();
        for (i = 0; i < listas.length; i++) {

            divListas += "<li>";
            //divListas += "<span onclick='agregarAlista(" + idproducto + ",\"" + convenio.toString().trim() + "\"," + listas[i].Id + ");'>";
            //divListas += "<span onclick='agregarAlista(" + idproducto + ",\"" + convenio.toString().trim() + "\"," + listas[i].Id + ","+ idProveedor +");'>" + listas[i].Nombre + "</span>" + "<div class=\"der\"><input type=\"button\" class=\"icheck1\"/>";

            divListas += "<span onclick='agregarAlista(" + idproducto + ",\"" + $.trim(convenio.toString()) + "\"," + listas[i].Id + "," + idProveedor + ");'>" + listas[i].Nombre + "</span>" + "<div class=\"der\"><input type=\"button\" class=\"icheck1\"/>";
            divListas += "</li>";

        }

        $("#lista_li").append(divListas);
    }
}

function despliegueMinificha(idproducto, convenio) {

     
    $("#panel_" + idproducto).slideToggle();

    if (($("#crear_nombre_" + idproducto).val() != undefined) && ($("#crear_descrip_" + idproducto).val() != undefined)) {
        $("#crear_nombre_" + idproducto).remove();
        $("#crear_descrip_" + idproducto).remove();
        $("#crear_button_" + idproducto).remove();

    }

    $("#listas_" + idproducto).remove();

    var listas = bindings.listas.todas();

    var divListas = "<div id='listas_" + idproducto + "'>";


    for (i = 0; i < listas.length; i++) {
        divListas += "<div class='mislistas'>";
        divListas += "<span onclick='agregarAlista(" + idproducto + ",\"" + convenio.toString().trim() + "\"," + listas[i].Id + ");'>";
        divListas += listas[i].Nombre;
        divListas += "</span><br />";
        divListas += "</div>";

    }

    divListas += "</div>";

    $("#crear_contenedor_" + idproducto).append(divListas);

    $("#crear_nueva_" + idproducto).show();


}

//SE AGREGAN LOS ELEMENTOS HTML PARA AGREGAR UNA LISTA
function crearListaMinficha(idproducto) {

    $("#crear_nueva_" + idproducto).hide();
    $("#listas_" + idproducto).remove();

    $("#crear_contenedor_" + idproducto).append("<input type='text' id='crear_nombre' placeholder='Escriba un nombre' />");
    $("#crear_contenedor_" + idproducto).append("<textarea type='text' id='crear_descrip' placeholder='Escriba una descripción para la lista' rows='4' cols='auto' />");
    $("#crear_contenedor_" + idproducto).append("<button id='crear_button' type='button' class='save' onclick='nuevaListaMinificha();'></button>");
}

// Agrega una lista 
function nuevaListaMinificha() {

     
    var listas = bindings.listas.todas();

    if ((!$.trim($("#crear_nombre").val())
        ) && (!$.trim($("#crear_descrip").val())
        )) return;

    var lista = { Nombre: $("#crear_nombre").val(), Descripcion: $("#crear_descrip").val() };
    $.post(urls.listas, lista, function (id) {
        lista.Id = id;
        listas.push(lista);
        bindings.listas.todas(listas);
        getListas();
    });

    $("#crear_nombre").val("");
    $("#crear_descrip").val("");
    alert("La lista se ha ingresado correctamente");


}

function buscarMiniFicha(data) {


     

    var url = "/ChileCompraExpress.Web/MiniFicha/Index";
    var minificha = { "IdProveedor": data.IdProveedor, "Id": data.Id, "IdConvenio": data.IdConvenio };
        

    $.ajax({
        async: false,
        data: minificha,
        type: 'GET',
        url: url,
        cache: false,
        success: function (result) {

            $("#modal_minificha").html(result);
            //$('#productInformacion').modal({ show: false });
            $('#productInformacion').modal('show');
        }
    });
    
}

function exportExcelDetail(IdProveedor, Id, IdConvenio) {

     
    var data = { "IdProveedor": IdProveedor, "Id": Id, "IdConvenio": IdConvenio };
    exportExcel(data);

}

function exportExcel(data) {

     
    var minificha = "IdProveedor=" + data.IdProveedor + "&Id="+ data.Id +"&IdConvenio="+ data.IdConvenio;
    var url = "/ChileCompraExpress.Web/MiniFicha/ExportExcel";

    $.download(url, minificha,"post");

}

jQuery.download = function (url, data, method) {
    //url and data options required
    if (url && data) {
        //data can be string of parameters or array/object
        data = typeof data == 'string' ? data : jQuery.param(data);
        //split params into form inputs
        var inputs = '';
        jQuery.each(data.split('#'), function () {
            var pair = this.split('=');
            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
        });
        //send request
        jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>')
		.appendTo('body').submit().remove();
    };
};