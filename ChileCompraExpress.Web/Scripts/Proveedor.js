﻿
function getMasProveedores(idProducto,idConvenio) {

     

    var url = '/ChileCompraExpress.Web/Productos/GetMasProveedores'; //ChileCompraExpress.Helpers.GetURL("Productos/GetMasProveedores");
    var prod = { "Id": idProducto, "IdConvenio": idConvenio };

    $.ajax({
        async: false,
        url: url,
        type: 'POST',
        data: prod,        
        cache: false,
        success: function (result) {

             
            $("#content_mas_proveedores_" + idProducto).html(result);
            $("#content_mas_proveedores_" + idProducto).show();

        }
    });

    

}


//var proveedor = {

//    // Controladores de listas
//    proveedores: {
//        caracteristicaEspecialCheck: ko.observable(),
//        caracteristicaEspecialDescrip: {
//            Nombre: ko.observable(),
//            Descripcion: ko.observable()
//        }
//    }

//    //Caracteristicas Especiales del producto    
//    var self = this;
//    self.caracteristicaEspecialCheck = check;
//    self.caracteristicaEspecialDescrip = ko.observable(descripcion);

//    //Atributos del proveedor
//    self.attrText = attrText;
//    self.attrValue = ko.observable(attrValue);

//};

//Método que se ejecuta al presionar el botón mas proveedores
function getProveedor(idProducto) {

    var proveedor_location = '/ChileCompraExpress.Web/api/Proveedor';

    $.getJSON(proveedor_location)
        .done(function (data) {



            ko.applyBindings(new ProveedorViewModel(data));
            


        });

}

//*viewmodel* - Método que define la data y el comportamiento sobre el UI
function ProveedorViewModel(data) {
   
    
    var myObservableArray = ko.observableArray();
    
    
    $.each(data, function (key, item) {


        myObservableArray.push(new CaracteristicasEspeciales(item.Check,
                                                item.CaracteristicaEspecial,
                                                item.AtributoTexto,
                                                item.AtributoValor));

    });

    this.proveedores = myObservableArray;

    
    return this.proveedores;
    
}


//Detalle de un proveedor
function CaracteristicasEspeciales(check, descripcion, attrText, attrValue) {

    //Caracteristicas Especiales del producto    
    var self = this;
    self.caracteristicaEspecialCheck = check;
    self.caracteristicaEspecialDescrip = ko.observable(descripcion);

    //Atributos del proveedor
    self.attrText = attrText;
    self.attrValue = ko.observable(attrValue);

}


//ko.applyBindings(new ProveedorViewModel(data));
 

