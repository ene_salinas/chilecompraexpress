﻿
////FUNCIONES PARA VALIDAR INPUTS, DROPDOWN///////////
function checkDropDownList(o, value) {
    if (o.val() == value) {        
        o.addClass("ui-state-error");
        return false;
    } else {
        o.removeClass("ui-state-error");
        o.addClass("inputbox");
        return true;
    }
}

function checkRegexp(o, regexp) {
    if (!(regexp.test(o.val()))) {        
        o.addClass("ui-state-error");
        return false;
    } else {
        o.removeClass("ui-state-error");        
        return true;
    }
}

function checkLength(o, min, max) {

    if (o.val().length > max || o.val().length < min) {        
        o.addClass("ui-state-error");
        return false;
    }
    else {

        o.removeClass("ui-state-error");        
        return true;
    }
}

///////////////////////////////////////////////////////

function linksuperior(url) {

    $.ajax({

        type: "GET",
        url: "/cmii/handlerGSA/HandlerSession.aspx",
        data: param,
        success: function (data) {


            $("#contenido").remove();
            iframe(url);
        }

    });
}

function despliegue(idproducto,convenio,idProveedor) {

     
    $("#panel_" + idproducto).slideToggle();

    if (($("#crear_nombre_" + idproducto).val() != undefined) && ($("#crear_descrip_" + idproducto).val() != undefined)) {
        $("#crear_nombre_" + idproducto).remove();
        $("#crear_descrip_" + idproducto).remove();
        $("#crear_button_" + idproducto).remove();

    }

    $("#listas_" + idproducto).remove();

    var listas = bindings.listas.todas();

    var divListas = "<div id='listas_" + idproducto + "'>";


    for (i = 0; i < listas.length; i++) {
        divListas += "<div class='mislistas'>";
        divListas += "<span onclick='agregarAlista(" + idproducto + "," + convenio + "," + listas[i].Id + "," + idProveedor + ");'>";
        divListas += listas[i].Nombre;
        divListas += "</span><br />";
        divListas += "</div>";

    }

    divListas += "</div>";

    $("#crear_contenedor_" + idproducto).append(divListas);

    $("#crear_nueva_" + idproducto).show();


}

//SE AGREGA AL CARRO
function agregarACarro(idProducto, convenio,idProveedor) {

     
    var bValid = true;
    var cantidad = $("#cantidad_" + idProducto + "_" + idProveedor);
    var div_error = $("#error_cantidad_" + idProducto + "_" + idProveedor);
    
    
    bValid = bValid && checkLength(cantidad, 1, 4);
    bValid = bValid && checkRegexp(cantidad, /^([0-9])*$/);

    if (!bValid) {
        alert("Verifique los valores ingresados");
        div_error.text("Ingrese una cantidad válida");
        return false;
    }

    div_error.text("");


    var prd = { "Id": idProducto, "IdConvenio": convenio };
    var paramHandlerIdFormato = "";
    var paramHandlerIdProducto = "";
    var paramHandlerIdConvenio = "";
    var paramHandlerIdProveedor = "";

    

    //SE OBTIENEN LOS ID's QUE SON NECESARIOS PARA EL HANDLER
    $.ajax({
        async: false,
        url: "/ChileCompraExpress.Web/Productos/GetBaseProducto",
        type: 'POST',
        data: prd,
        dataType: 'json',
        cache: false,
        success: function (data) {

            paramHandlerIdFormato = data.IdFormato;
            paramHandlerIdProveedor = data.IdProveedor;
            paramHandlerIdConvenio = data.IdConvenio;
            paramHandlerIdProducto = data.Id;

            //SE ESCRIBE LA SESSION DEL ID LISTA 
            $.ajax({
                async: false,
                url: "/cmii/handlerGSA/HandlerAgregarAlCarro.aspx",
                type: 'GET',
                cache: false,
                success: function (data) {

                    //FINALMENTE SE EJECUTA LA PAGINA ENCARGADA DE REALIZAR LA INSERCION DEL REGISTRO
                    var param = { "srtDatos": "AtributosVariables=&idProducto=" + paramHandlerIdProducto +
                   "&idConvenio=" + paramHandlerIdConvenio + "&idProveedor=" + paramHandlerIdProveedor +
                   "&Cantidad=" + $("#cantidad_" + idProducto + "_" + idProveedor).val() + "&idFormato=" + paramHandlerIdFormato
                    };
                    
                    var location = '/cmii/tienda/EmulaAxaj.aspx';

                    $.ajax({
                        async: false,
                        url: location,
                        type: 'POST',
                        cache: false,
                        data: param,
                        success: function (data) {

                            $.getJSON("/cmii/handlerGSA/HandlerGetSession.aspx")
                                .done(function (items) {

                                    $("#items").text(items);
                                    var data = { "count": items };
                                    $.get("/ChileCompraExpress.Web/Escritorio/SetSessionItems", data)
                                    .done(function () {
                                    });

                                });

                            alert("Producto ingresado correctamente.");
                        }
                    });

                }
            });

        }
    });

}

//SE AGREGA UNA LISTA
function agregarAlista(idProducto,convenio,idLista,idProveedor) {
    
    ///////VALIDACION DE CAMPOS//////////////////////////
     
    var bValid = true;
    var cantidad = $("#cantidad_" + idProducto + "_" + idProveedor);
    var div_error = $("#error_cantidad_" + idProducto + "_" + idProveedor);


    bValid = bValid && checkLength(cantidad, 1, 4);
    bValid = bValid && checkRegexp(cantidad, /^([0-9])*$/);

    if (!bValid) {
        alert("Verifique los valores ingresados");
        div_error.text("Ingrese una cantidad válida");
        return false;
    }

    div_error.text("");

    //////////////////////////////////////////////////


    var prd = { "Id": idProducto, "IdConvenio": convenio };
    var paramHandlerIdFormato = "";
    var paramHandlerIdProducto = "";
    var paramHandlerIdConvenio = "";
    var paramHandlerIdProveedor = "";
    var paramHandlerIdLista = { "idLista": idLista };

     

    //SE OBTIENEN LOS ID's QUE SON NECESARIOS PARA EL HANDLER
    $.ajax({
        async: false,
        url: "/ChileCompraExpress.Web/Productos/GetBaseProducto",
        type: 'POST',
        data: prd,
        dataType: 'json',
        cache: false,
        success: function (data) {

            paramHandlerIdFormato = data.IdFormato;
            paramHandlerIdProveedor = data.IdProveedor;
            paramHandlerIdConvenio = data.IdConvenio;
            paramHandlerIdProducto = data.Id;
           
            //SE ESCRIBE LA SESSION DEL ID LISTA 
            $.ajax({
                async: false,
                url: "/cmii/handlerGSA/HandlerAgregarALista.aspx",
                type: 'GET',
                cache: false,
                data: paramHandlerIdLista,
                success: function (data) {

                    
                    //FINALMENTE SE EJECUTA LA PAGINA ENCARGADA DE REALIZAR LA INSERCION DEL REGISTRO
                    var param = { "srtDatos": "AtributosVariables=&idProducto=" + paramHandlerIdProducto +
                   "&idConvenio=" + paramHandlerIdConvenio + "&idProveedor=" + paramHandlerIdProveedor +
                   "&Cantidad=" + $("#cantidad_" + idProducto + "_" + idProveedor).val() + "&idFormato=" + paramHandlerIdFormato
                    };
                    
                    var location = '/cmii/tienda/EmulaAxaj.aspx';

                    $.ajax({
                        async: false,
                        url: location,
                        type: 'POST',
                        cache: false,
                        data: param,
                        success: function (data) {
                            debugger
                            alert("Producto ingresado correctamente.");
                        }
                    });

                }
            });

        }
    });    

}

//SE AGREGAN LOS ELEMENTOS HTML PARA AGREGAR UNA LISTA
function crearLista(idproducto) {

    $("#crear_nueva_" + idproducto).hide();
    $("#listas_" + idproducto).remove();

    $("#crear_contenedor_" + idproducto).append("<input type='text' id='crear_nombre_" + idproducto + "' placeholder='Escriba un nombre' />");
    $("#crear_contenedor_" + idproducto).append("<textarea type='text' id='crear_descrip_" + idproducto + "' placeholder='Escriba una descripción para la lista' rows='4' cols='auto' />");
    $("#crear_contenedor_" + idproducto).append("<button id='crear_button_" + idproducto + "' type='button' class='save' onclick='nuevaLista(" + idproducto + ");'></button>");
}


// Agrega una lista 
function nuevaLista(idProducto) {
     
    var listas = bindings.listas.todas();

    if ((!$.trim($("#crear_nombre_" + idProducto).val())
        ) && (!$.trim($("#crear_descrip_" + idProducto).val())
        )) return;

    var lista = { Nombre: $("#crear_nombre_" + idProducto).val(), Descripcion: $("#crear_descrip_" + idProducto).val() };
    $.post(urls.listas, lista, function (id) {
        lista.Id = id;
        listas.push(lista);
        bindings.listas.todas(listas);
    });

    $("#crear_nombre_" + idProducto).val("");
    $("#crear_descrip_" + idProducto).val("");

    $("#crear_nombre_" + idProducto).remove();
    $("#crear_descrip_" + idProducto).remove();
    $("#crear_button_" + idProducto).remove();
    $("#listas_" + idProducto).remove();

    alert("La lista se ha ingresado correctamente");

    $("#flip_" + idProducto).click();
}


//VISUALIZA LA FICHA QUE SE ENCUENTRA EN TIENDA2
function verFicha(idProducto, convenio) {

    debugger;
    var productos_location = '/ChileCompraExpress.Web/Productos/VerFicha';
    var prod = { "Id": idProducto, "IdConvenio": convenio };

    $.ajax({
        async: false,
        url: productos_location,
        type: 'POST',
        dataType: "json",
        cache: false,
        data: prod,
        success: function (data) {

            var param = 'codeUserOrg=' + data.CodeUsrOrg;
            param += '&idProducto=' + data.Id + '&idConvenioMarco=' + data.IdConvenio + '&idFormato=' + data.IdFormato;
            param += '&idUsuario=' + data.IdUser;

            $.ajax({

                type: "GET",
                url: "/cmii/handlerGSA/HandlerSession.aspx",
                data: param,
                success: function (data) {


                    $("#contenido").remove();
                    iframe(data);
                    //$("#content-body").html("<iframe style='width:100%;height:900px' src="+ data +"></iframe>");


                }

            });

        }
    });


}


//VISUALIZA LA FICHA QUE SE ENCUENTRA EN TIENDA2
function verProveedor(idProducto, convenio,page) {


    var productos_location = '/ChileCompraExpress.Web/Productos/VerFicha';
    var prod = { "Id": idProducto, "IdConvenio": convenio };

    $.ajax({
        async: false,
        url: productos_location,
        type: 'POST',
        dataType: "json",
        cache: false,
        data: prod,
        success: function (data) {

            var param = 'codeUserOrg=' + data.CodeUsrOrg;
            param += '&idProducto=' + data.Id + '&idConvenioMarco=' + data.IdConvenio + '&idFormato=' + data.IdFormato;
            param += '&idUsuario=' + data.IdUser;

            $.ajax({

                type: "GET",
                url: "/cmii/handlerGSA/HandlerSession.aspx",
                data: param,
                success: function () {


                    $("#contenido").remove();
                    iframe(page);
                    //$("#content-body").html("<iframe style='width:100%;height:900px' src="+ data +"></iframe>");


                }

            });

        }
    });


}

 $(document).ready(function() {
    
});

