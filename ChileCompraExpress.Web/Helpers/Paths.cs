﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace ChileCompraExpress.Web.Helpers
{
    public class Paths
    {
        public static string PathImagenes()
        {
            return ConfigurationSettings.AppSettings["UrlImagen"].ToString();
        }
    }
}