﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using ChileCompraExpress.Common.Helpers;
using ChileCompraExpress.HttpHandlers.DapperHelper;
using Dapper;
using MP.Negocio.ChileCompraWeb.Entities;
using MP.Negocio.Entities;
using System.Collections;

namespace ChileCompraExpress.Web.Helpers
{
    public class SessionHelper
    {
        private NameValueCollection _connectionStrings = (NameValueCollection)ConfigurationManager.GetSection("ConexionesBD");

        private long SessionId
        {
            get
            {
                var identity = (FormsIdentity)HttpContext.Current.User.Identity;
                string[] data = identity.Ticket.UserData.Split(';');
                return long.Parse(data[0]);
            }
        }

        /// <summary>
        /// Establece el contador actual del carrito
        /// </summary>
        /// <param name="count">Items</param>
        public void SetCountItems(string count)
        {
            HttpContext.Current.Session["itemsCarrito"] = count;
        }

        /// <summary>
        /// Obtiene contador actual del carrito
        /// </summary>
        /// <returns></returns>
        public static string getCountItems()
        {            
            return ((HttpContext.Current.Session["itemsCarrito"] == null) || (String.IsNullOrEmpty(HttpContext.Current.Session["itemsCarrito"].ToString()))) ? "0" : HttpContext.Current.Session["itemsCarrito"].ToString();
        }

        /// <summary>
        /// Establece si el usuario es comprador 
        /// </summary>
        /// <param name="esComprador">bool es comprador</param>
        public void SetEsComprador(bool esComprador)
        {
            HttpContext.Current.Session["esComprador"] = esComprador;
        }

        /// <summary>
        /// Obtiene rol del usuario
        /// </summary>
        /// <returns></returns>
        public static bool getEsComprador()
        {
            return ((HttpContext.Current.Session["esComprador"] == null) || (String.IsNullOrEmpty(HttpContext.Current.Session["esComprador"].ToString()))) ? false : bool.Parse(HttpContext.Current.Session["esComprador"].ToString());
        }


        private string OrganizationId
        {
            get
            {
                var identity = (FormsIdentity)HttpContext.Current.User.Identity;
                string[] data = identity.Ticket.UserData.Split(';');
                return data[1];
            }
        }

        public List<Entities.Region> GetRegiones(Entities.Usuario user,string sesion)
        {
            string conn = ConfigurationSettings.AppSettings["Tienda_SESSION"].ToString();

            using (var connection = new SqlConnection(conn))
            {
                connection.Open();
                var results = connection.Query<Entities.Region>("ING01_Stp_CHC_ListaRegiones",
                        new { IdSesion = sesion, IdUsuario = user.Code + "_" + user.Organization.Code  },
                        null, true, null,
                        CommandType.StoredProcedure);

                var nombres = new ArrayList();
                var abreviaciones = new ArrayList();
                var ids = new List<string>();
                List<Entities.Region> regiones = new List<Entities.Region>();
                Entities.Region region = null;

                foreach (var result in results)
                {
                    region = new Entities.Region();
                    region.idRegion = result.idRegion;
                    region.Nombre = result.Nombre;
                    region.Abreviacion = result.Numero;
                    region.Existe = result.Existe;
                    regiones.Add(region);
                }

                return regiones;
            }
        }

        public ChileCompraExpress.Entities.Usuario GetUser()
        {

            string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
            bool datosdummies = (dummies.ToString().ToLower().Equals("true")) ? true : false;
            Entities.Usuario user = new Entities.Usuario();
            user.Code = "123";
            user.Organization = new Entities.Organization();
            user.Organization.Code = "123";

            if (!datosdummies)
            {

                var session = GetDBSession(SessionId);
                user = GetDBUser(session.sesUser);
                var sesion = GetDBSession(SessionId);



                if (user == null)
                {
                    throw new UnauthorizedAccessException();
                }

                user.Organization = GetDBOrganization(this.OrganizationId);

                user.Regiones = GetRegiones(user, HttpContext.Current.Session.SessionID.ToString()).ToArray();

            }
            else
            {
                List<Entities.Region> regiones = new List<Entities.Region>();
                Entities.Region region = new Entities.Region();
                region.Abreviacion = "XV";
                region.Nombre = "Test Region";
                region.Numero = "15";
                region.idRegion = 15;
                regiones.Add(region);
                user.Regiones = regiones.ToArray();
            }
            

            return user;
        }

        private string ConnectionString
        {
            get { return _connectionStrings["DCCPPlatform_SESSION"]; }
        }

        public bool CheckRoles()
        {
            try
            {
                ////TraceHelper.Verbose("Obteniendo sessión de BD para [sessionId = {0}].", SessionId);
                var session = GetDBSession(SessionId);

                if (session == null)
                {
                    //TraceHelper.Error("No se encontraron datos en la BD para la sessión [sessionId = {0}].", SessionId);
                    throw new UnauthorizedAccessException();
                }

                //TraceHelper.Verbose("Obteniendo datos del usuario [{0}].", session.sesUser);
                var user = GetDBUser(session.sesUser);

                if (user == null)
                {
                    //TraceHelper.Error("No se encontraron datos del usuario [{0}].", session.sesUser);
                    throw new UnauthorizedAccessException();
                }

                //TraceHelper.Verbose("Obteniendo organización [organizationId = {0}].", OrganizationId);
                user.Organization = GetDBOrganization(this.OrganizationId);

                //TraceHelper.Verbose("Obteniendo roles [userCode = {0}][organizationCode = {1}].", user.Code, user.Organization.Code);
                user.UserRoles = GetDBUserRoles(user.Code, user.Organization.Code);

                int rolComprador = int.Parse(ConfigurationManager.AppSettings["TiendaRolComprador"]);
                int rolProveedor = int.Parse(ConfigurationManager.AppSettings["TiendaRolProveedor"]);

                bool roleComprador = (user.UserRoles.ContainsKey(rolComprador));
                SetEsComprador(roleComprador);

                // comprueba que usuario tenga los roles de comprador y/o proveedor
                if (!(user.UserRoles.ContainsKey(rolComprador) || user.UserRoles.ContainsKey(rolProveedor)))
                {
                    //TraceHelper.Error("El usuario [{0}] no pose rol de comprador [{1}] y/o proveedor [{2}]", session.sesUser, rolComprador, rolProveedor);
                    throw new UnauthorizedAccessException();
                }

                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            } 
        }

        private Sesion GetDBSession(long sessionId)
        {
            try
            {
                Sesion result = new Sesion();
                
                string dummies = ConfigurationSettings.AppSettings["dummies"].ToString();
                bool datosdummies = (dummies.ToString().ToLower().Equals("true"))?true:false;

                if (!datosdummies)
                {
                    string conn = ConfigurationSettings.AppSettings["DCCPPlatform_SESSION"].ToString();
                    using (var connection = new SqlConnection(conn))
                    {
                        connection.Open();

                        result = connection.Query<Sesion>(
                            "gblSpcReadgblSession",
                            new { resultCode = 0, resultMessage = "", sesId = sessionId },
                            null, true, (int?)null,
                            CommandType.StoredProcedure).FirstOrDefault();
                                               
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private ChileCompraExpress.Entities.Usuario GetDBUser(string userCode)
        {
            try
            {
                string conn = ConfigurationSettings.AppSettings["DCCPPlatform_SESSION"].ToString();
                using (var connection = new SqlConnection(conn))
                {
                    connection.Open();

                    var result = connection.Query<Usuario>(
                        "gblSpcGetUser",
                        new { resultCode = 0, resultMessage = "", usrCode = userCode },
                        null, true, null,
                        CommandType.StoredProcedure).First();

                    

                    var user = new ChileCompraExpress.Entities.Usuario();

                    user.Id = result.usrID;
                    user.IsActive = result.usrIsActive == 1;
                    user.Code = result.usrCode;
                    user.IsTest = result.usrIsTest == 1;
                    user.IsLocked = result.usrIsLocked == 1;
                    user.Login = result.usrLogin;
                    user.Password = result.usrPassword;
                    if (result.usrPasswordChanged != null)
                        user.PasswordChanged = result.usrPasswordChanged;
                    user.FailedAttempts = result.usrFailedAttempts;
                    user.languageCode = result.usrLanguage;
                    user.TimeZone = result.usrTimeZone;
                    user.ApplyDST = result.usrApplyDST;
                    user.FirstName = result.usrFirstName;
                    user.LastName = result.usrLastName;
                    user.Position = result.usrPosition;
                    user.Email = result.usrEmail;
                    user.EmailAlternative = result.usrEmailAlternative;
                    user.Phone = result.usrPhone;
                    user.Mobile = result.usrMobile;
                    user.Fax = result.usrFax;
                    user.MailFormat = result.usrMailFormat;
                    user.PagerAddress = result.usrPagerAddress;
                    user.AcceptEmail = result.usrAcceptEmail;
                    user.AcceptSMS = result.usrAcceptSMS;
                    user.AcceptPager = result.usrAcceptPager;
                    user.TaxId = result.usrTaxId;
                    user.UserDefined1 = result.usrUserDefined1;
                    user.UserDefined2 = result.usrUserDefined2;
                    user.UserDefined3 = result.usrUserDefined3;

                    return user;
                }
            }
            catch (ArgumentException arg)
            {
                // cuando el sp no ejecuta un select, Dapper produce esta excepción
                // http://code.google.com/p/dapper-dot-net/issues/detail?id=57
                throw (arg);
            }
        }

       
        private ChileCompraExpress.Entities.Organization GetDBOrganization(string organizationCode)
        {
            try
            {
                string conn = ConfigurationSettings.AppSettings["DCCPPlatform_SESSION"].ToString();
                using (var connection = new SqlConnection(conn))
                {
                    connection.Open();

                    var param = new
                    {
                        resultCode = 0,
                        resultMessage = "",
                        orgCode = organizationCode,
                        orgName = "",
                        orgParentOrganization = "",
                        orgEnterprise = "",
                        orgIsActive = 1,
                        pageNumber = 0,
                        pageSize = 0
                    };

                    var result = connection.Query<ChileCompraExpress.Entities.Organization>(
                        "gblSpcReadgblOrganization",
                        param, null, true, null,
                        CommandType.StoredProcedure).FirstOrDefault();

                    if (result == null)
                        return null;

                    var organization = new ChileCompraExpress.Entities.Organization();
                    organization.Code = result.orgCode;
                    organization.Name = result.orgName;
                    organization.orgClass = result.orgClass;
                    organization.entCode = result.orgEnterprise;
                    organization.entName = result.entName;
                    organization.orgTaxID = result.orgTaxID;

                    return organization;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private Dictionary<int, ChileCompraExpress.Entities.UserRole> GetDBUserRoles(string userCode, string organizationCode)
        {
            try
            {
                string conn = ConfigurationSettings.AppSettings["DCCPPlatform_SESSION"].ToString();
                using (var connection = new SqlConnection(conn))
                {
                    connection.Open();

                    var output = new Dictionary<int, ChileCompraExpress.Entities.UserRole>();

                    var param = new
                    {
                        resultCode = 0,
                        resultMessage = "",
                        uroId = 0,
                        uroIsActive = 2,
                        uroOrganization = organizationCode,
                        uroUser = userCode,
                        uroRole = 0,
                        uroIsDefault = 2,
                        pageNumber = 0,
                        pageSize = 0
                    };

                    var results = connection.Query<UsuarioRol>(
                        "gblSpcReadgblSecUserRole",
                        param,
                        null,
                        true,
                        null,
                        CommandType.StoredProcedure);

                    foreach (var result in results)
                    {
                        var role = new ChileCompraExpress.Entities.UserRole();
                        role.IsActive = result.uroIsActive == 1;
                        role.Organization = result.uroOrganization;
                        role.Role = result.uroRole;
                        role.IsDefault = result.uroIsDefault == 1;

                        output.Add(role.Role, role);
                    }

                    return output;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
    }
}
