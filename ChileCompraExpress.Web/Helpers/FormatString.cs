﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChileCompraExpress.Utilities;

namespace ChileCompraExpress.Web.Helpers
{
    public class FormatString
    {
        public static string TagBr(string texto, int split)
        {
            string[] textSplit = texto.Split(' ');
            string textFormat = "";
            for (int i = 0; i < textSplit.Length; i++)
            {
                textFormat += (Utilities.Tools.esMultiplo(split,i )) ? textSplit[i] + " <br />" : textSplit[i] + " ";
            }

            return textFormat;
        }

        public static string TagBrByLetter(string texto, int split)
        {
            char[] textSplit = texto.ToCharArray();
            string textFormat = "";
            for (int i = 0; i < textSplit.Length; i++)
            {
                textFormat += (Utilities.Tools.esMultiplo(split, i)) ? textSplit[i] + " <br />" : textSplit[i] + " ";
            }

            return textFormat;
        }
    }
}