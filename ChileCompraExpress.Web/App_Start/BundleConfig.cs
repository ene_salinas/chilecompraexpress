﻿using System.Web;
using System.Web.Optimization;

namespace ChileCompraExpress.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*",
            //            "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
            "~/Scripts/knockout-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                       "~/Content/bxslider/jquery.bxslider.js",
                       "~/Scripts/jquery.form.js",
                       "~/Content/bsmodal/bootstrap.modal.js",
                       "~/Scripts/Carrusel.js",
                       "~/Scripts/Regiones.js",
                       "~/Scripts/Listas.js",
                       //"~/Scripts/jquery.timer.js",                       
                       //"~/Scripts/DenunciaPrecioCaro.js",
                       //"~/Scripts/chilecompraexpress.default.js",
                       //"~/Scripts/chilecompraexpress.resultados.js",
                       //"~/Scripts/Proveedor.js",
                       //"~/Scripts/DenunciaPrecioCaro.js",
                       //"~/Scripts/Minificha.js",
                       "~/Scripts/Informaciones.js",
                       "~/Scripts/Resultado.js"//,
                       //"~/Scripts/tooltipster-master/js/jquery.tooltipster.js"                       
                       ));
            


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/Estilos/Css/Base.css",
                        "~/Scripts/bootstrap/css/bootstrap.css",
                        "~/Content/Helpers.css",        
                        "~/Content/Site.css",
                        "~/Content/Layout.css",
                        "~/Content/Modals.css",
                        "~/Content/Forms.css",
                        "~/Content/Escritorio.css",


                        //"~/Content/Estilos/Tooltip2/ToolTip2.tooltip.css",
                        //"~/Scripts/tooltipster-master/css/tooltipster.css",


                        "~/Content/bxslider/jquery.bxslider.css",                        
                        "~/Content/bsmodal/bootstrap.modal.css"));

            //bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
            //            "~/Content/themes/base/jquery.ui.core.css",
            //            "~/Content/themes/base/jquery.ui.resizable.css",
            //            "~/Content/themes/base/jquery.ui.selectable.css",
            //            "~/Content/themes/base/jquery.ui.accordion.css",
            //            "~/Content/themes/base/jquery.ui.autocomplete.css",
            //            "~/Content/themes/base/jquery.ui.button.css",
            //            "~/Content/themes/base/jquery.ui.dialog.css",
            //            "~/Content/themes/base/jquery.ui.slider.css",
            //            "~/Content/themes/base/jquery.ui.tabs.css",
            //            "~/Content/themes/base/jquery.ui.datepicker.css",
            //            "~/Content/themes/base/jquery.ui.progressbar.css",
            //            "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}