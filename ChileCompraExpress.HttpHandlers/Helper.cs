﻿using System;
using System.Web;

namespace ChileCompraExpress.HttpHandlers
{
    public class Helper
    {
        public static string GetIdUsuario(HttpContext context)
        {
            string key = MP.Negocio.CMII_CLS.Generales.enumVariable.str_idUsuario.ToString();
            return context.Session[key].ToString();
        }

        public static int ParseInt(HttpContext context, string key, bool required = true)
        {
            string param = context.Request.QueryString[key];

            if (required && string.IsNullOrEmpty(param.Trim()))
                throw new ArgumentException(Messages.RequiredValue, key);

            int output;

            if (!int.TryParse(param, out output))
                throw new ArgumentException(Messages.InvalidValue, key);

            return output;
        }

        public static string ParseString(HttpContext context, string key, bool required = true)
        {
            string param = context.Request.QueryString[key];

            if (required && string.IsNullOrEmpty(param.Trim()))
                throw new ArgumentException(Messages.RequiredValue, key);

            return param.Trim();
        }
    }
}
