﻿using System;
using System.Web;
using System.Web.SessionState;

namespace ChileCompraExpress.HttpHandlers
{
    public abstract class BaseHttpHandler : IHttpAsyncHandler, IRequiresSessionState
    {
        private AsyncProcessorDelegate _delegate;
        protected delegate void AsyncProcessorDelegate(HttpContext context);

        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            _delegate = new AsyncProcessorDelegate(PreProcessRequest);
            return _delegate.BeginInvoke(context, cb, extraData);
        }

        public void EndProcessRequest(IAsyncResult result)
        {
            _delegate.EndInvoke(result);
        }

        public bool IsReusable
        {
            get { return true; }
        }

        public abstract void ProcessRequest(HttpContext context);

        private void PreProcessRequest(HttpContext context)
        {
            // la siguiente asignación es necesaria para evitar el famoso bug de QA
            // en la tienda CMII (de las rutas de los archivos de configuración)
            HttpContext.Current = context;

            ProcessRequest(context);
        }
    }
}
