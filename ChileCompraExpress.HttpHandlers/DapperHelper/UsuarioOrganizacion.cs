﻿namespace ChileCompraExpress.HttpHandlers.DapperHelper
{
    public class UsuarioOrganizacion
    {
        public short uroIsdefault { get; set; }
        public string orgCode { get; set; }
        public string orgName { get; set; }
        public short orgInformation { get; set; }
    }
}
