﻿using System;

namespace ChileCompraExpress.HttpHandlers.DapperHelper
{
    public class Usuario
    {
        public int usrID { get; set; }
        public short usrIsActive { get; set; }
        public string usrCode { get; set; }
        public short usrIsTest { get; set; }
        public short usrIsLocked { get; set; }
        public string usrLogin { get; set; }
        public string usrPassword { get; set; }
        public DateTime usrPasswordChanged { get; set; }
        public short usrFailedAttempts { get; set; }
        public int usrLanguage { get; set; }
        public int usrTimeZone { get; set; }
        public short usrApplyDST { get; set; }
        public int usrGender { get; set; }
        public string usrFirstName { get; set; }
        public string usrLastName { get; set; }
        public string usrPosition { get; set; }
        public string usrEmail { get; set; }
        public string usrEmailAlternative { get; set; }
        public string usrPhone { get; set; }
        public string usrMobile { get; set; }
        public string usrFax { get; set; }
        public short usrMailFormat { get; set; }
        public string usrPagerAddress { get; set; }
        public short usrAcceptEmail { get; set; }
        public short usrAcceptSMS { get; set; }
        public short usrAcceptPager { get; set; }
        public string usrTaxId { get; set; }
        public string usrUserDefined1 { get; set; }
        public string usrUserDefined2 { get; set; }
        public string usrUserDefined3 { get; set; }
        public string lngName { get; set; }
        public string tznName { get; set; }
        public string gndName { get; set; }
        public bool usrIsFirstLogin { get; set; }
        public DateTime usrLastLogin { get; set; }
        public string usrTwitter { get; set; }
    }
}
