﻿namespace ChileCompraExpress.HttpHandlers.DapperHelper
{
    public class UsuarioRol
    {
        public int uroID { get; set; }
        public short uroIsActive { get; set; }
        public string uroOrganization { get; set; }
        public string uroUser { get; set; }
        public int uroRole { get; set; }
        public string rolName { get; set; }
        public short uroIsDefault { get; set; }
        public string orgName { get; set; }
        public string usrLastName { get; set; }
        public string usrFirstName { get; set; }
        public short orgIsActive { get; set; }
    }
}
