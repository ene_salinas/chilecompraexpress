﻿using System;

namespace ChileCompraExpress.HttpHandlers.DapperHelper
{
    public class Sesion
    {
        public long sesId { get; set; }
        public string sesUser { get; set; }
        public DateTime sesStartDateTime { get; set; }
        public DateTime sesEndDateTime { get; set; }
        public string sesIP { get; set; }
    }
}
