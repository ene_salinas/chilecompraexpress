﻿namespace ChileCompraExpress.HttpHandlers
{
    public static class Messages
    {
        public static readonly string RequiredValue = "Parámetro requerido";
        public static readonly string InvalidValue = "Valor no permitido";
    }
}
