﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace ChileCompraExpress.HttpHandlers
{
    [DataContract]
    public class BaseResponse
    {
        bool _loggedOut = false;

        [DataMember]
        public bool LoggedOut
        {
            get { return _loggedOut; }
            set { _loggedOut = value; }
        }

        bool _error = false;

        [DataMember]
        public bool Error
        {
            get { return _error; }
            set { _error = value; }
        }

        string _errorMessage = string.Empty;

        [DataMember]
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        public BaseResponse()
        {
        }

        public BaseResponse(bool error, string errorMessage)
        {
            this._error = error;
            this._errorMessage = errorMessage;
        }

        public virtual string SerializeToJson()
        {
            string output = string.Empty;

            try
            {
                output = JsonConvert.SerializeObject(this);
            }
            catch (Exception ex)
            {
                output = "{\"Error\":true,\"ErrorMessage\":\"" + ex.Message + "\"}";
            }

            return output;
        }
    }
}
